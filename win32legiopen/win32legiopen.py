#! /usr/bin/env python

import os
import sys
import httplib
import tempfile
import random

filename = sys.argv[1]

conn = httplib.HTTPConnection("ootest.pcf.be")
body = file(filename, 'rb').read()
conn.request("POST", "/preview/odt", body=body)
response = conn.getresponse()
data = response.read()
conn.close()

output_filename = os.path.splitext(os.path.basename(filename))[0]
output_filename = os.path.join(tempfile.gettempdir(), output_filename)
if os.path.exists(output_filename + '.odt'):
    i = 1
    while os.path.exists(output_filename + '-%s.odt'%i):
        i += 1
    output_filename += '-%s' % i
output_filename += '.odt'
fd = file(output_filename, 'wb')
fd.write(data)
fd.close()

os.system('start %s' % output_filename)

