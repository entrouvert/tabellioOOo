#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys

legi_styles = [('Titre Préface', 'TitrePreface'),
    ('Titre Résumé', 'TitreSynthese'),
    ('Partie', 'Partie'),
    ('Chap', 'Chap'),
    ('Sec 1', 'Sec 1'),
    ('Sec 1.1', 'Sec 1.1'),
    ('Sec 1.1.1', 'Sec 1.1.1'),
    ('Sec 1.1.1.1', 'Sec 1.1.1.1'),
    ('Ss-Titre', 'Sous-Titre'),
    ('Literal', 'Standard'),
    ('Note', 'Note'),
    ('NoteTdM', 'NoteTableDesMatieres'),
    ('Para', 'Text body')
    ]

legistic_styles = [
    ('LPart', 'Lpart'),
    ('LLivre', 'LLivre'),
    ('LTitre', 'Ltitre'),
    ('LChapitre', 'Lchapitre'),
    ('LSection', 'Lsection'),
    ('LSous-Section', 'LSous-Section'),
    ('LArt', 'Larttitre')
    ]
f = sys.stdin.read()

def get_xml(styles):
    s = []
    for i, style in enumerate(styles):
        c = i
        if styles is legistic_styles:
            c += len(legi_styles) + 2
        stylelabel, stylename = style
        s.append('''
    <node oor:name="m%(c)02d" oor:op="replace">
      <prop oor:name="URL" oor:type="xs:string">
        <value>service:org.entrouvert.openoffice.StyleApply?%(stylename)s</value>
      </prop>
      <prop oor:name="Title" oor:type="xs:string">
        <value>%(stylelabel)s</value>
      </prop>
      <prop oor:name="Target" oor:type="xs:string">
        <value>_self</value>
      </prop>
      <prop oor:name="Context" oor:type="xs:string">
        <value>com.sun.star.text.TextDocument</value>
      </prop>
    </node>''' % locals())
    return '\n'.join(s)



print f.replace(
        '@TOOLBAR_LEGI_STYLES@', get_xml(legi_styles)).replace(
        '@TOOLBAR_LEGISTIC_STYLES@', get_xml(legistic_styles))

