#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Tabellio -- software suite for deliberative assemblies
#          -- suite logicielle pour assemblées délibératives
#          -- http://www.tabellio.org/
# Copyright (C) 2006 Parlement de la Communauté française de Belgique

# This file is part of Tabellio.

# Tabellio is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# Tabellio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import sys
import os
import string
import getopt

def printUsage():
    print """
Usage: pdfGenerator.py [options]
   Options:
      --input-file or -i: name of the input .legi file
      --output-pdf-file or -o: name of the output file (pdf) (optional)
      --output-latex-file or -t: name of the output file (latex) (optional)
      --help or -h: print this info page (optional)
      --latex or -l: keep the latex file (optional)
      --debug or -d: keep the temporary files (optional)
      --brouillon or -b: draft stamp (optional)
      --notoc: produce a pdf without table of content (defaut=with toc)
      --annexe or -a: annexe definition
      --parchemin or -p: style parchemin
      --bqr or -q: style BQR
      --legacy: legacy mode (old XSL and TeX)
      --nograyscale: do not convert to grayscale
        
        annexe definition = "filename|title|scale"

  example:
    pdfGenerator.py --input-file='test.legi' -a "annexe.pdf|Une annexe|O.9"
          """

if __name__ == '__main__':
    file = os.path.abspath(sys.argv[0])
    sys.path.append(os.path.join(os.path.dirname(file),"..","lib"))
    sys.path.append(os.path.join(os.path.dirname(file),"..","..","..","pythonlib","trunk","pythonlib"))
    
    from legi2pdf.pdfGenerator import convertLegi2Pdf

    inputFile = None
    useFont = None
    outputPdfFile = None
    outputLatexFile = None
    keepLatex = 0
    help = 0
    debug = 0
    draft = 0
    toc = True
    style= "normal"
    annexes = []
    legacyMode = False
    grayscale = True

    optlist, args = getopt.getopt(sys.argv[1:], 
            'i:o:t:a:hldbpq',
            ['input-file=', 'output-pdf-file=', 'output-latex-file=', 
             'annexe=', 'help', 'latex', 'debug', 'brouillon', 'notoc',
             'parchemin', 'bqr', 'use-font=', 'legacy', 'ng',
             'nograyscale'])
    
    for option,value in optlist:
        if option == '-i' or option == '--input-file':
            inputFile = value
        elif option == '-o' or option == '--output-pdf-file':
            outputPdfFile = value
        elif option == '-t' or option == '--output-latex-file':
            outputLatexFile = value
        elif option == '--use-font':
            useFont = value
        elif option == '--legacy':
            legacyMode = True
        elif option == '--ng':
            legacyMode = False
        elif option == '-l' or option == '--latex':
            keepLatex = 1
        elif option == '-h' or option == '--help':
            help = 1
        elif option == '-d' or option == '--debug':
            debug = 1
        elif option == '-b' or option == '--brouillon':
            draft = 1
        elif option == '-p' or option == '--parchemin':
            style = "parchment"
        elif option == '-q' or option == '--bqr':
            style = "bqr"
        elif option == '--notoc':
            toc = False
        elif option == '--nograyscale':
            grayscale = False
        elif option == '-a' or option == '--annexe':
            r = value.split('|')
            if len(r) == 1:
                annexes.append((r[0],"",100))
            elif len(r) == 2:
                annexes.append((r[0],r[1],100))
            elif len(r) == 3:
                scale = string.atoi(r[2])
                annexes.append((r[0],r[1],scale))
            else:
                print "invalid option", option
                printUsage()
                sys.exit(-1)
                
        else:
            print "invalid option", option

    if inputFile != None:
        if outputPdfFile == None:
            if inputFile.endswith('.legi'):
                outputPdfFile = inputFile[:-5] + '.pdf'
            else:
                outputPdfFile = inputFile + '.pdf'
        if outputLatexFile == None:
            if outputPdfFile.endswith('.pdf'):
                outputLatexFile = outputPdfFile[:-4] + '.tex'
            else:
                outputLatexFile = outputPdfFile + '.tex'

        convertLegi2Pdf(inputFile, outputPdfFile, outputLatexFile, keepLatex,
                        debug, annexes, draft, toc, style, useFont, legacyMode,
                        grayscale)
        if help:
            printUsage()
    else:
        printUsage()

