<?xml version="1.0" encoding="utf-8"?>
<!--
Tabellio - software suite for deliberative assemblies
         - suite logicielle pour assemblées délibératives
         - http://www.tabellio.org/
Copyright (C) 2006 Parlement de la Communauté française de Belgique

This file is part of Tabellio.

Tabellio is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tabellio is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!--  FileName: pfb.xsl                    --> 
<!-- 
      Purpose: PFB style
--> 


<xsl:variable name="pdfgen.document.pfb.session">
    <xsl:choose>
        <xsl:when test="starts-with($pdfgen.metadata.session,'SE')">
            <xsl:text>Session extraordinaire de </xsl:text>
            <xsl:value-of select="substring-after($pdfgen.metadata.session,'SE')"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:text>Session ordinaire </xsl:text>
            <xsl:value-of select="$pdfgen.metadata.session"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:variable>


<xsl:template name="pfb-first-page-normal">
    <!-- First part of the page -->

    <xsl:text>\thispagestyle{empty}&#10;</xsl:text>
    <xsl:text>\vspace*{-1.5cm}&#10;</xsl:text>

    <!-- Header part definition -->
    <xsl:text>\begin{changemargin}{-0.9cm}{-0.6cm}&#10;</xsl:text>
    <xsl:if test="$pdfgen.metadata.remplace = 'true'">
        <xsl:text>\vspace*{-12pt}&#10;</xsl:text>
        <xsl:text>\begin{center}&#10;</xsl:text>
        <xsl:text>\noindent{\textmd{\textbf{\uppercase{Ce document remplace et annule le document distribué précédemment}}}}\\&#10;</xsl:text>
        <xsl:text>\end{center}&#10;</xsl:text>
    </xsl:if>
    <xsl:text>\noindent\large{</xsl:text>
    <xsl:value-of select="$pdfgen.document.title.id"/>
    <xsl:text>}</xsl:text>
    <xsl:text>\hfill\large{</xsl:text>
    <xsl:value-of select="$pdfgen.document.title.id"/>
    <xsl:text>}</xsl:text>
    <xsl:text>\\[-0.3cm]&#10;</xsl:text>
    <xsl:text>\rule{17.5cm}{0.04cm}&#10;</xsl:text>

    <!-- Main title with the session reference -->
    <xsl:choose>
        <xsl:when test="$pdfgen.metadata.typedocid = 'CRICOM'">
           <xsl:text>\begin{minipage}[b][10.1cm][c]{17.5cm}&#10;</xsl:text>
        </xsl:when>
        <xsl:otherwise>
           <xsl:text>\begin{minipage}[b][7.6cm][c]{17.5cm}&#10;</xsl:text>
        </xsl:otherwise>
    </xsl:choose>

    <xsl:text>
\begin{center}
\vfill
\vfill
\textbf{
\Huge{{Parlement francophone bruxellois}} \\
\Large{{(Assemblée de la Commission communautaire française)}} \\
\vfill
\vfill
\includegraphics[width=3cm]{logo-pfb.png}
\vfill
\Large{{</xsl:text>
<xsl:value-of select="$pdfgen.document.pfb.session"/><xsl:text>}}}
\vfill
\vfill
\end{center}
    </xsl:text>

    <xsl:text>\end{minipage}&#10;</xsl:text>
    <xsl:text>\begin{minipage}[b][0.8cm][s]{17.5cm}&#10;</xsl:text>
    <xsl:text>\rule{17.5cm}{0.02cm}&#10;</xsl:text>
    <xsl:text>\center{\textbf{</xsl:text>
    <xsl:value-of select="property[@name='date']"/>
    <xsl:text>}}&#10;</xsl:text>
    <xsl:text>\rule{17.5cm}{0.02cm}&#10;</xsl:text>
    <xsl:text>\end{minipage}&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>

    <!-- First titleMain title with the session reference -->
    
    <xsl:if test="property[@name='typedos'] != '' or property[@name='intitdos'] != ''">
        <xsl:text>\vfill&#10;</xsl:text>
        <xsl:text>\vfill&#10;</xsl:text>
        <xsl:text>\relsize{</xsl:text>
        <xsl:value-of select="$pdfgen.document.tailletitres"/>
        <xsl:text>}&#10;</xsl:text>
        <xsl:text>\begin{center}\parbox[b][][c]{15cm}{&#10;</xsl:text>
        <xsl:text>\begin{center}\renewcommand{\baselinestretch}{1.4}&#10;</xsl:text>

        <xsl:if test="property[@name='typedos'] != ''">
            <!-- 2) Title part -->
            <xsl:text>\relsize{2}\uppercase{\textbf{</xsl:text>
            <xsl:value-of select="$pdfgen.metadata.typedos"/>
            <xsl:if test="not(property[@name='intitdos'] != '') and property[@name='docrefs'] != ''">
                <xsl:text>\footnote{</xsl:text>
                <xsl:value-of select="property[@name='docrefs']"/>
                <xsl:text>}&#10;</xsl:text>
            </xsl:if>
            <xsl:text>}} \\[0.5cm]&#10;</xsl:text>
        </xsl:if>
                    
        <xsl:if test="property[@name='intitdos'] != ''">
            <!-- 2) Sub-title part -->
            <xsl:text>\relsize{-2}\textbf{{</xsl:text>
            <xsl:value-of select="$pdfgen.metadata.intitdos"/>
            
            <xsl:if test="property[@name='docrefs'] != ''">
                <xsl:text>\footnote{</xsl:text>
                <xsl:value-of select="property[@name='docrefs']"/>
                <xsl:text>}&#10;</xsl:text>
            </xsl:if>
            <xsl:text>}}\\&#10;</xsl:text>
        </xsl:if>

        <xsl:text>\end{center}&#10;</xsl:text>
        <xsl:text>}\end{center}&#10;</xsl:text>
        <xsl:text>\vfill&#10;</xsl:text> 
    </xsl:if>

    <xsl:if test="property[@name='typedoc'] != '' or property[@name='intitdoc'] != ''">
        <xsl:text>\vfill&#10;</xsl:text> 
        <xsl:text>\begin{center}\parbox[b][][c]{15cm}{&#10;</xsl:text>
        <xsl:text>\begin{center}\renewcommand{\baselinestretch}{1.4}&#10;</xsl:text>

        <xsl:if test="property[@name='typedoc'] != ''">
            <!-- 2) Title part -->
            <xsl:text>\relsize{2}\uppercase{\textbf{</xsl:text>
	    <xsl:value-of select="$pdfgen.document.typedoc"/>
	    <xsl:if test="not (property[@name='typedos'] != '') and 
		          not (property[@name='intitdos'] != '')
			  and not(property[@name='intitdoc'] != '')
			  and property[@name='docrefs'] != ''">
                <xsl:text>\footnote{</xsl:text>
                <xsl:value-of select="property[@name='docrefs']"/>
                <xsl:text>}&#10;</xsl:text>
            </xsl:if>
            <xsl:text>}} \\[0.5cm]&#10;</xsl:text>
        </xsl:if>
                    
        <xsl:if test="$pdfgen.document.intitdoc != ''">
            <!-- 2) Sub-title part -->
            <xsl:text>\relsize{-2}\textbf{{</xsl:text>
	    <xsl:value-of select="$pdfgen.document.intitdoc"/>
            <xsl:if test="not (property[@name='typedos'] != '') and 
		          not (property[@name='intitdos'] != '') and 
			  property[@name='docrefs'] != ''">
                <xsl:text>\footnote{</xsl:text>
                <xsl:value-of select="property[@name='docrefs']"/>
                <xsl:text>}&#10;</xsl:text>
            </xsl:if>
            <xsl:text>}}\\&#10;</xsl:text>
        </xsl:if>

        <xsl:choose>
            <xsl:when test="$pdfgen.metadata.typedocid = 'AMENDCOM'"/>
            <xsl:when test="$pdfgen.metadata.typedocid = 'AMENDSCE'"/>
            <xsl:when test="property[@name='auteurs'] != ''">
                <!-- 2) Authors part -->
                <xsl:text>\relsize{-1}\uppercase{{</xsl:text>
                <xsl:value-of select="$pdfgen.metadata.depose_par"/>
                <xsl:text>\textbf{</xsl:text>
                <xsl:value-of select="property[@name='auteurs']"/>
                <xsl:text>}}}\\&#10;</xsl:text>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>

        <xsl:text>\end{center}&#10;</xsl:text>
	<xsl:text>}\end{center}&#10;</xsl:text>
        <xsl:text>\vfill&#10;</xsl:text> 
    </xsl:if>
    
    <xsl:if test="property[@name='commentaires'] != ''">
        <xsl:text>\begin{center}\parbox[b][][c]{15cm}{&#10;</xsl:text>
        <xsl:text>\begin{center}\renewcommand{\baselinestretch}{1.4}&#10;</xsl:text>
        <xsl:value-of select="$pdfgen.metadata.commentaires"/>
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\end{center}&#10;</xsl:text>
        <xsl:text>}\end{center}&#10;</xsl:text>
    </xsl:if>

    <!-- Bottom of the page -->

    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\end{changemargin}&#10;</xsl:text>

    <xsl:if test="$with-toc">
      <xsl:text>\newpage&#10;</xsl:text>
      <xsl:text>\tableofcontents&#10;</xsl:text>
      <xsl:text>\newpage&#10;</xsl:text>
      <xsl:if test="//table/title">
        <xsl:text>\listoftables&#10;</xsl:text>
        <xsl:text>\newpage&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="//mediaobject/caption">
        <xsl:text>\listoffigures&#10;</xsl:text>
        <xsl:text>\newpage&#10;</xsl:text>
      </xsl:if>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
