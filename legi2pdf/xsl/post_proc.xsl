<?xml version="1.0" encoding="utf-8"?>
<!--
Tabellio - software suite for deliberative assemblies
         - suite logicielle pour assemblées délibératives
         - http://www.tabellio.org/
Copyright (C) 2006 Parlement de la Communauté française de Belgique

This file is part of Tabellio.

Tabellio is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tabellio is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>
<xsl:output method="text" encoding="utf-8" indent="yes"/>

<!--  FileName: post_proc.xsl                    --> 
<!-- 
      Purpose: Post-processing rules to transform the '&#x20AC;' character (euro)
      to the \euro latex command.
--> 

<xsl:template name="string-replace">
    <xsl:param name="string"/>
    <xsl:param name="pattern"/>
    <xsl:param name="replacement"/>
    <xsl:choose>
        <xsl:when test="$pattern != '' and $string != '' and contains($string, $pattern)">
            <xsl:value-of select="substring-before($string, $pattern)"/>
            <xsl:copy-of select="$replacement"/>
            <xsl:call-template name="string-replace">
                <xsl:with-param name="string" select="substring-after($string, $pattern)"/>
                <xsl:with-param name="pattern" select="$pattern"/>
                <xsl:with-param name="replacement" select="$replacement"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$string"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<xsl:template match="//text()">
 <xsl:call-template name="string-replace">
  <xsl:with-param name="string">
   <xsl:call-template name="string-replace">
    <xsl:with-param name="pattern">&#x2019;</xsl:with-param>
    <xsl:with-param name="replacement">'</xsl:with-param>
    <xsl:with-param name="string">
     <xsl:call-template name="string-replace">
      <xsl:with-param name="pattern">&#x201C;</xsl:with-param>
      <xsl:with-param name="replacement">\guillemotleft</xsl:with-param>
      <xsl:with-param name="string">
       <xsl:call-template name="string-replace">
        <xsl:with-param name="pattern">&#x201D;</xsl:with-param>
        <xsl:with-param name="replacement">\guillemotright</xsl:with-param>
        <xsl:with-param name="string">
         <xsl:call-template name="string-replace">
          <xsl:with-param name="string" select="."/>
          <xsl:with-param name="pattern">&#x2011;</xsl:with-param>
          <xsl:with-param name="replacement">-</xsl:with-param>
         </xsl:call-template>
        </xsl:with-param>
       </xsl:call-template>
      </xsl:with-param>
     </xsl:call-template>
    </xsl:with-param>
   </xsl:call-template>
  </xsl:with-param>
 </xsl:call-template>
</xsl:template>

<xsl:template match="/root">
    <xsl:apply-templates/>
</xsl:template> 


</xsl:stylesheet>
