<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<xsl:variable name="pdfgen.metadata.keyword" select="/book/metadata/property[@name='keyword']"/>

<xsl:variable name="pdfgen.parlement">
  <xsl:choose>
    <xsl:when test="$pdfgen.metadata.keyword = 'Tabellio/PFB'">PFB</xsl:when>
    <xsl:otherwise>PCF</xsl:otherwise>
  </xsl:choose>
</xsl:variable>

</xsl:stylesheet>
