<?xml version="1.0" encoding="utf-8"?>
<!--
Tabellio - software suite for deliberative assemblies
         - suite logicielle pour assemblées délibératives
         - http://www.tabellio.org/
Copyright (C) 2006 Parlement de la Communauté française de Belgique

This file is part of Tabellio.

Tabellio is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tabellio is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!--  FileName: section.xsl                    --> 
<!-- 
      Purpose: Correct Db2Latex product to take into account the footnote 
      inside the title of a section.
      Instead of \section{title}, this template generates \section[title-for-toc]{title}
      'title-for-toc' (text for table of content) is similar as 'title' but without footnote.
--> 

<!-- templates customization of 'toc' mode; use for 'title-for-toc' definition --> 
<xsl:template match="footnote" mode="toc"/>
<xsl:template match="ulink" mode="toc"/>
<xsl:template match="link" mode="toc"/>
<xsl:template match="anchor" mode="toc"/>
<xsl:template match="ref" mode="toc"/>
<xsl:template match="br" mode="toc"><xsl:text> </xsl:text></xsl:template>
<xsl:param name="bqr"/>
<xsl:param name="parchemin"/>


<!-- template for section node --> 

<xsl:template match="synthese">
  <!-- do nothing, the synthese has special handling in the first page -->
</xsl:template>


<xsl:template match="preface">
    <xsl:if test="(parent::book or parent::article) and not($parchemin)">
        <xsl:text>% straight preface, no part&#10;</xsl:text>
        <xsl:text>\begin{multicols}{2}&#10;</xsl:text>
    </xsl:if>
    <xsl:if test="./title">
        <xsl:call-template name="map.begin"/>
    </xsl:if>
    <xsl:call-template name="content-templates"/>
    <xsl:call-template name="map.end"/>
    <xsl:if test="following-sibling::part and not($parchemin)">
        <!-- preface *before* part, shouldn't exist, but force the closing of
             the multicols environement, just in case -->
        <xsl:text>\end{multicols}&#10;</xsl:text>
    </xsl:if>
</xsl:template>



<xsl:template name="extract.object.subtitle">
    <xsl:param name="object"  select="."/>
    <xsl:choose>
        <xsl:when test="$latex.apply.title.templates='1'">
            <xsl:apply-templates select="$object/subtitle" mode="latex"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="normalize-scape">
                <xsl:with-param name="string" select="$object/subtitle"/>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template> 



<xsl:template match="section">
    <xsl:param name="bridgehead" select="ancestor::preface|ancestor::colophon|ancestor::dedication"/>
    <xsl:param name="appendix" select="ancestor::appendix"/>
    <xsl:param name="level" select="count(ancestor::section)+1"/>
    <xsl:param name="abslevel" select="count(ancestor::*)"/>
    <xsl:param name="string">
        <xsl:call-template name="extract.object.title">
            <xsl:with-param name="object" select="."/>
        </xsl:call-template>
    </xsl:param>
    <xsl:param name="use.label"	select="1"/>
    <xsl:param name="use.hypertarget" 	select="1"/>
    
    <xsl:param name="title-toc">
      <xsl:call-template name="escape-percent">
       <xsl:with-param name="string">
        <xsl:choose>
            <xsl:when test='$level = 2'>
                <xsl:choose>
                    <xsl:when test='$bqr'>
                        <xsl:apply-templates select="./bqr_objet" mode="toc"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="./title" mode="toc"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>

            <xsl:otherwise>
                <xsl:apply-templates select="./title" mode="toc"/>
            </xsl:otherwise>
        </xsl:choose>
       </xsl:with-param>
      </xsl:call-template>
    </xsl:param>

    <xsl:param name="section-tag">
        <xsl:choose>
            <xsl:when test='($level = 1) and ($bqr)'>
                <xsl:text>bqrsection</xsl:text>
            </xsl:when>
            <xsl:when test='($level = 1) and not($bqr)'>
                <xsl:text>section</xsl:text>
            </xsl:when>
            <xsl:when test='$level = 2'>
                <xsl:text>subsection</xsl:text>
            </xsl:when>
            <xsl:when test='$level = 3'>
                <xsl:text>subsubsection</xsl:text>
            </xsl:when>
            <xsl:when test='$level = 4'>
                <xsl:text>paragraph</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>subparagraph</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    <xsl:param name="begin-param-tag">
        <xsl:choose>
            <xsl:when test='($level = 1) and ($bqr)'>
                <xsl:text>{</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>[</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    <xsl:param name="end-param-tag">
        <xsl:choose>
            <xsl:when test='($level = 1) and ($bqr)'>
                <xsl:text>}</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>]</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:param>

    <xsl:variable name="id">
        <xsl:call-template name="generate.label.id">
            <xsl:with-param name="object" select="."/>
        </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="title">
        <xsl:choose>
            <xsl:when test="starts-with(local-name(.),'informal')">
                <xsl:if test="$string!=''">
                    <xsl:message>Ignoring title for <xsl:value-of select="local-name($object)"/>.</xsl:message>
                </xsl:if>
            </xsl:when>
            <xsl:when test="$string=''">
                <xsl:call-template name="gentext.element.name"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="normalize-space($string)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:if test="(parent::book or parent::article) and not (preceding::section or preceding::preface) and not($parchemin)">
        <xsl:text>% straight to section, no part, no preface&#10;</xsl:text>
        <xsl:text>\begin{multicols}{2}&#10;</xsl:text>
    </xsl:if>

    <xsl:if test="not(node()[position()=2])">
        <xsl:text>\pagebreak[2]&#10;</xsl:text>
    </xsl:if>

    <xsl:text>&#10;% ------------------------   &#10;</xsl:text>
    <xsl:text>% begin section (level: </xsl:text>
    <xsl:value-of select="$level"/>
    <xsl:text>)&#10;</xsl:text>
    <!--<xsl:if test="$appendix"><xsl:text>\tocless</xsl:text></xsl:if>-->
    <xsl:if test="$section-tag = 'bqrsection' and not($parchemin)">
      <xsl:text>\end{multicols}&#10;</xsl:text>
    </xsl:if>
    <xsl:if test="$section-tag = 'bqrsection' and (preceding-sibling::section)">
      <xsl:text>\newpage&#10;</xsl:text>
    </xsl:if>
    <xsl:text>\</xsl:text>
    <xsl:value-of select="$section-tag"/>
    <xsl:if test="$appendix"><xsl:text>*</xsl:text></xsl:if>
    <xsl:if test="$bridgehead"><xsl:text>*</xsl:text></xsl:if>
    <xsl:if test="not($appendix)">
      <xsl:value-of select="$begin-param-tag"/>
      <xsl:value-of select="$title-toc"/>
      <xsl:value-of select="$end-param-tag"/>
    </xsl:if>
    <xsl:text>{</xsl:text>
    <xsl:value-of select="$title"/>
    <xsl:text>}&#10;\label{</xsl:text>
    <xsl:value-of select="$id"/>
    <xsl:text>}\hypertarget{</xsl:text>
    <xsl:value-of select="$id"/>
    <xsl:text>}{}%&#10;</xsl:text>
    <xsl:if test="$abslevel = 1 and not(parent::book) and not($parchemin)">
        <xsl:text>% level 1&#10;</xsl:text>
        <xsl:text>\begin{multicols}{2}&#10;</xsl:text>
    </xsl:if>
    <xsl:if test="$section-tag = 'bqrsection' and not($parchemin)">
        <xsl:text>\begin{multicols}{2}&#10;</xsl:text>
    </xsl:if>
    
    <xsl:call-template name="content-templates"/>

    <xsl:if test="$abslevel = 1 and not(parent::book) and not($parchemin)">
        <xsl:text>\end{multicols}&#10;</xsl:text>
    </xsl:if>
    <xsl:text>% end section (level: </xsl:text>
    <xsl:value-of select="$level"/>
    <xsl:text>)&#10;</xsl:text>
    <xsl:text>% ------------------------   &#10;</xsl:text>

    <!-- end{multicols} when there is no part, and it is the last section -->
    <xsl:if test="(parent::book or parent::article) and not (following::section) and not ($parchemin)">
        <xsl:text>% end of document without part&#10;</xsl:text>
        <xsl:text>\end{multicols}&#10;</xsl:text>
    </xsl:if>

</xsl:template>

<xsl:template match="nosection">
    <xsl:if test="not($parchemin)">
      <xsl:text>\begin{multicols}{2}&#10;</xsl:text>
    </xsl:if>
    <xsl:call-template name="content-templates"/>
    <xsl:if test="not($parchemin)">
      <xsl:choose>
        <!-- if it's a document getting straight to a legistic level we keep
             the columns open and they'll get closed when the end-of-document
             processing instruction will be met.
          -->
        <xsl:when test="following-sibling::legistic_part"></xsl:when>
        <xsl:when test="following-sibling::legistic_title"></xsl:when>
        <xsl:when test="following-sibling::legistic_chapter"></xsl:when>
        <xsl:when test="following-sibling::legistic_section"></xsl:when>
        <xsl:otherwise>
          <xsl:text>\end{multicols}&#10;</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
</xsl:template>

<xsl:template match="processing-instruction('end-of-document')">
  <xsl:if test="not($parchemin)">
     <!-- columns have been left open after <nosection/>, they must be closed
          now that the end of the document has been reached.
      -->
    <xsl:if test="//nosection/following-sibling::legistic_part"><xsl:text>\end{multicols}&#10;</xsl:text></xsl:if>
    <xsl:if test="//nosection/following-sibling::legistic_title"><xsl:text>\end{multicols}&#10;</xsl:text></xsl:if>
    <xsl:if test="//nosection/following-sibling::legistic_chapter"><xsl:text>\end{multicols}&#10;</xsl:text></xsl:if>
    <xsl:if test="//nosection/following-sibling::legistic_section"><xsl:text>\end{multicols}&#10;</xsl:text></xsl:if>
  </xsl:if>
</xsl:template>

<xsl:template match="part">
    <xsl:param name="bridgehead" select="ancestor::preface|ancestor::colophon|ancestor::dedication"/>
    <xsl:param name="string">
        <xsl:call-template name="extract.object.title">
            <xsl:with-param name="object" select="."/>
        </xsl:call-template>
    </xsl:param>
    <xsl:param name="use.label"	select="1"/>
    <xsl:param name="use.hypertarget" 	select="1"/>
    
    <xsl:param name="subtitle">
        <xsl:call-template name="extract.object.subtitle">
            <xsl:with-param name="object" select="."/>
        </xsl:call-template>
    </xsl:param>
    <xsl:param name="title-toc">
        <xsl:apply-templates select="./title" mode="toc"/>
    </xsl:param>
    <xsl:param name="subtitle-toc">
        <xsl:apply-templates select="./subtitle" mode="toc"/>
    </xsl:param>
    <xsl:param name="appendix" select="ancestor::appendix"/>

    <xsl:variable name="id">
        <xsl:call-template name="generate.label.id">
            <xsl:with-param name="object" select="."/>
        </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="title">
        <xsl:choose>
            <xsl:when test="starts-with(local-name(.),'informal')">
                <xsl:if test="$string!=''">
                    <xsl:message>Ignoring title for <xsl:value-of select="local-name($object)"/>.</xsl:message>
                </xsl:if>
            </xsl:when>
            <xsl:when test="$string=''">
                <xsl:call-template name="gentext.element.name"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="normalize-space($string)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    
    <xsl:if test="starts-with(translate($title-toc, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 'AVANT-PROJET DE ')">
        <xsl:text>\sfont&#10;</xsl:text>
    </xsl:if>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>%\balance&#10;</xsl:text>
    <xsl:text>% -------------------------------------------------------------   &#10;</xsl:text>
    <xsl:text>% PART:  </xsl:text>
    <xsl:value-of select="$title-toc"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>%&#10;</xsl:text>
    <xsl:text>% -------------------------------------------------------------   &#10;</xsl:text>

    <xsl:text>\setcounter{section}{0}&#10;</xsl:text>
    <xsl:choose>
        <xsl:when test='$bqr'>
            <xsl:text>\bqrpart{</xsl:text>
            <xsl:value-of select="$title-toc"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="$title"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="$subtitle-toc"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="$subtitle"/>
            <xsl:text>}&#10;</xsl:text>
            <xsl:text>\label{</xsl:text>
            <xsl:value-of select="$id"/>
            <xsl:text>}\hypertarget{</xsl:text>
            <xsl:value-of select="$id"/>
            <xsl:text>}{}%&#10;</xsl:text>
            <xsl:if test="not($parchemin)">
              <xsl:text>\begin{multicols}{2}</xsl:text>
            </xsl:if>
        </xsl:when>
        <xsl:otherwise>
            <xsl:text>\newpage&#10;</xsl:text>
            <xsl:if test="$appendix"><xsl:text>\pcfpartappendix{</xsl:text></xsl:if>
            <xsl:if test="not($appendix)"><xsl:text>\pcfpart{</xsl:text></xsl:if>
            <xsl:value-of select="$title-toc"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="$title"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="$subtitle-toc"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="$subtitle"/>
            <xsl:text>}&#10;</xsl:text>
            <xsl:text>\label{</xsl:text>
            <xsl:value-of select="$id"/>
            <xsl:text>}\hypertarget{</xsl:text>
            <xsl:value-of select="$id"/>
            <xsl:text>}{}</xsl:text>
            <xsl:text>% part&#10;</xsl:text>
            <xsl:if test="not($parchemin)">
              <xsl:text>\begin{multicols}{2}</xsl:text>
            </xsl:if>
        </xsl:otherwise>
    </xsl:choose>

    <xsl:call-template name="content-templates"/>
    <xsl:text>% end part &#10;</xsl:text>
    <xsl:text>% ------------------------   &#10;</xsl:text>
    <xsl:if test="starts-with(translate($title-toc, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 'AVANT-PROJET DE ')">
        <xsl:text>\nfont&#10;</xsl:text>
    </xsl:if>
    <xsl:if test="not($parchemin)">
      <xsl:text>%\balance&#10;</xsl:text>
      <xsl:text>\end{multicols}&#10;</xsl:text>
    </xsl:if>
</xsl:template>


</xsl:stylesheet>
