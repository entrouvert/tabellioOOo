<?xml version="1.0" encoding="utf-8"?>
<!--
Tabellio - software suite for deliberative assemblies
         - suite logicielle pour assemblées délibératives
         - http://www.tabellio.org/
Copyright (C) 2006 Parlement de la Communauté française de Belgique

This file is part of Tabellio.

Tabellio is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tabellio is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!--  FileName: first_page.xsl                    --> 
<!-- 
      Purpose: Templates to define first page of a document taking 
      into account the metadata.
--> 

<xsl:param name="with-toc"/>
<xsl:param name="parchemin"/>
<xsl:param name="bqr"/>

<xsl:variable name="pdfgen.metadata.no" select="/book/metadata/property[@name='no']"/>
<xsl:variable name="pdfgen.metadata.sess" select="/book/metadata/property[@name='sess']"/>
<xsl:variable name="pdfgen.metadata.nodoc" select="/book/metadata/property[@name='nodoc']"/>
<xsl:variable name="pdfgen.metadata.docsess" select="/book/metadata/property[@name='docsess']"/>
<xsl:variable name="pdfgen.metadata.anx" select="/book/metadata/property[@name='anx']"/>
<xsl:variable name="pdfgen.metadata.moreno" select="/book/metadata/property[@name='moreno']"/>
<xsl:variable name="pdfgen.metadata.date" select="/book/metadata/property[@name='date']"/>
<xsl:variable name="pdfgen.metadata.typedocid" select="/book/metadata/property[@name='typedocid']"/>
<xsl:variable name="pdfgen.metadata.session" select="/book/metadata/property[@name='docsess']"/>
<xsl:variable name="pdfgen.metadata.intitdos">
    <xsl:call-template name="escape-percent">
        <xsl:with-param name="string">
            <xsl:call-template name="footnote-syntax-replace">
                <xsl:with-param name="string" select="/book/metadata/property[@name='intitdos']"/>
            </xsl:call-template>
        </xsl:with-param>
    </xsl:call-template>
</xsl:variable>
<xsl:variable name="pdfgen.metadata.intitdoc">
    <xsl:call-template name="escape-percent">
        <xsl:with-param name="string">
            <xsl:call-template name="footnote-syntax-replace">
                <xsl:with-param name="string" select="/book/metadata/property[@name='intitdoc']"/>
            </xsl:call-template>
        </xsl:with-param>
    </xsl:call-template>
</xsl:variable>
<xsl:variable name="pdfgen.metadata.typedos">
    <xsl:call-template name="escape-percent">
        <xsl:with-param name="string">
            <xsl:call-template name="footnote-syntax-replace-protect-from-uppercase">
                <xsl:with-param name="string" select="/book/metadata/property[@name='typedos']"/>
            </xsl:call-template>
        </xsl:with-param>
    </xsl:call-template>
</xsl:variable>
<xsl:variable name="pdfgen.metadata.typedoc">
    <xsl:call-template name="escape-percent">
        <xsl:with-param name="string">
            <xsl:call-template name="footnote-syntax-replace-protect-from-uppercase">
                <xsl:with-param name="string" select="/book/metadata/property[@name='typedoc']"/>
            </xsl:call-template>
        </xsl:with-param>
    </xsl:call-template>
</xsl:variable>
<xsl:variable name="pdfgen.metadata.commentaires">
    <xsl:call-template name="escape-percent">
        <xsl:with-param name="string">
            <xsl:call-template name="footnote-syntax-replace">
                <xsl:with-param name="string" select="/book/metadata/property[@name='commentaires']"/>
            </xsl:call-template>
        </xsl:with-param>
    </xsl:call-template>
</xsl:variable>
<xsl:variable name="pdfgen.metadata.tailletitres" select="/book/metadata/property[@name='tailletitres']"/>
<xsl:variable name="pdfgen.metadata.remplace" select="/book/metadata/property[@name='remplace']"/>
<xsl:variable name="pdfgen.metadata.comname" select="/book/metadata/property[@name='comname']"/>

<xsl:variable name="pdfgen.document.intitdoc">
    <xsl:choose>
        <xsl:when test="starts-with($pdfgen.metadata.intitdoc,'spécial')">
            <xsl:value-of select="substring-after($pdfgen.metadata.intitdoc,'spécial')"/>
        </xsl:when>
        <xsl:when test="$pdfgen.metadata.typedocid = 'AMENDCOM'">
            <xsl:text>\uppercase{déposé(s) en commission}</xsl:text>
        </xsl:when>
        <xsl:when test="$pdfgen.metadata.typedocid = 'AMENDSCE'">
            <xsl:text>\uppercase{déposé(s) en séance}</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$pdfgen.metadata.intitdoc"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="pdfgen.document.typedoc">
    <xsl:choose>
        <xsl:when test="starts-with($pdfgen.metadata.intitdoc,'spécial')">
            <xsl:value-of select="$pdfgen.metadata.typedoc"/>
            <xsl:text> spécial</xsl:text>
        </xsl:when>
        <xsl:when test="starts-with($pdfgen.metadata.typedocid, 'AMEND')">
            <xsl:text>Amendement(s)</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$pdfgen.metadata.typedoc"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="pdfgen.document.session">
    <xsl:choose>
        <xsl:when test="starts-with($pdfgen.metadata.session,'SE')">
            <xsl:text>Session extraordinaire de </xsl:text>
            <xsl:value-of select="substring-after($pdfgen.metadata.session,'SE')"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:text>Session </xsl:text>
            <xsl:value-of select="$pdfgen.metadata.session"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="pdfgen.metadata.no.prefix">
    <xsl:choose>
        <xsl:when test="$pdfgen.metadata.typedocid = 'CR'">
            <!-- for CR at PFB -->
            <xsl:text>C.R. N\textsuperscript{o}</xsl:text>
        </xsl:when>
        <xsl:when test="$pdfgen.metadata.typedocid = 'CRI'">
            <xsl:text>CRI N\textsuperscript{o}</xsl:text>
        </xsl:when>
        <xsl:when test="$pdfgen.metadata.typedocid = 'CRICOM'">
            <xsl:text>CRIc N\textsuperscript{o}</xsl:text>
        </xsl:when>
        <xsl:when test="$pdfgen.metadata.typedocid = 'BQR'">
            <xsl:text>N\textsuperscript{o}</xsl:text>
        </xsl:when>
        <xsl:otherwise>
        </xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="pdfgen.metadata.no.prefix.plain">
    <xsl:choose>
        <xsl:when test="$pdfgen.metadata.typedocid = 'CR'">
            <!-- for CR at PFB -->
            <xsl:text>C.R. N</xsl:text>
        </xsl:when>
        <xsl:when test="$pdfgen.metadata.typedocid = 'CRI'">
            <xsl:text>CRI N</xsl:text>
        </xsl:when>
        <xsl:when test="$pdfgen.metadata.typedocid = 'CRICOM'">
            <xsl:text>CRIc N</xsl:text>
        </xsl:when>
        <xsl:when test="$pdfgen.metadata.typedocid = 'BQR'">
            <xsl:text>N</xsl:text>
        </xsl:when>
        <xsl:otherwise>
        </xsl:otherwise>
    </xsl:choose>
</xsl:variable>


<xsl:variable name="pdfgen.metadata.depose_par">
    <xsl:choose>
        <xsl:when test="$pdfgen.metadata.typedocid = 'PPD'">
            <xsl:text>déposée par </xsl:text>
        </xsl:when>
        <xsl:when test="$pdfgen.metadata.typedocid = 'DG-PPMS'">
            <xsl:text>déposée par </xsl:text>
        </xsl:when>
        <xsl:when test="$pdfgen.metadata.typedocid = 'PPMR'">
            <xsl:text>déposée par </xsl:text>
        </xsl:when>
        <xsl:when test="$pdfgen.metadata.typedocid = 'DG-PPR'">
            <xsl:text>déposée par </xsl:text>
        </xsl:when>
        <xsl:when test="$pdfgen.parlement = 'PFB'">
            <xsl:text>déposé(e) par </xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:text>par </xsl:text>
        </xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="pdfgen.document.id">
    <xsl:text>\textbf{</xsl:text>
    <xsl:value-of select="$pdfgen.metadata.no.prefix"/>
    <xsl:value-of select="$pdfgen.metadata.no"/>
    <xsl:choose>
      <xsl:when test="$pdfgen.parlement = 'PFB'">
        <!-- keep going bold -->
        <xsl:text> (</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>} (</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="$pdfgen.metadata.sess"/>
    <xsl:text>)</xsl:text>
    <xsl:if test="/book/metadata/property[@name='nodoc']">
      <xsl:choose>
        <xsl:when test="$pdfgen.parlement = 'PFB'">
          <xsl:text> n\textsuperscript{o} </xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text> --- N\textsuperscript{o} </xsl:text>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="$pdfgen.metadata.nodoc"/>
    </xsl:if>
    <xsl:if test="/book/metadata/property[@name='anx']">
        <xsl:text> (Annexe </xsl:text>
	<xsl:value-of select="$pdfgen.metadata.anx"/>
        <xsl:text>)  </xsl:text>
    </xsl:if>
    <xsl:if test="$pdfgen.parlement = 'PFB'">
        <xsl:if test="/book/metadata/property[@name='moreno']">
          <xsl:value-of select="$pdfgen.metadata.moreno"/>
        </xsl:if>
        <!-- finally close the textbf -->
        <xsl:text>}</xsl:text>
    </xsl:if>
</xsl:variable>

<xsl:variable name="pdfgen.document.title.id">
    <xsl:text>\textbf{</xsl:text>
    <xsl:value-of select="$pdfgen.metadata.no.prefix"/>
    <xsl:value-of select="$pdfgen.metadata.no"/>
    <xsl:choose>
      <xsl:when test="$pdfgen.parlement = 'PFB'">
        <!-- keep going bold -->
        <xsl:text> (</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>} (</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="$pdfgen.metadata.sess"/>
    <xsl:text>)</xsl:text>
    <xsl:if test="/book/metadata/property[@name='nodoc']">
      <xsl:choose>
        <xsl:when test="$pdfgen.parlement = 'PFB'">
          <xsl:text> n\textsuperscript{o} </xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text> --- N\textsuperscript{o} </xsl:text>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="$pdfgen.metadata.nodoc"/>
    </xsl:if>
    <xsl:if test="/book/metadata/property[@name='anx']">
        <xsl:text> (Annexe </xsl:text>
	<xsl:value-of select="$pdfgen.metadata.anx"/>
        <xsl:text>)  </xsl:text>
    </xsl:if>
</xsl:variable>

<xsl:variable name="pdfgen.document.tailletitres">
    <xsl:choose>
        <xsl:when test="not($pdfgen.metadata.tailletitres) or ( $pdfgen.metadata.tailletitres = '' )">
            <xsl:text>0</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$pdfgen.metadata.tailletitres"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:variable>


<xsl:template match="metadata">
    <xsl:choose>
       <xsl:when test="$pdfgen.parlement = 'PFB'">
           <xsl:call-template name="pfb-first-page-normal"/>
       </xsl:when>
       <xsl:otherwise>
            <xsl:choose>
                <xsl:when test="$parchemin">
                    <xsl:call-template name="first-page-parchemin">
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$bqr">
                    <xsl:call-template name="first-page-bqr">
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="first-page-normal">
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="first-page-parchemin">
    <!-- First part of the page -->

    <xsl:text>\pagestyle{empty}&#10;</xsl:text>
    <xsl:text>\vspace*{-1.5cm}&#10;</xsl:text>

    <!-- Main title with the session reference -->
    <xsl:text>\begin{changemargin}{0cm}{0cm}&#10;</xsl:text>
    <xsl:text>\fontfamily{pun}\fontseries{m}\selectfont&#10;</xsl:text>
    <xsl:text>\begin{minipage}[b][10cm][c]{15cm}&#10;</xsl:text>
    <xsl:text>\begin{center}&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\Huge\textbf{{Le Parlement}} \\&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\Huge\textbf{{de la Communaut\'{e} fran\c{c}aise}} \\&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\Huge\textbf{{a adopt\'{e}}} \\&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\Huge\textbf{{et Nous, Gouvernement,}} \\&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\Huge\textbf{{sanctionnons ce qui suit:}} \\&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{center}&#10;</xsl:text>
    <xsl:text>\end{minipage}&#10;</xsl:text>
    <xsl:text>\end{changemargin}&#10;</xsl:text>
    <xsl:text>\normalfont&#10;</xsl:text>

    <!-- HACK to prevent bad aligment in changemargin environment -->
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\renewenvironment{changemargin}[2]{\begin{list}{}{&#10;</xsl:text>
    <xsl:text>\setlength{\topsep}{0pt}%&#10;</xsl:text>
    <xsl:text>\setlength{\leftmargin}{0pt}%&#10;</xsl:text>
    <xsl:text>\setlength{\rightmargin}{0pt}%&#10;</xsl:text>
    <xsl:text>\setlength{\listparindent}{\parindent}%&#10;</xsl:text>
    <xsl:text>\setlength{\itemindent}{0pt}%&#10;</xsl:text> <!-- HACK IS HERE -->
    <xsl:text>\setlength{\parsep}{0.2cm plus 1pt}%&#10;</xsl:text>
    <xsl:text>\addtolength{\leftmargin}{#1}%&#10;</xsl:text>
    <xsl:text>\addtolength{\rightmargin}{#2}%&#10;</xsl:text>
    <xsl:text>}\item }{\end{list}}%&#10;</xsl:text>

    <xsl:text>\begin{changemargin}{4.25cm}{4.25cm}&#10;</xsl:text>
</xsl:template>


<xsl:template name="first-page-bqr">
    <!-- First part of the page -->

    <xsl:text>\thispagestyle{empty}&#10;</xsl:text>
    <xsl:text>\vspace*{-1.5cm}&#10;</xsl:text>

    <!-- Header part definition -->
    <xsl:text>\begin{changemargin}{-0.9cm}{-0.6cm}&#10;</xsl:text>
    <xsl:text>\fontfamily{pun}\fontseries{m}\selectfont&#10;</xsl:text>
    <xsl:text>\noindent\large{</xsl:text>
    <xsl:value-of select="$pdfgen.document.title.id"/>
    <xsl:text>}</xsl:text>
    <xsl:text>\hfill\large{</xsl:text>
    <xsl:value-of select="$pdfgen.document.title.id"/>
    <xsl:text>}</xsl:text>
    <xsl:text>\\[-0.3cm]&#10;</xsl:text>
    <xsl:text>\rule{17.5cm}{0.04cm}&#10;</xsl:text>

    <!-- Main title with the session reference -->
    <xsl:text>\normalfont&#10;</xsl:text>
    <xsl:text>\begin{minipage}[b][7.6cm][c]{17.5cm}&#10;</xsl:text>
    <xsl:text>\begin{center}&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\textbf{&#10;</xsl:text>
    <xsl:text>\Huge\textmd{{PARLEMENT}} \\&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\LARGE\textmd{{DE LA}} \\&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\Huge\textmd{{COMMUNAUT\'{E} FRAN\c{C}AISE}} \\&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\fontfamily{pun}\fontseries{m}\selectfont&#10;</xsl:text>

    <xsl:text>\Large\textmd{{</xsl:text>
    <xsl:value-of select="$pdfgen.document.session"/>
    <xsl:text>}}}&#10;</xsl:text>
    
    <xsl:text>\normalfont&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\end{center}&#10;</xsl:text>
    <xsl:text>\end{minipage}&#10;</xsl:text>
    <xsl:text>\begin{minipage}[b][0.8cm][s]{17.5cm}&#10;</xsl:text>
    <xsl:text>\rule{17.5cm}{0.04cm}&#10;</xsl:text>
    <xsl:text>\fontfamily{pun}\fontseries{m}\selectfont&#10;</xsl:text>
    <xsl:text>\center\uppercase{{</xsl:text>
    <xsl:value-of select="property[@name='date']"/>
    <xsl:text>}}\\[-0.3cm]&#10;</xsl:text>
    <xsl:text>\normalfont&#10;</xsl:text>
    <xsl:text>\rule{17.5cm}{0.04cm}&#10;</xsl:text>
    <xsl:text>\end{minipage}&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>

    <!-- First titleMain title with the session reference -->

    <xsl:text>\vfill&#10;</xsl:text> 
    <xsl:text>\begin{center}\parbox[b][][c]{15cm}{&#10;</xsl:text>
    <xsl:text>\begin{center}&#10;</xsl:text>

    <xsl:text>\fontfamily{pun}\fontseries{m}\selectfont&#10;</xsl:text>
    <xsl:text>\LARGE\uppercase{{BULLETIN DES QUESTIONS ET REPONSES}}&#10;</xsl:text>
    <xsl:text>\end{center}&#10;</xsl:text>
    <xsl:text>}\end{center}&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text> 
    
    <!-- Bottom of the page -->
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\end{changemargin}&#10;</xsl:text>
    <xsl:text>\newpage&#10;</xsl:text>

    <xsl:if test="$with-toc">
      <xsl:text>\tableofcontents&#10;</xsl:text>
      <xsl:text>\newpage&#10;</xsl:text>
      <xsl:if test="(//table/title) and not($bqr)">
        <xsl:text>\listoftables&#10;</xsl:text>
        <xsl:text>\newpage&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="(//mediaobject/caption) and not($bqr)">
        <xsl:text>\listoffigures&#10;</xsl:text>
        <xsl:text>\newpage&#10;</xsl:text>
      </xsl:if>
  </xsl:if>

</xsl:template>



<xsl:template name="first-page-normal">
    <!-- First part of the page -->

    <xsl:text>\thispagestyle{empty}&#10;</xsl:text>
    <xsl:text>\vspace*{-1.5cm}&#10;</xsl:text>

    <!-- Header part definition -->
    <xsl:text>\begin{changemargin}{-0.9cm}{-0.6cm}&#10;</xsl:text>
    <xsl:if test="$pdfgen.metadata.remplace = 'true'">
        <xsl:text>\vspace*{-12pt}&#10;</xsl:text>
        <xsl:text>\begin{center}&#10;</xsl:text>
        <xsl:text>\noindent{\textmd{\textbf{\uppercase{Ce document remplace et annule le document distribué précédemment}}}}\\&#10;</xsl:text>
        <xsl:text>\end{center}&#10;</xsl:text>
    </xsl:if>
    <xsl:text>\fontfamily{pun}\fontseries{m}\selectfont&#10;</xsl:text>
    <xsl:text>\noindent\large{</xsl:text>
    <xsl:value-of select="$pdfgen.document.title.id"/>
    <xsl:text>}</xsl:text>
    <xsl:text>\hfill\large{</xsl:text>
    <xsl:value-of select="$pdfgen.document.title.id"/>
    <xsl:text>}</xsl:text>
    <xsl:text>\\[-0.3cm]&#10;</xsl:text>
    <xsl:text>\rule{17.5cm}{0.04cm}&#10;</xsl:text>

    <!-- Main title with the session reference -->
    <xsl:text>\normalfont&#10;</xsl:text>
    <xsl:choose>
        <xsl:when test="$pdfgen.metadata.typedocid = 'CRICOM'">
           <xsl:text>\begin{minipage}[b][10.1cm][c]{17.5cm}&#10;</xsl:text>
        </xsl:when>
        <xsl:otherwise>
           <xsl:text>\begin{minipage}[b][7.6cm][c]{17.5cm}&#10;</xsl:text>
        </xsl:otherwise>
    </xsl:choose>

    <xsl:text>\begin{center}&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\textbf{&#10;</xsl:text>
    <xsl:if test="$pdfgen.metadata.typedocid = 'CRICOM'">
        <xsl:if test="$pdfgen.metadata.comname">
            <xsl:text>\huge\textmd{{</xsl:text>
            <xsl:value-of select="$pdfgen.metadata.comname"/>
            <xsl:text>}} \\&#10;</xsl:text>
            <xsl:text>\vfill&#10;</xsl:text>
            <xsl:text>\vfill&#10;</xsl:text>
        </xsl:if>
    </xsl:if>
    <xsl:text>\Huge\textmd{{PARLEMENT}} \\&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\LARGE\textmd{{DE LA}} \\&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\Huge\textmd{{COMMUNAUT\'{E} FRAN\c{C}AISE}} \\&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\fontfamily{pun}\fontseries{m}\selectfont&#10;</xsl:text>

    <xsl:text>\Large\textmd{{</xsl:text>
    <xsl:value-of select="$pdfgen.document.session"/>
    <xsl:text>}}}&#10;</xsl:text>
    
    <xsl:text>\normalfont&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>
    <xsl:text>\end{center}&#10;</xsl:text>
    <xsl:text>\end{minipage}&#10;</xsl:text>
    <xsl:text>\begin{minipage}[b][0.8cm][s]{17.5cm}&#10;</xsl:text>
    <xsl:text>\rule{17.5cm}{0.04cm}&#10;</xsl:text>
    <xsl:text>\fontfamily{pun}\fontseries{m}\selectfont&#10;</xsl:text>
    <xsl:text>\center\uppercase{{</xsl:text>
    <xsl:value-of select="property[@name='date']"/>
    <xsl:text>}}\\[-0.3cm]&#10;</xsl:text>
    <xsl:text>\normalfont&#10;</xsl:text>
    <xsl:text>\rule{17.5cm}{0.04cm}&#10;</xsl:text>
    <xsl:text>\end{minipage}&#10;</xsl:text>
    <xsl:text>\vfill&#10;</xsl:text>

    <!-- First titleMain title with the session reference -->
    
    <xsl:if test="property[@name='typedos'] != '' or property[@name='intitdos'] != ''">
        <xsl:text>\vfill&#10;</xsl:text>
        <xsl:text>\vfill&#10;</xsl:text>
        <xsl:text>\relsize{</xsl:text>
        <xsl:value-of select="$pdfgen.document.tailletitres"/>
        <xsl:text>}&#10;</xsl:text>
        <xsl:text>\begin{center}\parbox[b][][c]{15cm}{&#10;</xsl:text>
        <xsl:text>\begin{center}\renewcommand{\baselinestretch}{1.4}&#10;</xsl:text>

        <xsl:if test="property[@name='typedos'] != ''">
            <!-- 2) Title part -->
            <xsl:text>\fontfamily{pun}\fontseries{m}\selectfont\relsize{2}\uppercase{</xsl:text>
            <xsl:value-of select="$pdfgen.metadata.typedos"/>
            <xsl:if test="not(property[@name='intitdos'] != '') and property[@name='docrefs'] != ''">
                <xsl:text>\footnote{</xsl:text>
                  <xsl:call-template name="docrefs-syntax-replace">
                    <xsl:with-param name="string" select="property[@name='docrefs']"/>
                  </xsl:call-template>
                <xsl:text>}&#10;</xsl:text>
            </xsl:if>
            <xsl:text>} \\[0.5cm]&#10;</xsl:text>
        </xsl:if>
                    
        <xsl:if test="property[@name='intitdos'] != ''">
            <!-- 2) Sub-title part -->
            <xsl:text>\fontfamily{pun}\fontseries{m}\selectfont\relsize{-2}\textsc{{</xsl:text>
            <xsl:value-of select="$pdfgen.metadata.intitdos"/>
            
            <xsl:if test="property[@name='docrefs'] != ''">
                <xsl:text>\footnote{</xsl:text>
                  <xsl:call-template name="docrefs-syntax-replace">
                    <xsl:with-param name="string" select="property[@name='docrefs']"/>
                  </xsl:call-template>
                <xsl:text>}&#10;</xsl:text>
            </xsl:if>
            <xsl:text>}}\\&#10;</xsl:text>
        </xsl:if>

        <xsl:text>\rule{1.2cm}{1pt}&#10;</xsl:text>
        <xsl:text>\end{center}&#10;</xsl:text>
        <xsl:text>}\end{center}&#10;</xsl:text>
        <xsl:text>\vfill&#10;</xsl:text> 
    </xsl:if>

    <xsl:if test="property[@name='typedoc'] != '' or property[@name='intitdoc'] != ''">
        <xsl:text>\vfill&#10;</xsl:text> 
        <xsl:text>\begin{center}\parbox[b][][c]{15cm}{&#10;</xsl:text>
        <xsl:text>\begin{center}\renewcommand{\baselinestretch}{1}&#10;</xsl:text>

        <xsl:if test="property[@name='typedoc'] != ''">
            <!-- 2) Title part -->
            <xsl:text>\fontfamily{pun}\fontseries{m}\selectfont\relsize{2}\uppercase{</xsl:text>
	    <xsl:value-of select="$pdfgen.document.typedoc"/>
	    <xsl:if test="not (property[@name='typedos'] != '') and 
		          not (property[@name='intitdos'] != '')
			  and not(property[@name='intitdoc'] != '')
			  and property[@name='docrefs'] != ''">
                <xsl:text>\footnote{</xsl:text>
                  <xsl:call-template name="docrefs-syntax-replace">
                    <xsl:with-param name="string" select="property[@name='docrefs']"/>
                  </xsl:call-template>
                <xsl:text>}&#10;</xsl:text>
            </xsl:if>
            <xsl:text>} \\[0.5cm]&#10;</xsl:text>
        </xsl:if>
                    
        <xsl:if test="$pdfgen.document.intitdoc != ''">
            <!-- 2) Sub-title part -->
            <xsl:text>\fontfamily{pun}\fontseries{m}\selectfont\relsize{-1}\textsc{{</xsl:text>
	    <xsl:value-of select="$pdfgen.document.intitdoc"/>
            <xsl:if test="not (property[@name='typedos'] != '') and 
		          not (property[@name='intitdos'] != '') and 
			  property[@name='docrefs'] != ''">
                <xsl:text>\footnote{</xsl:text>
                  <xsl:call-template name="docrefs-syntax-replace">
                    <xsl:with-param name="string" select="property[@name='docrefs']"/>
                  </xsl:call-template>
                <xsl:text>}&#10;</xsl:text>
            </xsl:if>
            <xsl:text>}}\\[0.5cm]&#10;</xsl:text>
        </xsl:if>

        <xsl:choose>
            <xsl:when test="$pdfgen.metadata.typedocid = 'AMENDCOM'"/>
            <xsl:when test="$pdfgen.metadata.typedocid = 'AMENDSCE'"/>
            <xsl:when test="property[@name='auteurs'] != ''">
                <!-- 2) Authors part -->
                <xsl:text>\fontfamily{pun}\fontseries{m}\selectfont\relsize{-1}\uppercase{{</xsl:text>
                <xsl:value-of select="$pdfgen.metadata.depose_par"/>
                <xsl:text>\textbf{</xsl:text>
                <xsl:value-of select="property[@name='auteurs']"/>
                <xsl:text>}}}\\&#10;</xsl:text>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>

        <xsl:text>\rule{1.2cm}{1pt}&#10;</xsl:text>
        <xsl:text>\end{center}&#10;</xsl:text>
	<xsl:text>}\end{center}&#10;</xsl:text>
        <xsl:text>\vfill&#10;</xsl:text> 
    </xsl:if>
    
    <xsl:if test="property[@name='commentaires'] != ''">
        <xsl:text>\begin{center}\parbox[b][][c]{15cm}{&#10;</xsl:text>
        <xsl:text>\begin{center}\renewcommand{\baselinestretch}{1.4}&#10;</xsl:text>
        <xsl:text>\fontfamily{pun}\fontseries{m}\selectfont{</xsl:text>
        <xsl:value-of select="$pdfgen.metadata.commentaires"/>
        <xsl:text>}\\&#10;</xsl:text>
        <xsl:text>\rule{1.2cm}{1pt}&#10;</xsl:text>
        <xsl:text>\end{center}&#10;</xsl:text>
        <xsl:text>}\end{center}&#10;</xsl:text>
    </xsl:if>

    <xsl:if test="../synthese">
      \framebox{%
        {\setlength{\currentparskip}{\parskip} % save the value
        \begin{minipage}[top]{\textwidth}
        \vspace{0mm}
        \setlength{\parskip}{\currentparskip} % restore the value
        \begin{center}
        \textsc{\textbf{Résumé}} \\
        \rule{1.2cm}{0.5pt}
        \end{center}
        \relsize{-1}

          <xsl:apply-templates select="../synthese/*[not(name()='title')]"/>

        \vspace{4mm}
        \end{minipage}}
      }
    </xsl:if>

    <!-- Bottom of the page -->

    <xsl:if test="not(../synthese)">
    <xsl:text>\vfill&#10;</xsl:text>
    </xsl:if>

    <xsl:text>\end{changemargin}&#10;</xsl:text>
    <xsl:text>\newpage&#10;</xsl:text>

    <xsl:if test="$with-toc">
      <xsl:text>\tableofcontents&#10;</xsl:text>
      <xsl:text>\newpage&#10;</xsl:text>
      <xsl:if test="//table/title">
        <xsl:text>\listoftables&#10;</xsl:text>
        <xsl:text>\newpage&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="//mediaobject/caption">
        <xsl:text>\listoffigures&#10;</xsl:text>
        <xsl:text>\newpage&#10;</xsl:text>
      </xsl:if>
  </xsl:if>
</xsl:template>

<xsl:template name="apply-title-template">
    <xsl:param name="template"/>
    <xsl:param name="title"    value=""/>
    <xsl:param name="subtitle" value=""/>
    <xsl:param name="footnote" value=""/>
    <xsl:param name="authors"   value=""/>

    <xsl:variable name="result">
        <xsl:call-template name="string-replace">
           <xsl:with-param name="from">%__title__%</xsl:with-param>
            <xsl:with-param name="to" select="$title"/>
            <xsl:with-param name="string">
                <xsl:call-template name="string-replace">
                    <xsl:with-param name="from">%__subtitle__%</xsl:with-param>
                    <xsl:with-param name="to" select="$subtitle"/>
                    <xsl:with-param name="string">
                        <xsl:call-template name="string-replace">
                            <xsl:with-param name="from">%__footnote__%</xsl:with-param>
                            <xsl:with-param name="to" select="$footnote"/>
                            <xsl:with-param name="string">
                                <xsl:call-template name="string-replace">
                                    <xsl:with-param name="from">%__authors__%</xsl:with-param>
                                    <xsl:with-param name="to" select="$authors"/>
                                    <xsl:with-param name="string" select="$template"/>
                                </xsl:call-template>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="$result" />
</xsl:template>


<xsl:template name="apply-template">
    <xsl:param name="properties"/>
    <xsl:param name="template"/>
    <xsl:choose>
        <xsl:when test="$properties">
            <xsl:variable name="property" select="$properties[1]"/>
            <xsl:variable name="result">
                <xsl:call-template name="apply-template">
                    <xsl:with-param name="properties" select="$properties[position()!=1]"/>
                    <xsl:with-param name="template">
                        <xsl:call-template name="string-replace">
                            <xsl:with-param name="from">%<xsl:value-of select="$property/@name"/>%</xsl:with-param>
                            <xsl:with-param name="to"><xsl:value-of select="$property"/></xsl:with-param>
                            <xsl:with-param name="string"><xsl:value-of select="$template"/></xsl:with-param>
                        </xsl:call-template>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:variable>
            <xsl:value-of select="$result" />
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$template" />
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
