<?xml version='1.0'?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY % xsldoc.ent SYSTEM "./xsldoc.ent"> %xsldoc.ent; ]>
<!--############################################################################# 
|	$Id: callout.mod.xsl 5354 2006-08-16 09:38:56Z pal $
|- #############################################################################
|	$Author: pal $
+ ############################################################################## -->

<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:doc="http://nwalsh.com/xsl/documentation/1.0"
	exclude-result-prefixes="doc" version='1.0'>

	<doc:reference id="callout" xmlns="">
		<referenceinfo>
			<releaseinfo role="meta">
				$Id: callout.mod.xsl 5354 2006-08-16 09:38:56Z pal $
			</releaseinfo>
			<authorgroup>
				&ramon;
				&james;
			</authorgroup>
			<copyright>
				<year>2000</year><year>2001</year><year>2002</year><year>2003</year><year>2004</year>
				<holder>Ramon Casellas</holder>
			</copyright>
			<revhistory>
				<doc:revision rcasver="1.5">&rev_2003_05;</doc:revision>
			</revhistory>
		</referenceinfo>
		<title>Callouts <filename>callout.mod.xsl</filename></title>
		<partintro>
			<para>
			
				Callouts are poorly supported.
			
			</para>
		</partintro>
	</doc:reference>

	<doc:template xmlns="">
		<refpurpose>Process <doc:db>programlistingco</doc:db> and <doc:db>screenco</doc:db> elements</refpurpose>
		<doc:description>
			<para>
				Applies templates.
			</para>
		</doc:description>
		<doc:variables>
			&no_var;
		</doc:variables>
	</doc:template>
	<xsl:template match="programlistingco|screenco">
		<xsl:apply-templates/>
	</xsl:template>

	<doc:template xmlns="">
		<refpurpose>Process <doc:db>area</doc:db>-related elements</refpurpose>
		<doc:description>
			<para>
				Suppressed.
			</para>
		</doc:description>
		<doc:variables>
			&no_var;
		</doc:variables>
	</doc:template>
    <xsl:template match="areaspec|areaset|area"/>

	<doc:template xmlns="">
		<refpurpose>Process <doc:db>co</doc:db>-related elements</refpurpose>
		<doc:description>
			<para>
				Print a callout number as a parenthesis.
			</para>
		</doc:description>
		<doc:variables>
			&no_var;
		</doc:variables>
	</doc:template>
	<xsl:template match="co">
		<xsl:apply-templates select="." mode="callout-bug"/>
	</xsl:template>

	<xsl:template match="co" mode="callout-bug">
		<xsl:variable name="conum">
			<xsl:number count="co" format="1"/>
		</xsl:variable>
		<xsl:text>(</xsl:text>
		<xsl:value-of select="$conum"/>
		<xsl:text>)</xsl:text>
	</xsl:template>

	<doc:template xmlns="">
		<refpurpose>Process <doc:db>calloutlist</doc:db> elements</refpurpose>
		<doc:description>
			<para>
				Applies templates.
			</para>
		</doc:description>
		<doc:variables>
			&no_var;
		</doc:variables>
	</doc:template>
	<xsl:template match="calloutlist">
		<xsl:apply-templates select="./title"/>
		<xsl:text>\begin{description}&#10;</xsl:text>
		<xsl:call-template name="content-templates"/>
		<xsl:text>\end{description}&#10;</xsl:text>
	</xsl:template>

	<doc:template xmlns="">
		<refpurpose>Process <doc:db>callout</doc:db> elements</refpurpose>
		<doc:description>
			<para>
				Applies templates.
			</para>
		</doc:description>
		<doc:variables>
			&no_var;
		</doc:variables>
	</doc:template>
	<xsl:template match="callout">
		<xsl:text>\item[{</xsl:text>
		<xsl:call-template name="callout.arearefs">
			<xsl:with-param name="arearefs" select="@arearefs"/>
		</xsl:call-template>
		<xsl:text>}]\null{}&#10;</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>&#10;</xsl:text>
	</xsl:template>

    <xsl:template name="callout.arearefs">
	<xsl:param name="arearefs"></xsl:param>
	<xsl:if test="$arearefs!=''">
	    <xsl:choose>
		<xsl:when test="substring-before($arearefs,' ')=''">
		    <xsl:call-template name="callout.arearef">
			<xsl:with-param name="arearef" select="$arearefs"/>
		    </xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
		    <xsl:call-template name="callout.arearef">
			<xsl:with-param name="arearef"
			    select="substring-before($arearefs,' ')"/>
		    </xsl:call-template>
		</xsl:otherwise>
	    </xsl:choose>
	    <xsl:call-template name="callout.arearefs">
		<xsl:with-param name="arearefs"
		    select="substring-after($arearefs,' ')"/>
	    </xsl:call-template>
	</xsl:if>
    </xsl:template>

    <xsl:template name="callout.arearef">
	<xsl:param name="arearef"></xsl:param>
	<xsl:variable name="targets" select="key('id',$arearef)"/>
	<xsl:variable name="target" select="$targets[1]"/>
	<xsl:choose>
	    <xsl:when test="count($target)=0">
		<xsl:value-of select="$arearef"/>
		<xsl:text>callout ???</xsl:text>
	    </xsl:when>
	    <xsl:when test="local-name($target)='co'">
		<!-- FIXME -->
		<xsl:text>\href{ </xsl:text>
		<xsl:value-of select="$target/@id"/> 
		<xsl:text>}{</xsl:text>
		<xsl:value-of select="$target/@id"/><xsl:text>} </xsl:text>
	    </xsl:when>
	    <xsl:otherwise>
		<xsl:text>callout ???</xsl:text>
	    </xsl:otherwise>
	</xsl:choose>
    </xsl:template>

</xsl:stylesheet>
