<?xml version='1.0'?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY % xsldoc.ent SYSTEM "./xsldoc.ent"> %xsldoc.ent; ]>
<!--############################################################################# 
|	$Id: procedure.mod.xsl 5354 2006-08-16 09:38:56Z pal $
|- #############################################################################
|	$Author: pal $
+ ############################################################################## -->

<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:doc="http://nwalsh.com/xsl/documentation/1.0"
	exclude-result-prefixes="doc" version='1.0'>

	<doc:reference id="procedure" xmlns="">
		<referenceinfo>
			<releaseinfo role="meta">
				$Id: procedure.mod.xsl 5354 2006-08-16 09:38:56Z pal $
			</releaseinfo>
			<authorgroup>
				&ramon;
				&james;
			</authorgroup>
			<copyright>
				<year>2000</year><year>2001</year><year>2002</year><year>2003</year><year>2004</year>
				<holder>Ramon Casellas</holder>
			</copyright>
			<revhistory>
				<doc:revision rcasver="1.10">&rev_2003_05;</doc:revision>
			</revhistory>
		</referenceinfo>
		<title>Procedures <filename>procedure.mod.xsl</filename></title>
		<partintro>
			<para>
			
			
			
			</para>
		</partintro>
	</doc:reference>

	<doc:template xmlns="">
		<refpurpose>Process <doc:db>procedure</doc:db> elements</refpurpose>
		<doc:description>
			<para>
			
			Format a titled, enumerated list of steps.
			
			</para>
		</doc:description>
		<doc:variables>
			<itemizedlist>
				<listitem><simpara><xref linkend="param.formal.title.placement"/></simpara></listitem>
			</itemizedlist>
		</doc:variables>
		<doc:notes>
			<para>
			
			The &LaTeX; <function condition="env">enumerate</function> environment
			is used. Although the procedure is a formal, titled block, is is not
			typeset as a subsection.
			
			</para>
		</doc:notes>
		<doc:samples>
			<simplelist type='inline'>
				&test_book;
				&test_procedure;
			</simplelist>
		</doc:samples>
		<doc:seealso>
			<itemizedlist>
				<listitem><simpara><xref linkend="template.procedure/title"/></simpara></listitem>
			</itemizedlist>
		</doc:seealso>
	</doc:template>
	<xsl:template match="procedure">
		<xsl:variable name="placement">
			<xsl:call-template name="generate.formal.title.placement">
				<xsl:with-param name="object" select="local-name(.)" />
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="preamble" select="node()[not(self::blockinfo or self::title or self::subtitle or self::titleabbrev or self::step)]"/>
		<xsl:choose>
			<xsl:when test="$placement='before' or $placement=''">
				<xsl:apply-templates select="title" mode="procedure.title"/>
				<xsl:apply-templates select="$preamble"/>
				<xsl:text>\begin{enumerate}&#10;</xsl:text>
				<xsl:apply-templates select="step"/>
				<xsl:text>\end{enumerate}&#10;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="$preamble"/>
				<xsl:text>\begin{enumerate}&#10;</xsl:text>
				<xsl:apply-templates select="step"/>
				<xsl:text>\end{enumerate}&#10;</xsl:text>
				<xsl:apply-templates select="title" mode="procedure.title"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<doc:template xmlns="">
		<refpurpose>Process a <doc:db>procedure</doc:db>'s <doc:db>title</doc:db> </refpurpose>
		<doc:description>
			<para>
			
			Format a special bridgehead.
			
			</para>
		</doc:description>
		<doc:variables>
			<itemizedlist>
				<listitem><simpara><xref linkend="param.latex.procedure.title.style"/></simpara></listitem>
				<listitem><simpara><xref linkend="param.latex.apply.title.templates"/></simpara></listitem>
			</itemizedlist>
		</doc:variables>
		<doc:notes>
			<para>
			
			The title is typeset as a paragraph.
			
			</para>
		</doc:notes>
		<doc:samples>
			<simplelist type='inline'>
				&test_book;
				&test_procedure;
			</simplelist>
		</doc:samples>
	</doc:template>
	<xsl:template match="procedure/title">
		<xsl:text>&#10;&#10;{</xsl:text>
		<xsl:value-of select="$latex.procedure.title.style"/>
		<xsl:text>{</xsl:text>
		<xsl:choose>
			<xsl:when test="$latex.apply.title.templates=1">
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="."/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>}}&#10;</xsl:text>
	</xsl:template>

	<doc:template xmlns="">
		<refpurpose>Process <doc:db>step</doc:db> elements </refpurpose>
		<doc:description>
			<para>
			
			Format steps and substeps as part of a procedure.
			
			</para>
		</doc:description>
		<doc:variables>
			<itemizedlist>
				<listitem><simpara><xref linkend="param.latex.step.title.style"/></simpara></listitem>
			</itemizedlist>
		</doc:variables>
		<doc:notes>
			<para>
			
			Each step is typeset using the &LaTeX; <function condition="latex">item</function> command.
			
			</para>
		</doc:notes>
		<doc:samples>
			<simplelist type='inline'>
				&test_book;
				&test_procedure;
			</simplelist>
		</doc:samples>
	</doc:template>
	<xsl:template match="step">
		<xsl:choose>
			<xsl:when test="title">
				<xsl:text>&#10;\item{{</xsl:text>
				<xsl:value-of select="$latex.step.title.style"/> <!-- by default \sc -->
				<xsl:text>{</xsl:text>
				<xsl:apply-templates select="title"/>
				<xsl:text>}}&#10;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>&#10;\item{</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		 <xsl:apply-templates select="node()[not(self::title)]"/>
		<xsl:text>}&#10;</xsl:text>
	</xsl:template>

	<doc:template xmlns="">
		<refpurpose>Process <doc:db>substep</doc:db> elements </refpurpose>
		<doc:description>
			<para>
			
			Format substeps as part of a step.
			
			</para>
		</doc:description>
		<doc:variables>
			&no_var;
		</doc:variables>
		<doc:notes>
			<para>
			
			Substeps are typeset by nesting a &LaTeX;
			<function condition="env">enumerate</function> environment.
			
			</para>
		</doc:notes>
		<doc:samples>
			<simplelist type='inline'>
				&test_book;
				&test_procedure;
			</simplelist>
		</doc:samples>
	</doc:template>
	<xsl:template match="substeps">
		<xsl:text>\begin{enumerate}&#10;</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>\end{enumerate}&#10;</xsl:text>
	</xsl:template>

</xsl:stylesheet>

