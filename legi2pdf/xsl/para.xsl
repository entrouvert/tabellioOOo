<?xml version="1.0" encoding="utf-8"?>
<!--
Tabellio - software suite for deliberative assemblies
         - suite logicielle pour assemblées délibératives
         - http://www.tabellio.org/
Copyright (C) 2006 Parlement de la Communauté française de Belgique

This file is part of Tabellio.

Tabellio is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tabellio is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!--  FileName: para.xsl                    --> 
<!-- 
Purpose: Alignment support for paragraphs
--> 

<xsl:template match="para[@align='center']">
  <xsl:text>\begin{center}&#10;</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>\end{center}&#10;</xsl:text>
</xsl:template>

<xsl:template match="para[@margin-left='true']">
  <xsl:text>\par\begingroup&#10;</xsl:text>
  <xsl:text>\leftskip2em&#10;</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>\par\endgroup&#10;</xsl:text>
</xsl:template>

<!-- this serie of rules are matching <para/> that happens directly inside
     <book>, not enclosed in a section.

     The first rule matches the first paragraph and is used to start the
     multicols environment; the second rule matches the last paragraph and
     is used to close the environment.

     The third rule handles the case where there is a single paragraph, and
     opens and closes the multicols environment by itself.
-->

<xsl:template match="book/para[preceding-sibling::metadata]">
  <xsl:text>\pagebreak&#10;</xsl:text>
  <xsl:text>\begin{multicols}{2}&#10;</xsl:text>
  <xsl:text>&#10;&#10;</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>&#10;&#10;</xsl:text>
</xsl:template>

<xsl:template match="book/para[last()]">
  <xsl:text>&#10;&#10;</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>&#10;&#10;</xsl:text>
  <xsl:text>\end{multicols}&#10;</xsl:text>
</xsl:template>

<xsl:template match="book/para[preceding-sibling::metadata and last()]">
  <xsl:text>\pagebreak&#10;</xsl:text>
  <xsl:text>\begin{multicols}{2}&#10;</xsl:text>
  <xsl:text>&#10;&#10;</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>&#10;&#10;</xsl:text>
  <xsl:text>\end{multicols}&#10;</xsl:text>
</xsl:template>

</xsl:stylesheet>
