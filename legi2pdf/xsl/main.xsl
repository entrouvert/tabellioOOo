<?xml version="1.0" encoding="utf-8"?>
<!--
Tabellio - software suite for deliberative assemblies
         - suite logicielle pour assemblées délibératives
         - http://www.tabellio.org/
Copyright (C) 2006 Parlement de la Communauté française de Belgique

This file is part of Tabellio.

Tabellio is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tabellio is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!--  FileName: main.xsl                    --> 
<!-- 
      Purpose: Entry point for the docbook to latex filter.
      Import the db2latex stylesheets then the customization.
      Re-define the entry point to create a 'root' node (there is
      a post-processing phase) 
--> 


<!-- Import db2latex stylesheets -->
<xsl:import href="./db2latex/xsl/docbook.xsl"/>

<!-- Import stylesheet to customize db2latex (redefinition of Db2Latex default value) -->
<xsl:import href="db2latex_custom.xsl"/>

<!-- Import stylesheet to produce the first page -->
<xsl:import href="first_page.xsl"/>

<!-- Import stylesheet to generate table object -->
<xsl:import href="table.xsl"/>

<!-- Import stylesheet to generate image object -->
<xsl:import href="image.xsl"/>

<!-- Import stylesheet to control section (db2latex doesn't support footnote inside section/title) -->
<xsl:import href="section.xsl"/>

<!-- Import stylesheet to define miscellaneous templates: speech / ... -->
<xsl:import href="misc.xsl"/>

<!-- Import stylesheet to control legistic items -->
<xsl:import href="legistic.xsl"/>

<!-- Import stylesheet to control paragraphs -->
<xsl:import href="para.xsl"/>

<!-- Import stylesheet to control appendix insertion -->
<xsl:import href="appendix.xsl"/>

<xsl:import href="parlement.xsl"/>

<xsl:import href="pfb.xsl"/>

<xsl:template match="processing-instruction('line-break')">
  <xsl:text> \\&#10;</xsl:text>
</xsl:template>

<xsl:template match="processing-instruction('page-break')">
  <xsl:text> \newpage&#10;</xsl:text>
</xsl:template>

<!-- The output method is 'xml' due to the post-processing phase to convert euro tag -->
<xsl:output method="xml" encoding="utf-8" indent="yes"/>

<xsl:variable name="table-width-threshold" select="'225'"/>
<xsl:variable name="table-width-parchemin-threshold" select="'225'"/>

<!-- Entry point of the docbook to latex filter -->
<xsl:template match="/">
    <xsl:message>--------------------------------------------</xsl:message>
    <xsl:message> DOCBOOK To LaTeX filter</xsl:message>
    <xsl:message> Version: 0.2</xsl:message>
    <xsl:message>--------------------------------------------</xsl:message>
    <xsl:element name="root">
        <xsl:variable name="xsl-vendor" select="system-property('xsl:vendor')"/>
        <xsl:apply-templates/>
    </xsl:element>
</xsl:template> 


        
        <xsl:template match="caption" mode="xref-to">
	<xsl:param name="referrer"/>
	<xsl:param name="purpose"/>
	<xsl:param name="xrefstyle"/>
	<!--xsl:param name="name">
		<xsl:choose>
			<xsl:when test="contains(local-name(parent::*), 'info')">
				<xsl:call-template name="xpath.location">
					<xsl:with-param name="node" select="../.."/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="xpath.location">
					<xsl:with-param name="node" select=".."/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:param-->
	<xsl:param name="name">figure</xsl:param>
	<xsl:message>referrer <xsl:value-of select="$referrer"/></xsl:message>
	<xsl:message>purpose <xsl:value-of select="$purpose"/></xsl:message>
	<xsl:message>xrefstyle <xsl:value-of select="$xrefstyle"/></xsl:message>
	<xsl:message>name <xsl:value-of select="$name"/></xsl:message>
	<xsl:variable name="template">
		<xsl:variable name="user-template">
			<xsl:if test="$xrefstyle != '' and not(contains($xrefstyle, ':'))">
				<xsl:call-template name="gentext.template.exists">
					<xsl:with-param name="context" select="$xrefstyle"/>
					<xsl:with-param name="name" select="$name"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:variable>
                <xsl:variable name="context">
			<xsl:choose>
				<xsl:when test="$user-template = 1">
					<xsl:value-of select="$xrefstyle"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'title'"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:call-template name="gentext.template">
			<xsl:with-param name="context" select="$context"/>
			<xsl:with-param name="name" select="$name"/>
			<xsl:with-param name="xrefstyle" select="$xrefstyle"/>
			<xsl:with-param name="purpose" select="$purpose"/>
			<xsl:with-param name="referrer" select="$referrer"/>
		</xsl:call-template>
		<xsl:text>. %t</xsl:text>
	</xsl:variable>

	<xsl:call-template name="substitute-markup">
		<xsl:with-param name="purpose" select="$purpose"/>
		<xsl:with-param name="xrefstyle" select="$xrefstyle"/>
		<xsl:with-param name="referrer" select="$referrer"/>
		<xsl:with-param name="template" select="$template"/>
	</xsl:call-template>
    
    </xsl:template>
 

</xsl:stylesheet>
