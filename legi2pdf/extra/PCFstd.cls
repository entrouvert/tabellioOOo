%%
%% This is file `PCFstd.cls'
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{PCFstd}[1995/06/20 Article personnel]

%%%  Chargement de la classe article, avec transfert d'options
\PassOptionsToClass{a4paper}{article} 
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions
\LoadClass{article}

\usepackage{eurosym}
\usepackage{threeparttable}
\usepackage{balance}
\usepackage{supertabular}
\usepackage{ifthen}
\usepackage[french]{babel}  
\usepackage{pdfpages}
\usepackage[normalem]{ulem}
\usepackage{color}
\definecolor{rltblack}{rgb}{0,0,0}
\usepackage{chngpage}



% --------------------------------------
% to define the size of the margin / headers / ...
% --------------------------------------
%\newlength{\headwidth}

\setlength{\hoffset}{-0.46cm}
\setlength{\voffset}{-0.56cm}
\setlength{\textwidth}{15.9cm}
\setlength{\textheight}{24.3cm} 
\setlength{\oddsidemargin}{0.66cm}
\setlength{\evensidemargin}{0.66cm}
\setlength{\topmargin}{0.1cm}
\setlength{\headheight}{0.4cm}
\setlength{\headsep}{0.6cm}
\setlength{\marginparwidth}{0cm}
\setlength{\marginparsep}{0cm}
%\setlength{\headwidth}{15.9cm}
\setlength{\columnsep}{0.8cm} 


\newlength{\mytablewidth}
\newlength{\mytempvalue}
\newlength{\mytempwidth}
\newlength{\mytempdepth}

% --------------------------------------
% Environment definition to adjust left/right margins
% --------------------------------------

  \newenvironment{changemargin}[2]{\begin{list}{}{
  \setlength{\topsep}{0pt}%
  \setlength{\leftmargin}{0pt}%
  \setlength{\rightmargin}{0pt}%
  \setlength{\listparindent}{\parindent}
  \setlength{\itemindent}{\parindent}
  \setlength{\parsep}{0.2cm plus 1pt}
  \addtolength{\leftmargin}{#1}
  \addtolength{\rightmargin}{#2}
  }\item }{\end{list}}

% --------------------------------------
% to define extra space between two parapgraphs
% --------------------------------------
  \addtolength{\parskip}{0.2cm}

% --------------------------------------
% to define the style of the footnote mark
% --------------------------------------
\makeatletter
\renewcommand\@makefnmark{\normalfont(\@thefnmark)}
%\renewcommand\@makefntext[1]{%
%  \parindent 1em%
%  \noindent
%  \hb@xt@1.8em{\hss\@makefnmark} #1} 
\makeatother

% --------------------------------------
% Definition of itemize environment to change the left margin space and 
% vertical space between items.
% --------------------------------------
  \newenvironment{Itemizedlist}[1]{\begin{list}{#1}
     {\setlength{\itemindent}{0pt}%
       \settowidth{\labelwidth}{#1}%
       \setlength{\labelsep}{0.4em}%
       \setlength{\leftmargin}{\labelwidth}%
       \setlength{\rightmargin}{0pt}%
       \setlength{\listparindent}{0pt}%
       \addtolength{\leftmargin}{\labelsep}%
       \setlength{\topsep}{0pt}%
       \setlength{\parsep}{\parskip}%
       \setlength{\partopsep}{\parskip}%
       \setlength{\itemsep}{\parskip}}}%
   {\end{list}}
 \newenvironment{itemizedlist1}{\begin{Itemizedlist}{---}}{\end{Itemizedlist}}
 \newenvironment{itemizedlist2}{\begin{Itemizedlist}{--}}{\end{Itemizedlist}}
 \newenvironment{itemizedlist3}{\begin{Itemizedlist}{-}}{\end{Itemizedlist}}

% --------------------------------------
% latex variables redefinitions
% --------------------------------------
  \setlength{\columnsep}{25pt}
  
  \addto\captionsfrench{\renewcommand{\contentsname}%
    {\centerline{TABLE DES MATI\`{E}RES}}}
  \addto\captionsfrench{\renewcommand{\listfigurename}%
    {\centerline{TABLE DES FIGURES}}}
  \addto\captionsfrench{\renewcommand{\listtablename}%
    {\centerline{LISTE DES TABLEAUX}}}

  \makeatletter

  \newcommand{\pcfpart}[4]{%
   \if@noskipsec \leavevmode \fi
   \par\nobreak
   \addvspace{4ex}%
   \@afterindentfalse
   \refstepcounter{part}%
   \ifthenelse{\equal{#3}{}}{\addcontentsline{toc}{part}{\textsc{#1}}}{\addcontentsline{toc}{part}{\textsc{#1 #3}}}
   {\parindent \z@ \centering
    \interlinepenalty \@M
    \fontfamily{pun}\fontseries{m}\selectfont
    \Large\textsc{#2}%
    \markboth{}{}
    \ifthenelse{\equal{#4}{}}{}{\vskip 1ex\normalsize\textsc{#4}\markboth{}{}}
    \par\nobreak
    \rule{1.2cm}{1pt}
    \markboth{}{}\par}%
    \nobreak
    \vskip 3ex
    \@afterheading}

  \newcommand{\pcfpartwithid}[4]{%
   \if@noskipsec \leavevmode \fi
   \par
   \addvspace{4ex}%
   \@afterindentfalse
   \refstepcounter{part}%
   \addcontentsline{toc}{part}{Partie\nobreakspace\thepart\hspace{1em} #1 #3}%
   {\parindent \z@ \centering
    \interlinepenalty \@M
    \normalfont
    \large\bfseries\scshape \partname\nobreakspace\thepart
    \par\nobreak
    \Large \bfseries \textsc{#2}%
    \markboth{}{}\par
    \large \bfseries \textsc{#4}%
    \markboth{}{}\par
    \rule{1.2cm}{1pt}
    \par}%
    \nobreak
    \vskip 3ex
    \@afterheading}

  \newcommand{\pcfpartappendix}[4]{%
   \if@noskipsec \leavevmode \fi
   \par\nobreak
   \addvspace{4ex}%
   \@afterindentfalse
   \refstepcounter{part}%
   {\parindent \z@ \centering
    \interlinepenalty \@M
    \fontfamily{pun}\fontseries{m}\selectfont
    \Large\textsc{#2}%
    \markboth{}{}
    \ifthenelse{\equal{#4}{}}{}{\vskip 1ex\normalsize\textsc{#4}\markboth{}{}}
    \par\nobreak
    \rule{1.2cm}{1pt}
    \markboth{}{}\par}%
    \nobreak
    \vskip 3ex
    \@afterheading}


  \newcommand{\bqrsection}[2]{%
   \if@noskipsec \leavevmode \fi
   \par
   \addvspace{4ex}%
   \@afterindentfalse
   \refstepcounter{section}%
   \addcontentsline{toc}{section}{\textsc{#1}}
   {\parindent \z@ \centering
    \interlinepenalty \@M
    \begin{changemargin}{2cm}{2cm}
    \begin{center}
    \large\bfseries{#2}%
    \markboth{}{}
    \par\nobreak
    \rule{1.2cm}{1pt}
    \markboth{}{}\par%
    \end{center}
    \end{changemargin}
    \par}%
    \nobreak
    \vskip 3ex
    \@afterheading}

  \newcommand{\bqrsubsection}[2]{%
   \if@noskipsec \leavevmode \fi
   \par
   \addvspace{4ex}%
   \@afterindentfalse
   \refstepcounter{subsection}%
   \addcontentsline{toc}{subsection}{#1}
   {\parindent \z@ 
    \interlinepenalty \@M
    \bfseries{#2}%
    \markboth{}{}%
    \par}%
    \nobreak
    \vskip 3ex
    \@afterheading}


  \newcommand{\bqrpart}[4]{%
   \if@noskipsec \leavevmode \fi
   \par
   \addvspace{4ex}%
   \@afterindentfalse
   \refstepcounter{part}%
   \addcontentsline{toc}{part}{#1 #3}%
   {\parindent \z@ \centering
    \interlinepenalty \@M
    \fontfamily{pun}\fontseries{m}\selectfont
    \Large \bfseries \textsc{#2}%
    \markboth{}{}\par
    \large \bfseries \textsc{#4}%
    \markboth{}{}\par
    \par}%
    \nobreak
    \vskip 3ex
    \@afterheading}

  \newcommand{\bqrfigure}[1]{%
   \refstepcounter{figure}%
   \par%
   \figurename\nobreakspace\thefigure\nobreakspace -- \nobreakspace #1%
   \par\nobreak}

\newcounter{pcfannexe}
\renewcommand{\thepcfannexe}{}

  \newcommand{\pcfannexe}[1]{%
   \if@noskipsec \leavevmode \fi
   \par\nobreak
   \addvspace{4ex}%
   \@afterindentfalse
   \refstepcounter{pcfannexe}%
   \setcounter{part}{0}%
   \setcounter{section}{0}%
   \setcounter{subsection}{0}%
   \setcounter{subsubsection}{0}%
   \setcounter{paragraph}{0}%
   \setcounter{subparagraph}{0}%
   \addcontentsline{toc}{part}{\textsc{#1}}
   {\parindent \z@ \centering
    \interlinepenalty \@M
    %\begin{flushleft}\large\bfseries\textsc{Annexe\nobreakspace\thepcfannexe}\end{flushleft}%
    %\par\nobreak
    \fontfamily{pun}\fontseries{m}\selectfont
    \Large\textsc{#1}%
    \markboth{}{}
    \par\nobreak
    \rule{1.2cm}{1pt}
    \markboth{}{}\par}%
    \nobreak
    \vskip 3ex
    \@afterheading}

  \renewcommand\section{\@startsection{section}{1}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\large\bfseries}} 
  \renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                      {\normalfont\normalsize\bfseries}} 


  \renewcommand\subparagraph{\@startsection{subparagraph}{5}{\z@}%
                                       {-1.25ex \@plus 1ex \@minus .2ex}%
                                       {0.2ex \@plus .2ex}%
                                      {\normalfont\normalsize\bfseries}} 


  \setlength{\parindent}{0.6cm}
  
  \renewcommand\part{%
   \if@noskipsec \leavevmode \fi
   \par
   \addvspace{4ex}%
   \@afterindentfalse
   \secdef\@part\@spart}

   \def\@part[#1]#2{%
       \ifnum \c@secnumdepth >\m@ne
         \refstepcounter{part}%
         \addcontentsline{toc}{part}{Partie\nobreakspace\thepart\hspace{1em} #1}%
       \else
         \addcontentsline{toc}{part}{#1}%
       \fi
       {\parindent \z@ \centering
        \interlinepenalty \@M
        \normalfont
        \ifnum \c@secnumdepth >\m@ne
          \large\bfseries\scshape \partname\nobreakspace\thepart
          \par\nobreak
        \fi
        \large \bfseries \textsc{#2}%
        \markboth{}{}\par}%
       \nobreak
       \vskip 3ex
       \@afterheading}
   \def\@spart[#1]#2{%
       \addcontentsline{toc}{part}{#1}%
       {\parindent \z@ \raggedright
        \interlinepenalty \@M
        \normalfont
        \huge \bfseries #1\par}%
        \nobreak
        \vskip 3ex
        \@afterheading} 
                                     
     \makeatother


%\newcommand{\nfont}{\fontsize{11}{12}\selectfont}
\newcommand{\nfont}{\normalsize\selectfont}
\newcommand{\sfont}{\fontsize{9}{12}\selectfont}

% --------------------------------------
% LEGISTIQUE : latex definitions
% --------------------------------------

\newcounter{legipart}
\renewcommand{\thelegipart}{\ifthenelse{\value{legipart} = 1}{PARTIE PREMI\`{E}RE}{PARTIE \Roman{legipart}}}
\newcounter{legilivre}
\renewcommand{\thelegilivre}{\ifthenelse{\value{legilivre} = 1}{LIVRE PREMIER}{LIVRE \Roman{legilivre}}}
\newcounter{legititre}
\renewcommand{\thelegititre}{\ifthenelse{\value{legititre} = 1}{TITRE PREMIER}{TITRE \Roman{legititre}}}
\newcounter{legichapitre}
\renewcommand{\thelegichapitre}{\ifthenelse{\value{legichapitre} = 1}{CHAPITRE PREMIER}{CHAPITRE \Roman{legichapitre}}}
\newcounter{legisection}
\renewcommand{\thelegisection}{\ifthenelse{\value{legisection} = 1}{SECTION PREMI\`{E}RE}{SECTION \Roman{legisection}}}
\newcounter{legisoussection}
\renewcommand{\thelegisoussection}{\ifthenelse{\value{legisoussection} = 1}{SOUS-SECTION PREMI\`{E}RE}{SOUS-SECTION \Roman{legisoussection}}}


\newcommand\legidebut{%
   \setcounter{legiarticleid}{0}%
   \setcounter{legipart}{0}%
   \setcounter{legilivre}{0}%
   \setcounter{legititre}{0}%
   \setcounter{legichapitre}{0}%
   \setcounter{legisection}{0}%
   \setcounter{legisoussection}{0}}

\makeatletter

% Partie
% ------
\newcommand\legipart[2]{%
   \stepcounter{legipart}%
   \setcounter{legilivre}{0}%
   \setcounter{legititre}{0}%
   \setcounter{legichapitre}{0}%
   \setcounter{legisection}{0}%
   \setcounter{legisoussection}{0}%
   \if@noskipsec \leavevmode \fi
   \par
   \addvspace{2ex}%
   \@afterindentfalse
   \addcontentsline{toc}{section}{PARTIE \Roman{legipart} #1}%
   {\parindent \z@ \centering
   \interlinepenalty \@M
    \normalfont{\thelegipart}
    \par\nobreak%
    \bfseries{#2}
   \markboth{}{}\par}%
   \nobreak
   \vskip 1ex
   \@afterheading}
% Partie TOC (caution: not used!!!)
\newcommand*\l@legipart[2]{%
  \ifnum \c@tocdepth >\z@
    \addpenalty\@secpenalty
    \addvspace{1.0em \@plus\p@}%
    \setlength\@tempdima{1.5em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      \leavevmode \bfseries
      \advance\leftskip\@tempdima
      \hskip -\leftskip
      #1\nobreak\hfil \nobreak\hb@xt@\@pnumwidth{\hss #2}\par
    \endgroup
  \fi} 

% Livre
% -----
\newcommand\legilivre[2]{%
   \stepcounter{legilivre}%
   \setcounter{legititre}{0}%
   \setcounter{legichapitre}{0}%
   \setcounter{legisection}{0}%
   \setcounter{legisoussection}{0}%
   \if@noskipsec \leavevmode \fi
   \par
   \addvspace{2ex}%
   \@afterindentfalse
   \addcontentsline{toc}{section}{LIVRE \Roman{legilivre}\hspace{1em}#1}%
   {\parindent \z@ \centering
   \interlinepenalty \@M
    \normalfont{\thelegilivre}
    \par\nobreak%
    \bfseries{#2}
   \markboth{}{}\par}%
   \nobreak
   \vskip 1ex
   \@afterheading}
% Livre TOC (caution: not used!!!)
\newcommand*\l@legilivre[2]{%
  \ifnum \c@tocdepth >\z@
    \addpenalty\@secpenalty
    \addvspace{1.0em \@plus\p@}%
    \setlength\@tempdima{1.5em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      \leavevmode \bfseries
      \advance\leftskip\@tempdima
      \hskip -\leftskip
      #1\nobreak\hfil \nobreak\hb@xt@\@pnumwidth{\hss #2}\par
    \endgroup
  \fi} 

% Titre
% -----
\newcommand\legititre[2]{%
   \stepcounter{legititre}%
   \setcounter{legichapitre}{0}%
   \setcounter{legisection}{0}%
   \setcounter{legisoussection}{0}%
   \if@noskipsec \leavevmode \fi
   \par
   \addvspace{2ex}%
   \@afterindentfalse
   \addcontentsline{toc}{section}{TITRE \Roman{legititre} #1}%
   {\parindent \z@ \centering
   \interlinepenalty \@M
    \normalfont{\thelegititre}
    \par\nobreak%
    \bfseries{#2}
   \markboth{}{}\par}%
   \nobreak
   \vskip 1ex
   \@afterheading}
% Titre TOC (caution: not used!!!)
\newcommand*\l@legititre[2]{%
  \ifnum \c@tocdepth >\z@
    \addpenalty\@secpenalty
    \addvspace{1.0em \@plus\p@}%
    \setlength\@tempdima{1.5em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      \leavevmode \bfseries
      \advance\leftskip\@tempdima
      \hskip -\leftskip
      #1\nobreak\hfil \nobreak\hb@xt@\@pnumwidth{\hss #2}\par
    \endgroup
  \fi} 

% Chapitre
% --------
\newcommand\legichapitre[2]{%
   \stepcounter{legichapitre}%
   \setcounter{legisection}{0}%
   \setcounter{legisoussection}{0}%
   \if@noskipsec \leavevmode \fi
   \par
   \addvspace{2ex}%
   \@afterindentfalse
   \addcontentsline{toc}{subsection}{CHAPITRE \Roman{legichapitre} #1}%
   {\parindent \z@ \centering
   \interlinepenalty \@M
    \normalfont{\thelegichapitre}
    \par\nobreak%
    \bfseries{#2}
   \markboth{}{}\par}%
   \nobreak
   \vskip 1ex
   \@afterheading}
% Chapitre TOC
\newcommand*\l@legichapitre[2]{%
  \ifnum \c@tocdepth >\z@
    \addpenalty\@secpenalty
    \addvspace{1.0em \@plus\p@}%
    \setlength\@tempdima{1.5em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      \leavevmode \bfseries
      \advance\leftskip\@tempdima
      \hskip -\leftskip
      #1\nobreak\hfil \nobreak\hb@xt@\@pnumwidth{\hss #2}\par
    \endgroup
  \fi} 

% Section
% --------
\newcommand\legisection[2]{%
   \stepcounter{legisection}%
   \setcounter{legisoussection}{0}%
   \if@noskipsec \leavevmode \fi
   \par
   \addvspace{2ex}%
   \@afterindentfalse
   {\parindent \z@ \centering
   \interlinepenalty \@M
    \normalfont{\thelegisection}
    \par\nobreak%
    \bfseries{#2}
   \addcontentsline{toc}{subsubsection}{SECTION \Roman{legisection} #1}%
   \markboth{}{}\par}%
   \nobreak
   \vskip 1ex
   \@afterheading}
% Section TOC
\newcommand*\l@legisection[2]{%
  \ifnum \c@tocdepth >\z@
    \addpenalty\@secpenalty
    \addvspace{1.0em \@plus\p@}%
    \setlength\@tempdima{1.5em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      \leavevmode \bfseries
      \advance\leftskip\@tempdima
      \hskip -\leftskip
      #1\nobreak\hfil \nobreak\hb@xt@\@pnumwidth{\hss #2}\par
    \endgroup
  \fi} 

% Sous-Section
% ------------
\newcommand\legisoussection[2]{%
   \stepcounter{legisoussection}%
   \if@noskipsec \leavevmode \fi
   \par
   \addvspace{2ex}%
   \@afterindentfalse
   {\parindent \z@ \centering
   \interlinepenalty \@M
    \normalfont{\thelegisoussection}
    \par\nobreak%
    \bfseries{#2}
   \addcontentsline{toc}{paragraph}{SOUS-SECTION \Roman{legisoussection} #1}%
   \markboth{}{}\par}%
   \nobreak
   \vskip 1ex
   \@afterheading}
% Sous-Section TOC
\newcommand*\l@legisoussection[2]{%
  \ifnum \c@tocdepth >\z@
    \addpenalty\@secpenalty
    \addvspace{1.0em \@plus\p@}%
    \setlength\@tempdima{1.5em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      \leavevmode \bfseries
      \advance\leftskip\@tempdima
      \hskip -\leftskip
      #1\nobreak\hfil \nobreak\hb@xt@\@pnumwidth{\hss #2}\par
    \endgroup
  \fi} 

% Paragraph level
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
            {-2.5ex\@plus -1ex \@minus -.25ex}%
            {1.25ex \@plus .25ex}%
            {\normalfont\normalsize\bfseries}}

\makeatother

% Article
% -------
\newcounter{legiarticleid}
\renewcommand{\thelegiarticleid}{\ifthenelse{\value{legiarticleid} = 1}{Article 1\textsuperscript{er}}{Art. \arabic{legiarticleid}}}
\makeatletter
\newcommand{\legiarticle}{%
   \stepcounter{legiarticleid}%
   \par%
   \vskip 1ex
   \@afterindentfalse
   {\parindent \z@ \centering
   \interlinepenalty \@M
    \bfseries{\thelegiarticleid}
   \markboth{}{}\par}%
   \nobreak
   \vskip 1ex
   \@afterheading}
\newcommand{\legimanualarticle}[1]{%
   \par%
   \vskip 1ex
   \@afterindentfalse
   {\parindent \z@ \centering
   \interlinepenalty \@M
    \bfseries{#1}
   \markboth{}{}\par}%
   \nobreak
   \vskip 1ex
   \@afterheading}
\makeatother


%\newcounter{legisection}
%\renewcommand{\thelegisection}{}
%\makeatletter

%\newcommand\legisection[4]{%
%   \if@noskipsec \leavevmode \fi
%   \par
%   \addvspace{4ex}%
%   \@afterindentfalse
%   \addcontentsline{toc}{section}{#1 : #3}%
%   {\parindent \z@ \centering
%   \interlinepenalty \@M
%    \normalfont\large{\textsc{#2}}
%    \par\nobreak%
%    \bfseries{#4}
%   \markboth{}{}\par}%
%   \nobreak
%   \vskip 3ex
%   \@afterheading}

%\newcommand*\l@legisection[2]{%
%  \ifnum \c@tocdepth >\z@
%    \addpenalty\@secpenalty
%    \addvspace{1.0em \@plus\p@}%
%    \setlength\@tempdima{1.5em}%
%    \begingroup
%      \parindent \z@ \rightskip \@pnumwidth
%      \parfillskip -\@pnumwidth
%      \leavevmode \bfseries
%      \advance\leftskip\@tempdima
%      \hskip -\leftskip
%      #1\nobreak\hfil \nobreak\hb@xt@\@pnumwidth{\hss #2}\par
%    \endgroup
%  \fi} 
%\makeatother


%%
%% End of file `PCFstd.cls'.
