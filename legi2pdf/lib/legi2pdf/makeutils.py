# -*- coding: utf-8 -*-
# Tabellio -- software suite for deliberative assemblies
#          -- suite logicielle pour assemblées délibératives
#          -- http://www.tabellio.org/
# Copyright (C) 2006 Parlement de la Communauté française de Belgique

# This file is part of Tabellio.

# Tabellio is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# Tabellio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import os
from stat import ST_MTIME 

def listDir(dir,exts):
    return filter(lambda f,exts=exts: os.path.splitext(f)[1] in exts,
                  os.listdir(dir))

def mustBuild(source,target):
    """Return true if target does not exist or is older than source"""
    if not os.path.exists(target):
        return 1
    srcMTime = os.stat(source)[ST_MTIME]
    trgMTime = os.stat(target)[ST_MTIME]
    return srcMTime >= trgMTime
