<?xml version="1.0" encoding="utf-8"?>
<!--
Tabellio - software suite for deliberative assemblies
         - suite logicielle pour assemblées délibératives
         - http://www.tabellio.org/
Copyright (C) 2006 Parlement de la Communauté française de Belgique

This file is part of Tabellio.

Tabellio is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tabellio is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!--  FileName: misc.xsl                    --> 
<!-- 
     Purpose: Customize Db2Latex product for:
       o literallayout: not supported by Db2Latex
       o speech: not docbook
       o footnote: customization for table 
         generate \tnote instead of \footnote
--> 

<xsl:template match="emphasis">
    <xsl:param name="content">
        <xsl:apply-templates/>
    </xsl:param>
    <xsl:text>{\em{</xsl:text>
    <xsl:copy-of select="$content"/>
    <xsl:text>}}</xsl:text> 
</xsl:template>

<xsl:template match="emphasis[@role='bold']">
    <xsl:text>{\bfseries{</xsl:text>
        <xsl:apply-templates/>
    <xsl:text>}}</xsl:text> 
</xsl:template>
 
<xsl:template match="emphasis[@role='underline']">
    <xsl:param name="content">
        <xsl:apply-templates/>
    </xsl:param>
    <xsl:text>{\uline{</xsl:text>
    <xsl:copy-of select="$content"/>
    <xsl:text>}}</xsl:text> 
</xsl:template>

	<xsl:template match="orderedlist">
		<xsl:variable name="numeration">
			<xsl:choose>
				<xsl:when test="@numeration">
					<xsl:value-of select="@numeration"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="arabic"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:apply-templates select="node()[not(self::listitem)]"/>
		<xsl:text>&#10;\begin{enumerate}</xsl:text>
		<xsl:if test="@numeration">
			<xsl:choose>
                            <xsl:when test="@numeration='arabic'"> 	<xsl:text>[1{\textsuperscript{o}}]</xsl:text>&#10;</xsl:when>
			<xsl:when test="@numeration='upperalpha'"><xsl:text>[A]</xsl:text>&#10;</xsl:when>
                        <xsl:when test="@numeration='loweralpha'"><xsl:text>[a{)}]</xsl:text>&#10;</xsl:when>
			<xsl:when test="@numeration='upperroman'"><xsl:text>[I]</xsl:text>&#10;</xsl:when>
			<xsl:when test="@numeration='lowerroman'"><xsl:text>[i]</xsl:text>&#10;</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="listitem"/>
		<xsl:text>\end{enumerate}&#10;</xsl:text>
	</xsl:template>
 


<!-- literallayout template --> 
<xsl:template match="literallayout">
    <xsl:text>&#10;\begin{literallayout}&#10;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#10;\end{literallayout}&#10;</xsl:text> 
</xsl:template>


<!-- speech template --> 
<xsl:template match="note">
    <xsl:text>&#10;\begin{center}&#10;</xsl:text>
    <xsl:text>\fontshape{it}\selectfont&#10;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#10;\normalfont&#10;</xsl:text>
    <xsl:text>&#10;\end{center}&#10;</xsl:text>
</xsl:template>


<!-- speech template --> 
<xsl:template match="speech">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="speech/ref" />

<xsl:template match="bqr_objet">
    <xsl:text>Objet : </xsl:text>
    <xsl:apply-templates/> 
    <xsl:text>&#10;&#10;</xsl:text>
</xsl:template>


<!-- itemizedlist template --> 
<xsl:template match="itemizedlist">
    <xsl:apply-templates select="node()[not(self::listitem)]"/>
    <xsl:variable name="level" select="count(ancestor-or-self::itemizedlist)"/>
    <xsl:choose>
        <xsl:when test="$level='1'">
            <xsl:text>&#10;\begin{itemizedlist1}</xsl:text>
            <xsl:apply-templates select="listitem"/>
            <xsl:text>\end{itemizedlist1}&#10;</xsl:text>
        </xsl:when>
        <xsl:when test="$level='2'">
            <xsl:text>&#10;\begin{itemizedlist2}</xsl:text>
            <xsl:apply-templates select="listitem"/>
            <xsl:text>\end{itemizedlist2}&#10;</xsl:text>
        </xsl:when>
        <xsl:when test="$level='3'">
            <xsl:text>&#10;\begin{itemizedlist3}</xsl:text>
            <xsl:apply-templates select="listitem"/>
            <xsl:text>\end{itemizedlist3}&#10;</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:text>&#10;\begin{itemize}</xsl:text>
            <xsl:apply-templates select="listitem"/>
            <xsl:text>\end{itemize}&#10;</xsl:text>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="footnote">
    <xsl:variable name="table" select="ancestor::table"/>
    <xsl:variable name="emphasis" select="ancestor::emphasis[@role='underline']"/>

    <xsl:choose>
        <xsl:when test="$table">
            <xsl:variable name="table.type">
                <xsl:call-template name="table.get-type">
                    <xsl:with-param name="table" select="$table"/>
                </xsl:call-template>
            </xsl:variable>

            <xsl:choose>
                <xsl:when test="$table.type = '1'">
                    <xsl:variable name="cur-footnote-node" select="."/>
                    <xsl:variable name="footnote-index">
                        <xsl:for-each select="ancestor::table//footnote">
                            <xsl:if test="$cur-footnote-node = .">
                                <xsl:value-of select="position()"/>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:variable>
                    <xsl:text>\tnote{</xsl:text>
                    <xsl:number value="$footnote-index" format="a" />
                    <!--xsl:value-of select="$footnote-index"/ -->
                    <xsl:text>}</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="label.id"/>
                    <xsl:text>\begingroup\catcode`\#=12\footnote{</xsl:text>
                    <xsl:apply-templates/>
                    <xsl:text>}\endgroup\docbooktolatexmakefootnoteref{</xsl:text>
                    <xsl:call-template name="generate.label.id"/>
                    <xsl:text>}</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:when>
        
        <xsl:when test="$emphasis">
            <xsl:text>}}</xsl:text>
            <xsl:call-template name="label.id"/>
            <xsl:text>\begingroup\catcode`\#=12\footnote{</xsl:text>
            <xsl:apply-templates/>
            <xsl:text>}\endgroup\docbooktolatexmakefootnoteref{</xsl:text>
            <xsl:call-template name="generate.label.id"/>
            <xsl:text>}</xsl:text>
            <xsl:text>{\uline{</xsl:text>

        </xsl:when>

        <xsl:otherwise>
            <xsl:call-template name="label.id"/>
            <xsl:text>\begingroup\catcode`\#=12\footnote{</xsl:text>
            <xsl:apply-templates/>
            <xsl:text>}\endgroup\docbooktolatexmakefootnoteref{</xsl:text>
            <xsl:call-template name="generate.label.id"/>
            <xsl:text>}</xsl:text>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<xsl:template name="footnote-syntax-replace">
    <xsl:param name="string"/>
    <xsl:call-template name="string-replace">
        <xsl:with-param name="string">
            <xsl:call-template name="string-replace">
                <xsl:with-param name="string" select="$string"/>
                <xsl:with-param name="from">[%</xsl:with-param>
                <xsl:with-param name="to">\footnote{</xsl:with-param>
            </xsl:call-template>
        </xsl:with-param>
        <xsl:with-param name="from">%]</xsl:with-param>
        <xsl:with-param name="to">}</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template name="footnote-syntax-replace-protect-from-uppercase">
    <xsl:param name="string"/>
    <xsl:call-template name="string-replace">
        <xsl:with-param name="string">
            <xsl:call-template name="string-replace">
                <xsl:with-param name="string" select="$string"/>
                <xsl:with-param name="from">[%</xsl:with-param>
                <xsl:with-param name="to">}\footnote{</xsl:with-param>
            </xsl:call-template>
        </xsl:with-param>
        <xsl:with-param name="from">%]</xsl:with-param>
        <xsl:with-param name="to">}\uppercase{</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template name="escape-percent">
    <xsl:param name="string"/>
    <xsl:call-template name="string-replace">
        <xsl:with-param name="string" select="$string"/>
        <xsl:with-param name="from">%</xsl:with-param>
        <xsl:with-param name="to">\%</xsl:with-param>
    </xsl:call-template>
</xsl:template>

</xsl:stylesheet>
