<?xml version="1.0" encoding="utf-8"?>
<!--
Tabellio - software suite for deliberative assemblies
         - suite logicielle pour assemblées délibératives
         - http://www.tabellio.org/
Copyright (C) 2006 Parlement de la Communauté française de Belgique

This file is part of Tabellio.

Tabellio is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tabellio is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!--  FileName: legistic.xsl                    --> 

<!-- legisitic templates --> 

<xsl:template name="extract.object.subtitle">
    <xsl:param name="object"  select="."/>
    <xsl:choose>
        <xsl:when test="$latex.apply.title.templates='1'">
            <xsl:apply-templates select="$object/subtitle" mode="latex"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="normalize-scape">
                <xsl:with-param name="string" select="$object/subtitle"/>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template> 


<xsl:template name="startLegisticCounter_List">
    <xsl:if test="not (preceding-sibling::*[contains(name(),'legistic')]) and not (ancestor::*[contains(name(),'legistic')])">
        <xsl:text>&#10;% ------------------------   &#10;</xsl:text>
        <xsl:text>% Debut d'une structure Legistique - </xsl:text>
        <xsl:text>&#10;% ------------------------   &#10;</xsl:text>
        <xsl:text>\legidebut&#10;</xsl:text>
    </xsl:if>
</xsl:template>

<!-- template for legistic_section node --> 
<xsl:template name="generate_legistic_section">
    <xsl:param name="current_node"  select="."/>
    <xsl:param name="level"/>
    <xsl:param name="latex-tag"/>

    <xsl:call-template name="startLegisticCounter_List"/>

    <xsl:param name="string1">
        <xsl:call-template name="extract.object.title">
            <xsl:with-param name="object" select="$current_node"/>
        </xsl:call-template>
    </xsl:param>
    <xsl:param name="subtitle">
        <xsl:call-template name="extract.object.subtitle">
            <xsl:with-param name="object" select="$current_node"/>
        </xsl:call-template>
    </xsl:param>
    <xsl:param name="use.label"	select="1"/>
    <xsl:param name="use.hypertarget" 	select="1"/>
    <xsl:variable name="id">
        <xsl:call-template name="generate.label.id">
            <xsl:with-param name="object" select="$current_node"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="title">
        <xsl:choose>
            <xsl:when test="$string1=''">
                <xsl:call-template name="gentext.element.name"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="normalize-space($string1)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:param name="title-toc">
        <xsl:apply-templates select="$current_node/title" mode="toc"/>
    </xsl:param>
    <xsl:param name="subtitle-toc">
        <xsl:apply-templates select="$current_node/subtitle" mode="toc"/>
    </xsl:param>

    <xsl:if test="not($current_node[position()=2])">
        <xsl:text>\pagebreak[2]&#10;</xsl:text>
    </xsl:if>

    <xsl:text>&#10;% ------------------------   &#10;</xsl:text>
    <xsl:text>% begin legistique - </xsl:text>
    <xsl:value-of select="$level"/>
    <xsl:text>)&#10;\</xsl:text>
    <xsl:value-of select="$latex-tag"/>
    <xsl:text>{</xsl:text>
    <xsl:value-of select="$title-toc"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="$title"/>
    <xsl:text>}</xsl:text>
    <xsl:text>&#10;\label{</xsl:text>
    <xsl:value-of select="$id"/>
    <xsl:text>}\hypertarget{</xsl:text>
    <xsl:value-of select="$id"/>
    <xsl:text>}{}%&#10;</xsl:text>
    <xsl:call-template name="content-templates"/>
    <xsl:text>% end legistique -</xsl:text>
    <xsl:value-of select="$level"/>
    <xsl:text>)&#10;</xsl:text>
    <xsl:text>% ------------------------   &#10;</xsl:text>
</xsl:template>

<xsl:template match="legistic_part">
    <xsl:call-template name="generate_legistic_section">
        <xsl:with-param name="current_node" select="."/>
        <xsl:with-param name="level">Legistique Partie</xsl:with-param>
        <xsl:with-param name="latex-tag">legipart</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template match="legistic_book">
    <xsl:call-template name="generate_legistic_section">
        <xsl:with-param name="current_node" select="."/>
        <xsl:with-param name="level">Legistique Livre</xsl:with-param>
        <xsl:with-param name="latex-tag">legilivre</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template match="legistic_title">
    <xsl:call-template name="generate_legistic_section">
        <xsl:with-param name="current_node" select="."/>
        <xsl:with-param name="level">Legistique Titre</xsl:with-param>
        <xsl:with-param name="latex-tag">legititre</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template match="legistic_chapter">
    <xsl:call-template name="generate_legistic_section">
        <xsl:with-param name="current_node" select="."/>
        <xsl:with-param name="level">Legistique Chapitre</xsl:with-param>
        <xsl:with-param name="latex-tag">legichapitre</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template match="legistic_section">
    <xsl:call-template name="generate_legistic_section">
        <xsl:with-param name="current_node" select="."/>
        <xsl:with-param name="level">Legistique Section</xsl:with-param>
        <xsl:with-param name="latex-tag">legisection</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template match="legistic_subsection">
    <xsl:call-template name="generate_legistic_section">
        <xsl:with-param name="current_node" select="."/>
        <xsl:with-param name="level">Legistique Sous-section</xsl:with-param>
        <xsl:with-param name="latex-tag">legisoussection</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<!-- template for legistic_section node --> 
<xsl:template match="legistic_section_old">
    <xsl:param name="level" select="count(ancestor::legistic_section)+1"/>
    <xsl:param name="string1">
        <xsl:call-template name="extract.object.title">
            <xsl:with-param name="object" select="."/>
        </xsl:call-template>
    </xsl:param>
    <xsl:param name="subtitle">
        <xsl:call-template name="extract.object.subtitle">
            <xsl:with-param name="object" select="."/>
        </xsl:call-template>
    </xsl:param>
    <xsl:param name="use.label"	select="1"/>
    <xsl:param name="use.hypertarget" 	select="1"/>
    <xsl:variable name="id">
        <xsl:call-template name="generate.label.id">
            <xsl:with-param name="object" select="."/>
        </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="title">
        <xsl:choose>
            <xsl:when test="$string1=''">
                <xsl:call-template name="gentext.element.name"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="normalize-space($string1)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:param name="title-toc">
        <xsl:apply-templates select="./title" mode="toc"/>
    </xsl:param>
    <xsl:param name="subtitle-toc">
        <xsl:apply-templates select="./subtitle" mode="toc"/>
    </xsl:param>

    <xsl:param name="section-tag">
        <xsl:choose>
            <xsl:when test='$level = 1'>
                <xsl:text>legisection</xsl:text>
            </xsl:when>
            <xsl:when test='$level = 2'>
                <xsl:text>legisubsection</xsl:text>
            </xsl:when>
            <xsl:when test='$level = 3'>
                <xsl:text>legisubsubsection</xsl:text>
            </xsl:when>
            <xsl:when test='$level = 4'>
                <xsl:text>legiparagraph</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>legisubparagraph</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:param>

    <xsl:text>&#10;% ------------------------   &#10;</xsl:text>
    <xsl:text>% begin legistic section (level: </xsl:text>
    <xsl:value-of select="$level"/>
    <xsl:text>)&#10;\</xsl:text>
    <xsl:value-of select="$section-tag"/>
    <xsl:text>{</xsl:text>
    <xsl:value-of select="$title-toc"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="$title"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="$subtitle-toc"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="$subtitle"/>
    <xsl:text>}</xsl:text>
    <xsl:text>&#10;\label{</xsl:text>
    <xsl:value-of select="$id"/>
    <xsl:text>}\hypertarget{</xsl:text>
    <xsl:value-of select="$id"/>
    <xsl:text>}{}%&#10;</xsl:text>

    <xsl:call-template name="content-templates"/>

    <xsl:text>% end legistic section (level: </xsl:text>
    <xsl:value-of select="$level"/>
    <xsl:text>)&#10;</xsl:text>
    <xsl:text>% ------------------------   &#10;</xsl:text>
</xsl:template>

<xsl:template match="legistic_article">
    <xsl:call-template name="startLegisticCounter_List"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\legiarticle&#10;</xsl:text>
    <xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template match="legistic_manualarticle">
    <xsl:call-template name="startLegisticCounter_List"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\legimanualarticle{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}&#10;</xsl:text>
    <xsl:text>&#10;</xsl:text>
</xsl:template>


</xsl:stylesheet>
