<?xml version="1.0" encoding="utf-8"?>
<!--
Tabellio - software suite for deliberative assemblies
         - suite logicielle pour assemblées délibératives
         - http://www.tabellio.org/
Copyright (C) 2006 Parlement de la Communauté française de Belgique

This file is part of Tabellio.

Tabellio is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tabellio is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!--  FileName: image.xsl                    --> 
<!-- 
Purpose: New template to convert mediaobject to figure
The template generates LaTeX commands to include JPG or PNG if the .LEGI contents this type of files. 
Otherwise the template generates LaTeX commands for PDF files (WMF->EPS->PDF see python script). 
--> 
<xsl:param name="bqr"/>

<xsl:template match="mediaobject">
    <xsl:choose>
        <xsl:when test="imageobject/imagedata[@format='JPG']">
            <xsl:call-template name="generateForMedia">
                <xsl:with-param name="imagedata" select="imageobject/imagedata[@format='JPG']"/>
                <xsl:with-param name="mediaobject" select="."/>
                <xsl:with-param name="fileref" select="imageobject/imagedata[@format='JPG']/@fileref"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:when test="imageobject/imagedata[@format='PNG']">
            <xsl:call-template name="generateForMedia">
                <xsl:with-param name="imagedata" select="imageobject/imagedata[@format='PNG']"/>
                <xsl:with-param name="mediaobject" select="."/>
                <xsl:with-param name="fileref" select="imageobject/imagedata[@format='PNG']/@fileref"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <!-- We specify a scale factor of 0.567 because the size/scale are no correct. -->
            <xsl:call-template name="generateForMedia">
                <xsl:with-param name="imagedata" select="imageobject/imagedata[@format='WMF']"/>
                <xsl:with-param name="mediaobject" select="."/>
                <xsl:with-param name="factor" select="string(0.567)"/>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="generateForMedia">
    <xsl:param name="imagedata"/>
    <xsl:param name="mediaobject"/>
    <xsl:param name="factor" select="string(1)"/>
    <xsl:param name="fileref" select="substring-before($imagedata/@fileref, '.wmf')"/>
    <xsl:if test="$fileref != ''">
        <xsl:variable name="caption" select="$mediaobject/caption"/>
        <xsl:variable name="captionTex">
            <xsl:if test="$caption">
                <xsl:value-of select="$latex.table.caption.style"/>
                <xsl:text>\caption{</xsl:text>
                <xsl:if test="$caption/@id">
                    <xsl:text>\label{</xsl:text>
                    <xsl:value-of select="$caption/@id"/>
                    <xsl:text>}</xsl:text>
                </xsl:if>
                <xsl:call-template name="label.id"/>
                <xsl:apply-templates select="$caption" mode="caption.mode"/>
                <xsl:text>}</xsl:text> 
            </xsl:if>
        </xsl:variable>

        <xsl:if test="$bqr">
            <xsl:text>&#10;</xsl:text>
            <xsl:text>\end{multicols}&#10;</xsl:text>
        </xsl:if>

        <xsl:text>&#10;\setlength{\mytempwidth}{</xsl:text>
        <xsl:value-of select="$imagedata/@width"/>
        <xsl:text>}&#10;</xsl:text> 
        <xsl:text>\setlength{\mytempwidth}{</xsl:text>
        <xsl:value-of select="$factor"/>
        <xsl:text>\mytempwidth}&#10;</xsl:text>

        <xsl:text>&#10;\setlength{\mytempdepth}{</xsl:text>
        <xsl:value-of select="$imagedata/@depth"/>
        <xsl:text>}&#10;</xsl:text> 
        <xsl:text>\setlength{\mytempdepth}{</xsl:text>
        <xsl:value-of select="$factor"/>
        <xsl:text>\mytempdepth}&#10;</xsl:text>

        <xsl:if test="$imagedata/@scalex">
            <xsl:text>\setlength{\mytempwidth}{0.01\mytempwidth}&#10;</xsl:text>
            <xsl:text>\setlength{\mytempwidth}{</xsl:text>
            <xsl:value-of select="$imagedata/@scalex"/>
            <xsl:text>\mytempwidth}&#10;</xsl:text>
        </xsl:if>

        <xsl:if test="$imagedata/@scaley">
            <xsl:text>\setlength{\mytempdepth}{0.01\mytempdepth}&#10;</xsl:text>
            <xsl:text>\setlength{\mytempdepth}{</xsl:text>
            <xsl:value-of select="$imagedata/@scaley"/>
            <xsl:text>\mytempdepth}&#10;</xsl:text>
        </xsl:if>

        <xsl:choose>
            <xsl:when test="$bqr">
                <xsl:text>&#10;</xsl:text>
                <xsl:text>\begin{center}&#10;</xsl:text>
                <xsl:if test="$mediaobject/caption">
                    <xsl:text>\bqrfigure{</xsl:text>
                    <xsl:apply-templates select="$mediaobject/caption" mode="caption.mode"/>
                    <xsl:text>}&#10;</xsl:text>
                </xsl:if>
                <xsl:text>\ifthenelse{\mytempwidth &lt; \textwidth}&#10;</xsl:text> 
                <xsl:text>{&#10;</xsl:text>
                <xsl:text>\resizebox{\mytempwidth}{\mytempdepth}{\includegraphics{</xsl:text>
                <xsl:value-of select="$fileref"/>
                <xsl:text>}}&#10;</xsl:text>
                <xsl:text>}&#10;</xsl:text>
                <xsl:text>{&#10;</xsl:text>
                <xsl:text>\resizebox{\textwidth}{!}{\includegraphics{</xsl:text>
                <xsl:value-of select="$fileref"/>
                <xsl:text>}}&#10;</xsl:text>
                <xsl:text>}&#10;</xsl:text>
                <xsl:text>\end{center}&#10;</xsl:text>
                <xsl:text>&#10;</xsl:text>
                <xsl:text>\begin{multicols}{2}&#10;</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>\ifthenelse{\mytempwidth &lt; \columnwidth}&#10;</xsl:text> 
                <xsl:text>{&#10;</xsl:text>
                <xsl:text>\begin{figure}[htb]&#10;</xsl:text>
                <xsl:text>\begin{center}&#10;</xsl:text>
                <xsl:value-of select="$captionTex"/>
                <xsl:text>\resizebox{\mytempwidth}{\mytempdepth}{\includegraphics{</xsl:text>
                <xsl:value-of select="$fileref"/>
                <xsl:text>}}&#10;</xsl:text>
                <xsl:text>\end{center}&#10;</xsl:text>
                <xsl:text>\end{figure}&#10;</xsl:text>
                <xsl:text>}&#10;{&#10;\begin{figure*}[htb]&#10;</xsl:text>
                <xsl:text>\begin{center}&#10;</xsl:text>
                <xsl:value-of select="$captionTex"/>
                <xsl:text>\ifthenelse{\mytempwidth &lt; \textwidth}&#10;</xsl:text> 
                <xsl:text>{&#10;</xsl:text>
                <xsl:text>\resizebox{\mytempwidth}{\mytempdepth}{\includegraphics{</xsl:text>
                <xsl:value-of select="$fileref"/>
                <xsl:text>}}&#10;</xsl:text>
                <xsl:text>}&#10;</xsl:text>
                <xsl:text>{&#10;</xsl:text>
                <xsl:text>\resizebox{\textwidth}{!}{\includegraphics{</xsl:text>
                <xsl:value-of select="$fileref"/>
                <xsl:text>}}&#10;</xsl:text>
                <xsl:text>}&#10;</xsl:text>
                <xsl:text>\end{center}&#10;</xsl:text>
                <xsl:text>\end{figure*}&#10;</xsl:text>
                <xsl:text>}&#10;</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:if>
</xsl:template>

</xsl:stylesheet>
