<?xml version='1.0' encoding="utf-8"?>
<!--
Tabellio - software suite for deliberative assemblies
         - suite logicielle pour assemblées délibératives
         - http://www.tabellio.org/
Copyright (C) 2006 Parlement de la Communauté française de Belgique

This file is part of Tabellio.

Tabellio is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tabellio is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!--  FileName: appendix.xsl                    --> 

<xsl:param name="parchemin"/>

<!-- appendix template --> 
<xsl:template match="appendix">
    <xsl:if test="not (preceding-sibling::appendix)">
        <xsl:text>&#10;</xsl:text>
        <xsl:text>%\balance&#10;</xsl:text> 
        <xsl:text>% ------------------------------------------------------------- &#10;</xsl:text>
        <xsl:text>% Appendices start here&#10;</xsl:text>
        <xsl:text>% -------------------------------------------------------------&#10;</xsl:text>
    </xsl:if>

    <xsl:variable name="object"  select="."/>
    <xsl:variable name="keyword" select="local-name($object)"/> 
    <xsl:variable name="id">
        <xsl:call-template name="generate.label.id">
            <xsl:with-param name="object" select="$object"/>
        </xsl:call-template>
    </xsl:variable> 
    
    <xsl:choose>
        <xsl:when test="@type='legi'">
            <xsl:text>%\balance&#10;</xsl:text> 
            <xsl:text>&#10;</xsl:text>
            <xsl:text>% -------------------------------------------------------------&#10;</xsl:text>
            <xsl:text>% appendix:  </xsl:text>
            <xsl:value-of select="title"/>
            <xsl:text>&#10;</xsl:text>
            <xsl:text>% -------------------------------------------------------------&#10;</xsl:text>
            <xsl:if test="title!=''">
                <xsl:text>\twocolumn[\pcfannexe{</xsl:text>
                <xsl:value-of select="title"/>
                <xsl:text>}&#10;</xsl:text>
                <xsl:text>\label{</xsl:text>
                <xsl:value-of select="$id"/>
                <xsl:text>}\hypertarget{</xsl:text>
                <xsl:value-of select="$id"/>
                <xsl:text>}{}%&#10;</xsl:text>
                <xsl:text>]&#10;</xsl:text>
            </xsl:if>
            <xsl:call-template name="content-templates"/>
        </xsl:when>
        <xsl:when test="@type='pdf'">
            <xsl:text>%\balance&#10;</xsl:text> 
            <xsl:text>&#10;</xsl:text>
            <xsl:text>% -------------------------------------------------------------&#10;</xsl:text>
            <xsl:text>% appendix:  </xsl:text>
            <xsl:value-of select="title"/>
            <xsl:text>&#10;</xsl:text>
            <xsl:text>% -------------------------------------------------------------&#10;</xsl:text>

        
            <xsl:choose>
                <xsl:when test="$parchemin">
                    <xsl:if test="title!=''">
                        <xsl:text>\twocolumn[\pcfannexe{</xsl:text>
                        <xsl:value-of select="title"/>
                        <xsl:text>}&#10;</xsl:text>
                        <xsl:text>\label{</xsl:text>
                        <xsl:value-of select="$id"/>
                        <xsl:text>}\hypertarget{</xsl:text>
                        <xsl:value-of select="$id"/>
                        <xsl:text>}{}%&#10;</xsl:text>
                        <xsl:text>]&#10;</xsl:text>
                    </xsl:if>
                    <xsl:text>\onecolumn&#10;</xsl:text> 
                    <xsl:text>\includepdf[pages={-},offset=15.5mm -15.5mm,scale=</xsl:text>
                    <xsl:value-of select="pdf-annex/@scale"/>
                    <xsl:text>]{</xsl:text>
                    <xsl:value-of select="pdf-annex/@pdf-file"/>
                    <xsl:text>}&#10;</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="title!=''">
                        <xsl:text>\twocolumn[\pcfannexe{</xsl:text>
                        <xsl:value-of select="title"/>
                        <xsl:text>}&#10;</xsl:text>
                        <xsl:text>\label{</xsl:text>
                        <xsl:value-of select="$id"/>
                        <xsl:text>}\hypertarget{</xsl:text>
                        <xsl:value-of select="$id"/>
                        <xsl:text>}{}%&#10;</xsl:text>
                        <xsl:text>]&#10;</xsl:text>
                    </xsl:if>
                    <xsl:text>\onecolumn&#10;</xsl:text> 
                    <xsl:text>\includepdf[pages={-},pagecommand={\pagestyle{fancy}},scale=</xsl:text>
                    <xsl:value-of select="pdf-annex/@scale"/>
                    <xsl:text>]{</xsl:text>
                    <xsl:value-of select="pdf-annex/@pdf-file"/>
                    <xsl:text>}&#10;</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
