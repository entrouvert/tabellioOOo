<?xml version="1.0" encoding="utf-8"?>
<!--
Tabellio - software suite for deliberative assemblies
         - suite logicielle pour assemblées délibératives
         - http://www.tabellio.org/
Copyright (C) 2006 Parlement de la Communauté française de Belgique

This file is part of Tabellio.

Tabellio is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tabellio is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!--  FileName: db2latex_custom.xsl                    --> 
<!-- 
      Purpose: re-definition of the db2latex variables.
--> 

<xsl:param name="with-draft-tag"/>
<xsl:param name="parchemin"/>

<!-- Customization of Db2Latex product -->
<xsl:variable name="latex.mapping.xml" select="document('local.mapping.xml')"/>
<xsl:variable name="latex.inputenc">utf8</xsl:variable>
<xsl:variable name="latex.document.font">sabon</xsl:variable> 
<xsl:variable name="admon.graphics.path">/home/rfl/db2latex/xsl/figures</xsl:variable> 

<xsl:variable name="latex.documentclass.book">
    <xsl:choose>
        <xsl:when test="$parchemin">notitlepage,twocolumn,openany,10pt</xsl:when>
        <xsl:otherwise>a4paper,twocolumn,notitlepage,openany,twoside,10pt</xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="latex.documentclass">PCFlegacy</xsl:variable> 
<xsl:variable name="latex.titlepage.file"></xsl:variable>
<xsl:variable name="latex.maketitle"></xsl:variable>
<xsl:variable name="latex.book.preamble.pre"></xsl:variable> 
<xsl:param name="latex.hyperref.param.common">bookmarksnumbered,colorlinks=false,backref, bookmarks, breaklinks, linktocpage</xsl:param>
 

<xsl:param name="latex.book.begindocument">
    <xsl:text>\begin{document}&#10;</xsl:text>
    <xsl:text>% --------------------------------------------&#10;</xsl:text>
    <xsl:text>% Begin of document&#10;</xsl:text>
    <xsl:text>% --------------------------------------------&#10;</xsl:text>
    <xsl:if test="$parchemin">
        <xsl:text>\setlength{\pdfpagewidth}{241mm}&#10;</xsl:text>
        <xsl:text>\setlength{\pdfpageheight}{317mm}&#10;</xsl:text>
        <xsl:text>\setlength{\columnwidth}{226pt}</xsl:text>
    </xsl:if>
    <xsl:text>\renewcommand{\labelitemi}{$\bullet$}&#10;</xsl:text>
    <xsl:text>\renewcommand{\labelitemii}{$\bullet$}&#10;</xsl:text>
    <xsl:text>\renewcommand{\labelitemiii}{$\bullet$}&#10;</xsl:text>
    <xsl:text>\renewcommand{\labelitemiv}{$\bullet$}&#10;</xsl:text>
    <xsl:text>\renewcommand{\labelenumi}{( \roman{enumi} )}&#10;</xsl:text>
    <xsl:text>\renewcommand{\labelenumii}{\arabic{enumii}}&#10;</xsl:text>
    <xsl:text>&#10;&#10;</xsl:text>
    <xsl:text>\setcounter{secnumdepth}{5}&#10;</xsl:text>
    <xsl:text>\setcounter{tocdepth}{4}&#10;</xsl:text>
    <xsl:text>&#10;&#10;</xsl:text>

    <xsl:if test="$with-draft-tag">
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\AddToShipoutPicture{% watermark&#10;</xsl:text>
        <xsl:text>\AtTextCenter{\makebox[0pt]{\scalebox{6}{%&#10;</xsl:text>
        <xsl:text>\rotatebox[origin=c]{45}%&#10;</xsl:text>
        <xsl:text>{\color[gray]{.9}VERSION PROVISOIRE}}}}}&#10;</xsl:text>
        <xsl:text>&#10;</xsl:text>
    </xsl:if>
    
</xsl:param>

<xsl:template name="latex.float.preamble">
</xsl:template>


<xsl:template name="latex.hyperref.preamble">
</xsl:template> 

<xsl:param name="latex.pdf.preamble">
    <xsl:text>\usepackage{ifthen}&#10;</xsl:text>
    <xsl:text>\usepackage[T1]{fontenc}&#10;</xsl:text>
    <xsl:text>\usepackage{ucs}&#10;</xsl:text>
    <xsl:text>\usepackage{relsize}&#10;</xsl:text>
    <xsl:text>% --------------------------------------------&#10;</xsl:text>
    <xsl:text>% Check for PDFLaTeX/LaTeX &#10;</xsl:text>
    <xsl:text>% --------------------------------------------&#10;</xsl:text>
    <xsl:text>\newif\ifpdf&#10;</xsl:text>
    <xsl:text>\ifx\pdfoutput\undefined&#10;</xsl:text>
    <xsl:text>\pdffalse % we are not running PDFLaTeX&#10;</xsl:text>
    <xsl:text>\else&#10;</xsl:text>
    <xsl:text>\pdfoutput=1 % we are running PDFLaTeX&#10;</xsl:text>
    <xsl:text>\pdftrue&#10;</xsl:text>
    <xsl:text>\fi&#10;</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\usepackage[pdftex,&#10;</xsl:text>
    <xsl:text>            colorlinks,&#10;</xsl:text>
    <xsl:text>            urlcolor=rltblack,&#10;</xsl:text> 
    <xsl:text>            filecolor=rltblack,&#10;</xsl:text>
    <xsl:text>            linkcolor=rltblack,&#10;</xsl:text>
    <xsl:text>            pdftitle={My Document},&#10;</xsl:text>
    <xsl:text>            pdfauthor={Tabellio},&#10;</xsl:text>
    <xsl:text>            pdfproducer={pdfLaTeX},&#10;</xsl:text>
    <xsl:text>            bookmarksnumbered,&#10;</xsl:text>
    <xsl:text>            backref, &#10;</xsl:text>
    <xsl:text>            bookmarks=false, &#10;</xsl:text>
    <xsl:text>            breaklinks, &#10;</xsl:text>
    <xsl:text>            linktocpage,&#10;</xsl:text>
    <xsl:text>            pdfstartview=FitH]{hyperref}&#10;</xsl:text>
    <xsl:if test="$bqr">
        <xsl:text>\usepackage{multicol}&#10;</xsl:text>
    </xsl:if>
    <xsl:text>&#10;</xsl:text>

    
    <xsl:if test="$with-draft-tag">
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\usepackage{eso-pic}&#10;</xsl:text>
        <xsl:text>&#10;</xsl:text>
    </xsl:if>

    
</xsl:param>

<xsl:variable name="latex.book.preamble.post">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\makeatletter&#10;</xsl:text>
    <xsl:text>\renewcommand*\l@subsection{\@dottedtocline{2}{1.5em}{3.3em}}&#10;</xsl:text>
    <xsl:text>\renewcommand*\l@subsubsection{\@dottedtocline{3}{3.8em}{4.3em}} &#10;</xsl:text>
    <xsl:text>\renewcommand*\l@paragraph{\@dottedtocline{4}{7.0em}{5em}&#10;</xsl:text>}
    <xsl:text>\renewcommand*\l@subparagraph{\@dottedtocline{5}{10em}{5em}} &#10;</xsl:text>
    <xsl:text>\makeatother&#10;</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\setlength{\hoffset}{-0.46cm}&#10;</xsl:text>
    <xsl:text>\setlength{\voffset}{-0.56cm}&#10;</xsl:text>
    <xsl:text>\setlength{\textwidth}{15.9cm}&#10;</xsl:text>
    <xsl:text>\setlength{\textheight}{24.3cm} &#10;</xsl:text>
    <xsl:text>\setlength{\oddsidemargin}{0.55cm}&#10;</xsl:text>
    <xsl:text>\setlength{\evensidemargin}{0.55cm}&#10;</xsl:text>
    <xsl:text>\setlength{\topmargin}{0.1cm}&#10;</xsl:text>
    <xsl:text>\setlength{\headheight}{0.4cm}&#10;</xsl:text>
    <xsl:text>\setlength{\headsep}{0.6cm}&#10;</xsl:text>
    <xsl:text>\setlength{\marginparwidth}{0cm}&#10;</xsl:text>
    <xsl:text>\setlength{\marginparsep}{0cm}&#10;</xsl:text>
    <xsl:text>\setlength{\columnsep}{0.8cm} &#10;</xsl:text>
    <xsl:if test="$parchemin">
        <xsl:text>\setlength{\textheight}{26.5cm} &#10;</xsl:text>
        <xsl:text>\setlength{\oddsidemargin}{1.75cm}&#10;</xsl:text>
        <xsl:text>\setlength{\evensidemargin}{1.75cm}&#10;</xsl:text>
    </xsl:if>
    <xsl:if test="$bqr">
        <xsl:text>\renewcommand{\thesubsection}{}&#10;</xsl:text>
    </xsl:if>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>% --------------------------------------&#10;</xsl:text>
    <xsl:text>% Definition des headers / footers&#10;</xsl:text>
    <xsl:text>% --------------------------------------&#10;</xsl:text>
    <xsl:text>\newenvironment{literallayout}&#10;</xsl:text>
    <xsl:text>{\fontfamily{psb}\selectfont}{}&#10;</xsl:text>
    <xsl:text>&#10;</xsl:text> 
    <xsl:text>\renewcommand{\headrulewidth}{0pt}&#10;</xsl:text>
    <xsl:text>\renewcommand{\footrulewidth}{0pt}&#10;</xsl:text>
    <xsl:text>\fancyhead[EL,OR]{\footnotesize{</xsl:text>
    <xsl:value-of select="$pdfgen.document.id"/>
    <xsl:text>}}&#10;</xsl:text>
    <xsl:text>\fancyhead[C]{\footnotesize{( \thepage{} )}}&#10;</xsl:text>
    <xsl:text>\fancyhead[ER,OL]{}&#10;</xsl:text>
    <xsl:text>\fancyfoot{}&#10;</xsl:text>
    <xsl:text>&#10;</xsl:text>
    
</xsl:variable> 

<xsl:param name="latex.book.end">
    <xsl:if test="$parchemin">
        <xsl:text>\end{changemargin}&#10;</xsl:text>
        <xsl:text>% --------------------------------------------&#10;</xsl:text>
        <xsl:text>% Last part: Parchemin&#10;</xsl:text>
        <xsl:text>% --------------------------------------------&#10;</xsl:text>
        <xsl:text>&#10;</xsl:text>
        <xsl:text>\newpage&#10;</xsl:text>
        <xsl:text>\begin{list}{}{&#10;</xsl:text>
        <xsl:text>\setlength{\parsep}{0.2cm plus 1pt}&#10;</xsl:text>
        <xsl:text>\addtolength{\leftmargin}{3cm}&#10;</xsl:text>
        <xsl:text>\addtolength{\rightmargin}{3cm}&#10;</xsl:text>
        <xsl:text>}&#10;</xsl:text>
        <xsl:text>\item&#10;</xsl:text>
        
        <xsl:text>\center{\large{ADOPT\'{E} PAR}} \\&#10;</xsl:text>
        <xsl:text>\center{\large{LE PARLEMENT DE LA COMMUNAUT\'{E}}} \\&#10;</xsl:text>
        <xsl:text>\center{\large{FRAN\c{C}AISE}} \\[1.5cm]&#10;</xsl:text>
    
        <xsl:if test="/book/metadata/property[@name='date']">
            <xsl:text>\begin{flushleft}Bruxelles, le </xsl:text>
            <xsl:value-of select="/book/metadata/property[@name='date']"/>
            <xsl:text>. \\[1.5cm] \end{flushleft}&#10;</xsl:text>
        </xsl:if>

        <xsl:text>\center{\textit{Le Pr\'{e}sident}} \\&#10;</xsl:text>
        <xsl:text>\center{\textit{du Parlement de la Communaut\'{e} fran\c{c}aise,}} \\[5.5cm]&#10;</xsl:text>
        <xsl:text>\center{\textit{Les Secr\'{e}taires,}} \\[5.5cm]&#10;</xsl:text>
        <xsl:text>\center{\textit{Le Greffier,}} \\&#10;</xsl:text>
        <xsl:text>\newpage&#10;</xsl:text>
        <xsl:text>\begin{flushleft}Promulguons le pr\'{e}sent d\'{e}cret, ordonnons qu'il soit publi\'{e} au \textit{Moniteur belge}. \\[1.5cm]&#10;</xsl:text>
        <xsl:text>\end{flushleft}&#10;</xsl:text>
        <xsl:text>\begin{flushleft}Donn\'{e} \`a\end{flushleft}&#10;</xsl:text>
        <xsl:text>\end{list}&#10;</xsl:text>
        <xsl:text>&#10;</xsl:text>
    </xsl:if>

    
    <xsl:text>% --------------------------------------------&#10;</xsl:text>
    <xsl:text>% End of document&#10;</xsl:text>
    <xsl:text>% --------------------------------------------&#10;</xsl:text>
</xsl:param> 



</xsl:stylesheet>
