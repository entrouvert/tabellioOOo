<?xml version="1.0" encoding="utf-8"?>
<!--
Tabellio - software suite for deliberative assemblies
         - suite logicielle pour assemblées délibératives
         - http://www.tabellio.org/
Copyright (C) 2006 Parlement de la Communauté française de Belgique

This file is part of Tabellio.

Tabellio is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tabellio is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>
<xsl:output method="xml" encoding="utf-8" indent="yes"/>

<xsl:param name="annex-id"/>
<xsl:param name="annex-title"/>

<!--  FileName: annex_pre_proc.xsl                    --> 
<!-- 
      Purpose: Preprocessing rules to transform the chapter / sect(xxx) 
      to section.
--> 

<xsl:template match="/">
    <xsl:element name="appendix">
        <xsl:attribute name="type">legi</xsl:attribute>
        <xsl:element name="title">
            <xsl:value-of select="$annex-title"/>
        </xsl:element>
        <xsl:apply-templates select="book/*"/>
    </xsl:element>
</xsl:template>

<xsl:template match="metadata">
</xsl:template>

<xsl:template match="imagedata/@fileref">
    <xsl:attribute name="fileref">
        <xsl:value-of select="concat($annex-id, . )"/>
    </xsl:attribute>
</xsl:template>

<xsl:template match="para">
    <xsl:choose>
        <xsl:when test="@role='legistic_article'">
            <xsl:element name="legistic_article">
                <xsl:apply-templates/>
            </xsl:element>
        </xsl:when>
        <xsl:when test="@role='legistic_manualarticle'">
            <xsl:element name="legistic_manualarticle">
                <xsl:apply-templates/>
            </xsl:element>
        </xsl:when>
        <xsl:otherwise>
            <xsl:element name="para">
                <xsl:apply-templates select="@*|node()"/>
            </xsl:element>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="chapter|sect1|sect2|sect3|sect4|sect5|sect6">
    <xsl:element name="section">
      <xsl:apply-templates/>
    </xsl:element>
</xsl:template>

<xsl:template match="@*|node()">
    <xsl:copy>
        <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
</xsl:template>

</xsl:stylesheet>
