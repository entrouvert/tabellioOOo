<?xml version="1.0" encoding="utf-8"?>
<!--
Tabellio - software suite for deliberative assemblies
         - suite logicielle pour assemblées délibératives
         - http://www.tabellio.org/
Copyright (C) 2006 Parlement de la Communauté française de Belgique

This file is part of Tabellio.

Tabellio is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tabellio is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!--  FileName: section.xsl                    --> 
<!-- 
      Purpose: Correct Db2Latex product to take into account the footnote 
      inside the title of a section.
      Instead of \section{title}, this template generates \section[title-for-toc]{title}
      'title-for-toc' (text for table of content) is similar as 'title' but without footnote.
--> 

<!-- templates customization of 'toc' mode; use for 'title-for-toc' definition --> 
<xsl:template match="footnote" mode="toc"/>
<xsl:template match="ulink" mode="toc"/>
<xsl:template match="link" mode="toc"/>
<xsl:template match="anchor" mode="toc"/>
<xsl:template match="ref" mode="toc"/>
<xsl:param name="bqr"/>


<!-- template for section node --> 



<xsl:template match="preface">
    <xsl:if test="./title">
        <xsl:call-template name="map.begin"/>
    </xsl:if>
    <xsl:call-template name="content-templates"/>
    <xsl:call-template name="map.end"/>
</xsl:template>



<xsl:template name="extract.object.subtitle">
    <xsl:param name="object"  select="."/>
    <xsl:choose>
        <xsl:when test="$latex.apply.title.templates='1'">
            <xsl:apply-templates select="$object/subtitle" mode="latex"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="normalize-scape">
                <xsl:with-param name="string" select="$object/subtitle"/>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template> 



<xsl:template match="section">
    <xsl:param name="bridgehead" select="ancestor::preface|ancestor::colophon|ancestor::dedication"/>
    <xsl:param name="level" select="count(ancestor::section)+1"/>
    <xsl:param name="string">
        <xsl:call-template name="extract.object.title">
            <xsl:with-param name="object" select="."/>
        </xsl:call-template>
    </xsl:param>
    <xsl:param name="use.label"	select="1"/>
    <xsl:param name="use.hypertarget" 	select="1"/>
    <xsl:variable name="id">
        <xsl:call-template name="generate.label.id">
            <xsl:with-param name="object" select="."/>
        </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="title">
        <xsl:choose>
            <xsl:when test="starts-with(local-name(.),'informal')">
                <xsl:if test="$string!=''">
                    <xsl:message>Ignoring title for <xsl:value-of select="local-name($object)"/>.</xsl:message>
                </xsl:if>
            </xsl:when>
            <xsl:when test="$string=''">
                <xsl:call-template name="gentext.element.name"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="normalize-space($string)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:param name="title-toc">
        <xsl:choose>
            <xsl:when test='$level = 2'>
                <xsl:choose>
                    <xsl:when test='$bqr'>
                        <xsl:apply-templates select="./bqr_objet" mode="toc"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="./title" mode="toc"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>

            <xsl:otherwise>
                <xsl:apply-templates select="./title" mode="toc"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:param>

    <xsl:param name="section-tag">
        <xsl:choose>
            <xsl:when test='($level = 1) and ($bqr)'>
                <xsl:text>bqrsection</xsl:text>
            </xsl:when>
            <xsl:when test='($level = 1) and not($bqr)'>
                <xsl:text>section</xsl:text>
            </xsl:when>
            <xsl:when test='$level = 2'>
                <xsl:text>subsection</xsl:text>
            </xsl:when>
            <xsl:when test='$level = 3'>
                <xsl:text>subsubsection</xsl:text>
            </xsl:when>
            <xsl:when test='$level = 4'>
                <xsl:text>paragraph</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>subparagraph</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    <xsl:param name="begin-param-tag">
        <xsl:choose>
            <xsl:when test='($level = 1) and ($bqr)'>
                <xsl:text>{</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>[</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:param>
    <xsl:param name="end-param-tag">
        <xsl:choose>
            <xsl:when test='($level = 1) and ($bqr)'>
                <xsl:text>}</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>]</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:param>

    <xsl:if test="not(node()[position()=2])">
        <xsl:text>\pagebreak[2]&#10;</xsl:text>
    </xsl:if>

    <xsl:text>&#10;% ------------------------   &#10;</xsl:text>
    <xsl:text>% begin section (level: </xsl:text>
    <xsl:value-of select="$level"/>
    <xsl:text>)&#10;\</xsl:text>
    <xsl:value-of select="$section-tag"/>
    <xsl:if test="$bridgehead"><xsl:text>*</xsl:text></xsl:if>
    <xsl:value-of select="$begin-param-tag"/>
    <xsl:value-of select="$title-toc"/>
    <xsl:value-of select="$end-param-tag"/>
    <xsl:text>{</xsl:text>
    <xsl:value-of select="$title"/>
    <xsl:text>}&#10;\label{</xsl:text>
    <xsl:value-of select="$id"/>
    <xsl:text>}\hypertarget{</xsl:text>
    <xsl:value-of select="$id"/>
    <xsl:text>}{}%&#10;</xsl:text>
    <xsl:if test="$bqr">
        <xsl:if test="$level = 1">
            <xsl:message>begin{multicols}</xsl:message>
            <xsl:text>\begin{multicols}{2}&#10;</xsl:text>
        </xsl:if>
    </xsl:if>
    
    <xsl:call-template name="content-templates"/>

    <xsl:if test="$bqr">
        <xsl:if test="$level = 1">
            <xsl:message>end{multicols}</xsl:message>
            <xsl:text>\end{multicols}&#10;</xsl:text>
        </xsl:if>
    </xsl:if>
    <xsl:text>% end section (level: </xsl:text>
    <xsl:value-of select="$level"/>
    <xsl:text>)&#10;</xsl:text>
    <xsl:text>% ------------------------   &#10;</xsl:text>
</xsl:template>



<xsl:template match="part">
    <xsl:param name="bridgehead" select="ancestor::preface|ancestor::colophon|ancestor::dedication"/>
    <xsl:param name="string">
        <xsl:call-template name="extract.object.title">
            <xsl:with-param name="object" select="."/>
        </xsl:call-template>
    </xsl:param>
    <xsl:param name="use.label"	select="1"/>
    <xsl:param name="use.hypertarget" 	select="1"/>
    <xsl:variable name="id">
        <xsl:call-template name="generate.label.id">
            <xsl:with-param name="object" select="."/>
        </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="title">
        <xsl:choose>
            <xsl:when test="starts-with(local-name(.),'informal')">
                <xsl:if test="$string!=''">
                    <xsl:message>Ignoring title for <xsl:value-of select="local-name($object)"/>.</xsl:message>
                </xsl:if>
            </xsl:when>
            <xsl:when test="$string=''">
                <xsl:call-template name="gentext.element.name"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="normalize-space($string)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:param name="subtitle">
        <xsl:call-template name="extract.object.subtitle">
            <xsl:with-param name="object" select="."/>
        </xsl:call-template>
    </xsl:param>
    <xsl:param name="title-toc">
        <xsl:apply-templates select="./title" mode="toc"/>
    </xsl:param>
    <xsl:param name="subtitle-toc">
        <xsl:apply-templates select="./subtitle" mode="toc"/>
    </xsl:param>
    
    <xsl:if test="starts-with(translate($title-toc, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 'AVANT-PROJET DE ')">
        <xsl:text>\sfont&#10;</xsl:text>
    </xsl:if>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>%\balance&#10;</xsl:text>
    <xsl:text>% -------------------------------------------------------------   &#10;</xsl:text>
    <xsl:text>% PART:  </xsl:text>
    <xsl:value-of select="$title-toc"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>%&#10;</xsl:text>
    <xsl:text>% -------------------------------------------------------------   &#10;</xsl:text>

    <xsl:text>\setcounter{section}{0}&#10;</xsl:text>
    <xsl:choose>
        <xsl:when test='$bqr'>
            <xsl:text>\onecolumn</xsl:text>
            <xsl:text>\bqrpart{</xsl:text>
            <xsl:value-of select="$title-toc"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="$title"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="$subtitle-toc"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="$subtitle"/>
            <xsl:text>}&#10;</xsl:text>
            <xsl:text>\label{</xsl:text>
            <xsl:value-of select="$id"/>
            <xsl:text>}\hypertarget{</xsl:text>
            <xsl:value-of select="$id"/>
            <xsl:text>}{}%&#10;</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:text>\twocolumn[\pcfpart{</xsl:text>
            <xsl:value-of select="$title-toc"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="$title"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="$subtitle-toc"/>
            <xsl:text>}{</xsl:text>
            <xsl:value-of select="$subtitle"/>
            <xsl:text>}&#10;</xsl:text>
            <xsl:text>\label{</xsl:text>
            <xsl:value-of select="$id"/>
            <xsl:text>}\hypertarget{</xsl:text>
            <xsl:value-of select="$id"/>
            <xsl:text>}{}%&#10;]</xsl:text>
        </xsl:otherwise>
    </xsl:choose>

    <xsl:call-template name="content-templates"/>
    <xsl:text>% end part &#10;</xsl:text>
    <xsl:text>% ------------------------   &#10;</xsl:text>
    <xsl:if test="starts-with(translate($title-toc, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 'AVANT-PROJET DE ')">
        <xsl:text>\nfont&#10;</xsl:text>
    </xsl:if>
</xsl:template>


</xsl:stylesheet>
