Application - Legi2Pdf
======================

INTRODUCTION
------------
L'objectif  de ce filtre est de produire un PDF qualit� "imprimeur" � partir
d'un fichier LEGI (XML bas� sur DocBook). 
Ce filtre permet d'�viter le co�t financier et un d�lai important pour un mise 
en page "manuelle" d'un document (pr�c�dement r�alis� par l'imprimeur).

=================================================================================

OUTIL
-----
Pour utiliser ce filtre il est n�cessaire d'avoir:
 o python 2.2 ou post�rieur
 o libxslt / xsltproc: librairies/application pour XML (voir http://xmlsoft.org/)
 o tetex: g�n�rateur de document � partir de TEX (voir http://www.tug.org/teTeX/)
 o db2latex: ensemble de sp�cification XSL pour transformer du DocBook en XSL
   (voir http://db2latex.sourceforge.net/)
   Remarque: Legi2Pdf inclue db2latex
 o Ghostscript: permet d'effectuer la converssion entre du PS et du PDF
 o libwmf/wmf2eps: permet d'effectuer la converssion entre du WMF et du PS
   (voir: http://wvware.sourceforge.net/libwmf.html)
   Pour utiliser xmf2eps sous cygwin, il est n�cessaire d'avoir les modules 
   gd, libgd2 et libgd-devel.


=================================================================================

TEX/LATEX packages
------------------
Ils est n�cessaire d'installer les modules tex/latex suivants:
o unicode
o truncate
o eurosym
o relsize
o eurosym
o footnote
o flushend (n'est plus utilis�)
o ucs

Comment installer un nouveau package TEX/LATEX ?
Pour trouver les packages: 
  http://www.ctan.org

Les packages non-standard � la distribution doivent (pour bien faire) �tre plac�s
dans le r�pertoire: 
  /usr/local/share/texmf/tex/latex 
au lieu de 
  /usr/share/texmf/tex/latex

Il suffit de copier les fichiers et d'ex�cuter la commande: 'mktexlsr' ou 'texhash'

=================================================================================

FONTS
-----

G�n�ralit�s � propos des Fonts

Remarques:
o <tex-root> == r�pertoire d'installation de texmf
  e.g. /usr/share/texmf

o Les noms des fichiers qui sont utilis�s dans la proc�dure correspondent 
  � la Font Sabon, il est relativement ais� d'extrapoler pour un autre Font 
  comme Univers (ex. psbb8a.afm -> punb8a.afm).

Fichiers de Font
Structure d'un nom: 
  p sb b [i] 8a
  |  | r  |  + encoding
  |  | |  + italic
  |  | + bold or roman
  |  +- sabon 
  +- Adobe

 ex. psbb8a.afm    (Adobe, Sabon, Bold, 8a encoding)
     punr8a.afm    (Adobe, Univers, Roman, 8a encoding)

Types de fichier:

*.afm: Adobe Font Metrics 
  dimensions des caract�res, ligatures, ...
  �quivalent aux fichiers *.tfm pour les fonts de type METAFONT.

*.pfa: Printer Font Ascii
*.pfb: Printer Font Binary 
  instructions de dessin des caract�res.
  (souvent payant)

*.vf: Virtual Font

Etapes pour l'installation des Fonts:

1) Copier les fichier config.psb et psb.map dans <tex-root>/dvips/psnfss
2) Cr�er le r�pertoire <tex-root>/fonts/tfm/adobe/sabon
  et copier tous les fichiers avec l'extension *.tfm
3) Cr�er le r�pertoire <tex-root>/fonts/vf/adobe/sabon
  et copier tous les fichiers avec l'extension *.vf
4) Cr�er le r�pertoire <tex-root>/tex/latex/sabon/tex
  et copier tous les fichiers avec l'extension *.fd et le
  fichier sabon.sty
5) Cr�er le r�pertoire <tex-root>/fonts/afm/adobe/sabon
  et copier les fichiers: 
     sab____.afm, sabi____.afm, sai____.afm et sar____.afm dans le r�pertoire.
  Renomer les fichiers:
     sab____.afm    -> psbb8a.afm
     sabi____.afm   -> psbbi8a.afm
     sai____.afm    -> psbri8a.afm
     sar____.afm    -> psbr8a.afm
6) Cr�er le r�pertoire <tex-root>/fonts/type1/adobe/sabon
  et copier les fichiers: 
     sab____.pfb, sabi____.pfb, sai____.pfb et sar____.pfb dans le r�pertoire.
  Renomer les fichiers:
     sab____.pfb    -> psbb8a.pfb
     sabi____.pfb   -> psbbi8a.pfb
     sai____.pfb    -> psbri8a.pfb
     sar____.pfb    -> psbr8a.pfb

7) Il faut modifier le fichier de configuration: updmap.cfg qui se trouve 
   dans <tex-root>/web2c
   MAIS pour une distribution Linux/Debian il ne faut pas directement modifier
   ce fichier mais plutot le fichier: 
     /etc/texmf/updmap.d/00updmap.cfg
   Ajouter � fin du fichier: 

     # Sabon
     Map psb.map

     # Univers
     Map pun.map

   Il faut ensuite ex�cuter la commande 'updmap' et la commande 'mktexlsr'


8) Il faut modifier le fichier de configuration :pdftex.cfg
    Ajouter la ligne "pdf_minorversion 5" pour prendre en charge le pdf1.5

=================================================================================

BABEL
-----

Comment configurer votre syst�me pour 'supporter' le fran�ais (c�sure) ?

1) Pour configurer les motifs de c�sure pour votre langue, �diter le fichier 
   /etc/texmf/language.dat 
    V�rifier que le fran�ais est actif (pas mis en commentaire)

    % French, TWO lines!
    french          frhyph.tex
    =patois

2) Editer le fichier /etc/texmf/fmt.d/00tetex.cnf
   pour que pdftex et pdflatex supporte babel: 
   remplacer :
     '-' -> language.dat

    pdftex          pdftex          language.dat    pdftex.ini
    pdflatex        pdftex          language.dat    pdflatex.ini

3) V�rifier que 'frhyph.tex' existe bien; normalement ce fichier se 
   trouve dans: <tex-root>/tex/generic/hyphen/frhyph.tex

4) puis lancer : 
   > texconfig init ; texconfig hyphen

5) pour faire un test: 

test.tex= 
d�but >>>
\documentclass{article}
\usepackage[latin1]{inputenc}
\usepackage[cyr]{aeguill}
\usepackage[francais]{babel}
\begin{document}
\showhyphens{signal container \'ev\'enement alg\`ebre}
\showhyphens{signal container �v�nement alg�bre}
\end{document}
<<< fin

> pdflatex test.tex
Si vous obtenez:
[] /aer10/si-gnal contai-ner ^^e9v^^e9-ne-ment al-g^^e8bre 
          ^^^^^^^

==> OK

Par contre si vous obtenez:
[] /aer10/sig-nal con-tainer ^^e9v^^e9ne-ment al-g^^e8bre
          ^^^^^^^
==> PROBLEME !!! toujours en anglais !           


