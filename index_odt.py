#! /usr/bin/env python

import sys
import os
import zipfile
from optparse import OptionParser
from cStringIO import StringIO

try:
    import elementtree.ElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET


def main():
    parser = OptionParser()
    parser.add_option('-o', '--output', dest = 'output', metavar = 'FILENAME')
    options, args = parser.parse_args()

    if len(args) != 1:
        print parser.get_usage()
        sys.exit(1)

    index = ET.Element('index')

    for filename in os.listdir(args[0]):
        if not filename.endswith('.odt'):
            continue
        z = zipfile.ZipFile(os.path.join(args[0], filename))
        for zfile in z.namelist():
            if zfile == 'meta.xml':
                metadata = z.read(zfile)
                break
        else:
            continue
        metadata_tree = ET.ElementTree(ET.fromstring(metadata))
        title = metadata_tree.find('//{http://purl.org/dc/elements/1.1/}title')
        if title is None:
            title = filename
        else:
            title = title.text

        document = ET.SubElement(index, 'document')
        document.attrib['filename'] = filename
        document.text = title

    out = StringIO()
    ET.ElementTree(index).write(out)
    if options.output:
        file(options.output, 'w').write(out.getvalue())
    else:
        print out.getvalue()

if __name__ == '__main__':
    main()

