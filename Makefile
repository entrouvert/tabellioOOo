UNOPKG=/usr/lib/libreoffice/program/unopkg
#UNOPKG=/var/tmp/Apache_OpenOffice_incubating_3.4.1_Linux_x86-64_install-arc_en-US/openoffice.org3/program/unopkg

tabellio-pcf.ott:
	(cd template-pcf && zip ../tabellio-pcf.ott -r . -x '.svn')

tabellio-pfb.ott:
	(cd template-pfb && zip ../tabellio-pfb.ott -r . -x '.svn')

addon/Addons.xcu: Addons.xcu.in styles.py
	python styles.py < Addons.xcu.in > addon/Addons.xcu

tabellio.oxt: addon/Addons.xcu
	(cd addon && fastjar -c -f ../tabellio.oxt -M .)

install: tabellio.oxt
	$(UNOPKG) add -v tabellio.oxt
	
uninstall:
	$(UNOPKG) remove tabellio.oxt

.PHONY: tabellio-pcf.ott tabellio.oxt tabellio-pfb.ott
