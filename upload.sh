#! /bin/sh

make tabellio-pcf.ott
make tabellio-pfb.ott
make tabellio.oxt

VERSION=$(grep "version value" addon/description.xml  | cut -d'"' -f2)

cat << _EOF_ > update.xml
<?xml version="1.0" encoding="UTF-8"?>
<description xmlns="http://openoffice.org/extensions/update/2006" 
             xmlns:xlink="http://www.w3.org/1999/xlink">
  <identifier value="org.tabellio.openoffice"/>
  <version value="$VERSION" />
    <update-download>
      <src xlink:href="http://tabellio.entrouvert.com/tabellio.oxt" />
    </update-download>
</description>
_EOF_

scp update.xml tabellio-pcf.ott tabellio-pfb.ott tabellio.oxt leucas.entrouvert.org:/var/vhosts/tabellio.entrouvert.com/web/

