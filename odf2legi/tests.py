#! /usr/bin/env python

from cStringIO import StringIO
import unittest
try:
    import xml.etree.ElementTree as ET
except ImportError:
    import elementtree.ElementTree as ET

import odf2legi
odf2legi.debug = True

class FillInlineTestCase(unittest.TestCase):
    def convert_to_legi(self, doc):
        elem = ET.fromstring(doc)
        para = ET.Element('para')
        odf2legi.fill_inline(para, elem)
        out = StringIO()
        ET.ElementTree(para).write(out)
        return out.getvalue()

    def test_inline(self):
        doc = '''\
<text:p xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
Plop plop plop
</text:p>'''
        self.assertEqual(self.convert_to_legi(doc), '''<para>Plop plop plop</para>''')

    def test_inline_with_soft_page_break(self):
        doc = '''\
<text:p xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
Plop plop <text:soft-page-break/>plop
</text:p>'''
        self.assertEqual(self.convert_to_legi(doc), '''<para>Plop plop plop\n</para>''')

    def test_inline_with_unknown_style(self):
        odf2legi.STYLES = {}
        doc = '''\
<text:p xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
Plop <text:span text:style-name="T1">plop</text:span> plop
</text:p>'''
        self.assertEqual(self.convert_to_legi(doc), '''<para>Plop plop plop\n</para>''')

    def test_inline_with_known_style(self):
        odf2legi.STYLES = {'T1': {'props': ['bold']}}
        doc = '''\
<text:p xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
Plop <text:span text:style-name="T1">plop</text:span> plop
</text:p>'''
        self.assertEqual(self.convert_to_legi(doc),
                '''<para>Plop <emphasis role="bold">plop</emphasis> plop\n</para>''')

    def test_inline_with_combined_styles(self):
        odf2legi.STYLES = {'T1': {'props': ['bold', 'italic']}}
        doc = '''\
<text:p xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
Plop <text:span text:style-name="T1">plop</text:span> plop
</text:p>'''
        self.assertEqual(self.convert_to_legi(doc),
                '''<para>Plop <emphasis role="bold"><emphasis role="italic">plop</emphasis></emphasis> plop
</para>''')

    def test_inline_with_known_style_and_soft_page_break(self):
        odf2legi.STYLES = {'T1': {'props': ['bold']}}
        doc = '''\
<text:p xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
Plop <text:span text:style-name="T1">plop</text:span> <text:soft-page-break/>plop
</text:p>'''
        self.assertEqual(self.convert_to_legi(doc),
                '''<para>Plop <emphasis role="bold">plop</emphasis> plop\n</para>''')

    def test_inline_with_emphasis_and_trailing_space(self):
        odf2legi.STYLES = {'T1': {'props': ['bold']}}
        doc = '''\
<text:p xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
Plop <text:span text:style-name="T1">plop </text:span>plop
</text:p>'''
        self.assertEqual(self.convert_to_legi(doc),
                '''<para>Plop <emphasis role="bold">plop</emphasis> plop\n</para>''')

    def test_inline_with_known_style_and_line_break(self):
        odf2legi.STYLES = {'T1': {'props': ['bold']}}
        doc = '''\
<text:p xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
Plop <text:span text:style-name="T1">plop</text:span> <text:line-break/>plop
</text:p>'''
        self.assertEqual(self.convert_to_legi(doc),
                '''<para>Plop <emphasis role="bold">plop</emphasis> <?line-break?>plop\n</para>''')

    def test_inline_with_known_styles_and_line_break(self):
        odf2legi.STYLES = {'T1': {'props': ['bold']}}
        doc = '''\
<text:p xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
Plop <text:span text:style-name="T1">plop</text:span> <text:line-break/>plop
plop <text:span text:style-name="T1">plop</text:span>
</text:p>'''
        self.assertEqual(self.convert_to_legi(doc),
                '''<para>Plop <emphasis role="bold">plop</emphasis> <?line-break?>plop\nplop <emphasis role="bold">plop</emphasis>\n</para>''')

    def test_inline_with_annotation(self):
        odf2legi.STYLES = {'T1': {'props': ['bold']}}
        doc = '''\
<text:p xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:annotation>
  <text:p>plop: 1</text:p>
</office:annotation>
Plop
</text:p>'''
        self.assertEqual(self.convert_to_legi(doc),
                '''<para>\nPlop\n</para>''')


    def test_footnotes_bug4254(self):
        doc = '''\
<text:p xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">Foo
<text:span><text:note><text:note-citation>2</text:note-citation
><text:note-body><text:p><text:span ><text:s/></text:span><text:span
>Bar</text:span></text:p></text:note-body></text:note></text:span
>. Baz</text:p>
'''

        self.assertEqual(self.convert_to_legi(doc),
                '''<para>Foo<footnote><para>Bar</para></footnote>. Baz</para>''')

    def test_footnotes_bug4254bis(self):
        doc = '''\
<text:p xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">Foo
<text:span><text:note><text:note-citation>2</text:note-citation
><text:note-body><text:p><text:span ></text:span><text:span
>Bar</text:span></text:p></text:note-body></text:note></text:span
><text:span>. Baz<text:s/></text:span></text:p>
'''

        self.assertEqual(self.convert_to_legi(doc),
                '''<para>Foo<footnote><para>Bar</para></footnote>. Baz</para>''')

    def test_inline_with_link(self):
        odf2legi.STYLES = {}
        doc = '''\
<text:p xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
Plop <text:a>foobar@example.net</text:a> plop
</text:p>'''
        self.assertEqual(self.convert_to_legi(doc), '''<para>Plop foobar@example.net plop\n</para>''')

    def test_inline_many_spans(self):
        doc = '''\
<text:p xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<text:span>Plop</text:span> <text:span>plop</text:span> <text:span>plop</text:span>\
</text:p>'''
        self.assertEqual(self.convert_to_legi(doc), '''<para>Plop plop plop</para>''')

    def test_inline_many_spans_s(self):
        # appears in https://dev.entrouvert.org/issues/5978
        doc = '''\
<text:p xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<text:span>Plop</text:span> <text:span>plop <text:s/>plop</text:span> <text:span>plop</text:span>\
</text:p>'''
        self.assertEqual(self.convert_to_legi(doc), '''<para>Plop plop plop plop</para>''')

    def test_inline_many_span_and_style_and_s(self):
        odf2legi.STYLES = {'T1': {'props': ['bold']}}
        doc = '''\
<text:p xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<text:span>Plop</text:span> <text:span text:style-name="T1">plop</text:span> \
<text:span>plop <text:s/>plop</text:span>
</text:p>'''
        self.assertEqual(self.convert_to_legi(doc),
                '''<para>Plop <emphasis role="bold">plop</emphasis> plop plop\n</para>''')




class TableTestCase(unittest.TestCase):
    def test_plain_table(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<table:table>
  <table:table-column/>
  <table:table-column/>
  <table:table-row>
    <table:table-cell><text:p>1st Col</text:p></table:table-cell>
    <table:table-cell><text:p>2nd Col</text:p></table:table-cell>
  </table:table-row>
</table:table>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><table><tgroup cols="2"><colspec colname="col1" colnum="1" colwidth="250pt" /><colspec colname="col2" colnum="2" colwidth="250pt" /><tbody><row><entry><para>1st Col</para></entry><entry><para>2nd Col</para></entry></row></tbody></tgroup></table></sect1></book>''')

    def test_plain_table_number_columns_repeated(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<table:table>
  <table:table-column table:number-columns-repeated="2"/>
  <table:table-row>
    <table:table-cell><text:p>1st Col</text:p></table:table-cell>
    <table:table-cell><text:p>2nd Col</text:p></table:table-cell>
  </table:table-row>
</table:table>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><table><tgroup cols="2"><colspec colname="col1" colnum="1" colwidth="250pt" /><colspec colname="col2" colnum="2" colwidth="250pt" /><tbody><row><entry><para>1st Col</para></entry><entry><para>2nd Col</para></entry></row></tbody></tgroup></table></sect1></book>''')

    def test_plain_table_with_colwidth(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:automatic-styles>
  <style:style style:name="Table1.A" style:family="table-column">
    <style:table-column-properties style:column-width="4cm"/>
  </style:style>
</office:automatic-styles>
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<table:table>
  <table:table-column table:number-columns-repeated="2" table:style-name="Table1.A"/>
  <table:table-row>
    <table:table-cell><text:p>1st Col</text:p></table:table-cell>
    <table:table-cell><text:p>2nd Col</text:p></table:table-cell>
  </table:table-row>
</table:table>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><table><tgroup cols="2"><colspec colname="col1" colnum="1" colwidth="113.8pt" /><colspec colname="col2" colnum="2" colwidth="113.8pt" /><tbody><row><entry><para>1st Col</para></entry><entry><para>2nd Col</para></entry></row></tbody></tgroup></table></sect1></book>''')

    def test_plain_table_with_alignment_right(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:automatic-styles>
  <style:style style:name="P1" style:family="paragraph">
    <style:paragraph-properties fo:text-align="end"/>
  </style:style>
</office:automatic-styles>
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<table:table>
  <table:table-column table:number-columns-repeated="2"/>
  <table:table-row>
    <table:table-cell><text:p>1st Col</text:p></table:table-cell>
    <table:table-cell><text:p text:style-name="P1">2nd Col</text:p></table:table-cell>
  </table:table-row>
</table:table>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><table><tgroup cols="2"><colspec colname="col1" colnum="1" colwidth="250pt" /><colspec align="right" colname="col2" colnum="2" colwidth="250pt" /><tbody><row><entry><para>1st Col</para></entry><entry role="right"><para role="right">2nd Col</para></entry></row></tbody></tgroup></table></sect1></book>''')

    def test_plain_table_with_alignment_center(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:automatic-styles>
  <style:style style:name="P1" style:family="paragraph">
    <style:paragraph-properties fo:text-align="center"/>
  </style:style>
</office:automatic-styles>
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<table:table>
  <table:table-column table:number-columns-repeated="2"/>
  <table:table-row>
    <table:table-cell><text:p>1st Col</text:p></table:table-cell>
    <table:table-cell><text:p text:style-name="P1">2nd Col</text:p></table:table-cell>
  </table:table-row>
</table:table>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><table><tgroup cols="2"><colspec colname="col1" colnum="1" colwidth="250pt" /><colspec align="center" colname="col2" colnum="2" colwidth="250pt" /><tbody><row><entry><para>1st Col</para></entry><entry role="center"><para role="center">2nd Col</para></entry></row></tbody></tgroup></table></sect1></book>''')

    def legend(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<table:table>
  <table:table-column/>
  <table:table-column/>
  <table:table-row>
    <table:table-cell><text:p>1st Col</text:p></table:table-cell>
    <table:table-cell><text:p>2nd Col</text:p></table:table-cell>
  </table:table-row>
</table:table>
<text:p text:style-name="Table">
  Tableau <text:sequence text:ref-name="refTable0" text:name="Table" text:formula="ooow:Table+1"
  style:num-format="1">1</text:sequence>: Test
</text:p>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><table><title id="refTable0">Test</title><tgroup cols="2"><colspec colnum="1" colwidth="250pt" /><colspec colnum="2" colwidth="250pt" /><tbody><row><entry><para>1st Col</para></entry><entry><para>2nd Col</para></entry></row></tbody></tgroup></table></sect1></book>''')

    def crossreference(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<table:table>
  <table:table-column/>
  <table:table-column/>
  <table:table-row>
    <table:table-cell><text:p>1st Col</text:p></table:table-cell>
    <table:table-cell><text:p>2nd Col</text:p></table:table-cell>
  </table:table-row>
</table:table>
<text:p text:style-name="Table">
  Tableau <text:sequence text:ref-name="refTable0" text:name="Table" text:formula="ooow:Table+1"
  style:num-format="1">1</text:sequence>: Test
</text:p>
<text:p>Cf le tableau <text:sequence-ref text:reference-format="text" text:ref-name="refTable0">Tableau 1: Test</text:sequence-ref>.</text:p>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><table><title id="refTable0">Test</title><tgroup cols="2"><colspec colnum="1" colwidth="250pt" /><colspec colnum="2" colwidth="250pt" /><tbody><row><entry><para>1st Col</para></entry><entry><para>2nd Col</para></entry></row></tbody></tgroup></table><para>Cf le tableau <xref linkend="refTable0" />.</para></sect1></book>''')


class ListTestCase(unittest.TestCase):
    def test_numeric_list(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:automatic-styles>
    <text:list-style style:name="L1">
      <text:list-level-style-number text:level="1" text:style-name="Numbering_20_Symbols" style:num-suffix="." style:num-format="1">
        <style:list-level-properties text:list-level-position-and-space-mode="label-alignment">
          <style:list-level-label-alignment text:label-followed-by="listtab" text:list-tab-stop-position="1.27cm" fo:text-indent="-0.635cm" fo:margin-left="1.27cm"/>
        </style:list-level-properties>
      </text:list-level-style-number>
    </text:list-style>
</office:automatic-styles>
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<text:list xml:id="list526489326" text:style-name="L1">
  <text:list-item>
    <text:p>Premiere liste</text:p>
  </text:list-item>
  <text:list-item>
    <text:p>Tres simple</text:p>
  </text:list-item>
</text:list>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><para><orderedlist continuation="restarts" numeration="arabic"><listitem><para>Premiere liste</para></listitem><listitem><para>Tres simple</para></listitem></orderedlist></para></sect1></book>''')

    def test_alpha_list(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:automatic-styles>
    <text:list-style style:name="L1">
      <text:list-level-style-number text:level="1" text:style-name="Numbering_20_Symbols" style:num-suffix="." style:num-format="a">
        <style:list-level-properties text:list-level-position-and-space-mode="label-alignment">
          <style:list-level-label-alignment text:label-followed-by="listtab" text:list-tab-stop-position="1.27cm" fo:text-indent="-0.635cm" fo:margin-left="1.27cm"/>
        </style:list-level-properties>
      </text:list-level-style-number>
    </text:list-style>
</office:automatic-styles>
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<text:list xml:id="list526489326" text:style-name="L1">
  <text:list-item>
    <text:p>Premiere liste</text:p>
  </text:list-item>
  <text:list-item>
    <text:p>Tres simple</text:p>
  </text:list-item>
</text:list>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><para><orderedlist continuation="restarts" numeration="loweralpha"><listitem><para>Premiere liste</para></listitem><listitem><para>Tres simple</para></listitem></orderedlist></para></sect1></book>''')

    def test_alpha_then_numeric_list(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:automatic-styles>
    <text:list-style style:name="L1">
      <text:list-level-style-number text:level="1" text:style-name="Numbering_20_Symbols" style:num-suffix="." style:num-format="a"/>
      <text:list-level-style-number text:level="2" text:style-name="Numbering_20_Symbols" style:num-suffix="." style:num-format="1"/>
    </text:list-style>
</office:automatic-styles>
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<text:list xml:id="list526489326" text:style-name="L1">
  <text:list-item>
    <text:p>Premiere liste</text:p>
    <text:list>
      <text:list-item>
        <text:p>Second niveau</text:p>
      </text:list-item>
    </text:list>
  </text:list-item>
  <text:list-item>
    <text:p>Tres simple</text:p>
  </text:list-item>
</text:list>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><para><orderedlist continuation="restarts" numeration="loweralpha"><listitem><para>Premiere liste</para><para><orderedlist continuation="restarts" numeration="arabic"><listitem><para>Second niveau</para></listitem></orderedlist></para></listitem><listitem><para>Tres simple</para></listitem></orderedlist></para></sect1></book>''')

    def test_list_with_footnote(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:automatic-styles>
    <text:list-style style:name="L1">
      <text:list-level-style-number text:level="1" text:style-name="Numbering_20_Symbols" style:num-suffix="." style:num-format="1">
        <style:list-level-properties text:list-level-position-and-space-mode="label-alignment">
          <style:list-level-label-alignment text:label-followed-by="listtab" text:list-tab-stop-position="1.27cm" fo:text-indent="-0.635cm" fo:margin-left="1.27cm"/>
        </style:list-level-properties>
      </text:list-level-style-number>
    </text:list-style>
</office:automatic-styles>
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<text:list xml:id="list526489326" text:style-name="L1">
  <text:list-item>
    <text:p>Premiere<text:span><text:note><text:note-citation>2</text:note-citation
><text:note-body><text:p><text:span ><text:s/></text:span><text:span
>Bar</text:span></text:p></text:note-body></text:note></text:span
>. liste</text:p>
  </text:list-item>
</text:list>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><para><orderedlist continuation="restarts" numeration="arabic"><listitem><para>Premiere<footnote><para>Bar</para></footnote>. liste</para></listitem></orderedlist></para></sect1></book>''')


class FigureTestCase(unittest.TestCase):
    def test_figure(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<text:p>
<draw:frame>
<draw:image xlink:href="image.jpg"/>
</draw:frame>
</text:p>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><para><mediaobject><imageobject><imagedata fileref="image.jpg" format="JPG" /></imageobject></mediaobject></para></sect1></book>''')

    def test_figure_dimensions(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<text:p>
<draw:frame svg:width="12cm" svg:height="6cm">
<draw:image xlink:href="image.jpg"/>
</draw:frame>
</text:p>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><para><mediaobject><imageobject><imagedata depth="6cm" fileref="image.jpg" format="JPG" width="12cm" /></imageobject></mediaobject></para></sect1></book>''')

    def test_figure_with_caption(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<text:p>
<draw:frame>
<draw:text-box>
<text:p>
<draw:frame>
<draw:image xlink:href="image.jpg"/>
</draw:frame>
Illustration <text:sequence text:ref-name="refIllustration0" text:name="Illustration" text:formula="ooow:Illustration+1" style:num-format="1">1</text:sequence>: Image
</text:p>
</draw:text-box>
</draw:frame>
</text:p>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><para><mediaobject><imageobject><imagedata fileref="image.jpg" format="JPG" /></imageobject><caption id="refIllustration0"><para>Image</para></caption></mediaobject></para></sect1></book>''')


class HierarchyTestCase(unittest.TestCase):
   def test_part(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_1">Part</text:h>
<text:p>Plop plop plop</text:p>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><part><title>Part</title><para>Plop plop plop</para></part></book>''')

   def test_chapter(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_2">Chapter</text:h>
<text:p>Plop plop plop</text:p>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><chapter><title>Chapter</title><para>Plop plop plop</para></chapter></book>''')


   def test_sect1(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Sect 1</text:h>
<text:p>Plop plop plop</text:p>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Sect 1</title><para>Plop plop plop</para></sect1></book>''')

   def test_part2sec(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_1">Part</text:h>
<text:h text:style-name="Heading_20_2">Chapter</text:h>
<text:h text:style-name="Heading_20_3">Sect 1</text:h>
<text:p>Plop plop plop</text:p>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><part><title>Part</title><chapter><title>Chapter</title><sect1><title>Sect 1</title><para>Plop plop plop</para></sect1></chapter></part></book>''')




class MiscTestCase(unittest.TestCase):
    def test_empty_paragraphs(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<text:p>Plop plop plop</text:p>
<text:p></text:p>
<text:p></text:p>
<text:p>Plop plop plop</text:p>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><para>Plop plop plop</para><para>Plop plop plop</para></sect1></book>''')

    def test_blank_paragraphs(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<text:p>Plop plop plop</text:p>
<text:p> </text:p>
<text:p> </text:p>
<text:p>Plop plop plop</text:p>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><para>Plop plop plop</para><para>Plop plop plop</para></sect1></book>''')


    def test_paragraph_with_annotation(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<text:p>
<office:annotation>
  <text:p>type: TABELLIO</text:p>
  <text:p>ref id: 000000000</text:p>
  <text:p>classname: PARL</text:p>
  <text:p>aspres: 1</text:p>
</office:annotation>
Mme Michu
</text:p>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc).replace('\n', ''),
                '<book><sect1><title>Chapter</title><speech><ref type="TABELLIO"><param name="ref id">000000000</param><param name="classname">PARL</param><param name="aspres">1</param></ref><para>Mme Michu</para></speech></sect1></book>')

    def test_paragraph_with_multiple_annotations(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<text:p>
<office:annotation>
  <text:p>type: TABELLIO</text:p>
  <text:p>ref id: 000000000</text:p>
  <text:p>classname: PARL</text:p>
  <text:p>aspres: 1</text:p>
</office:annotation>
Mme Michu bla bla bla
</text:p>
<text:p>
Bla bla bla
</text:p>
<text:p>
<office:annotation>
  <text:p>type: TABELLIO</text:p>
  <text:p>ref id: 000000000</text:p>
  <text:p>classname: PARL</text:p>
  <text:p>aspres: 1</text:p>
</office:annotation>
M. Michu bla bla bla
</text:p>

</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc).replace('\n', ''),
                '<book><sect1><title>Chapter</title><speech><ref type="TABELLIO"><param name="ref id">000000000</param><param name="classname">PARL</param><param name="aspres">1</param></ref><para>Mme Michu bla bla bla</para><para>Bla bla bla</para></speech><speech><ref type="TABELLIO"><param name="ref id">000000000</param><param name="classname">PARL</param><param name="aspres">1</param></ref><para>M. Michu bla bla bla</para></speech></sect1></book>')


    def test_paragraph_with_annotation_in_span(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<text:p>
<text:span>
<office:annotation>
  <text:p>type: TABELLIO</text:p>
  <text:p>ref id: 000000000</text:p>
  <text:p>classname: PARL</text:p>
  <text:p>aspres: 1</text:p>
</office:annotation>
</text:span><text:span>Mme Michu</text:span>
</text:p>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc).replace('\n', ''),
                '<book><sect1><title>Chapter</title><speech><ref type="TABELLIO"><param name="ref id">000000000</param><param name="classname">PARL</param><param name="aspres">1</param></ref><para>Mme Michu</para></speech></sect1></book>')

    def test_title_with_line_break(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Invitation pour<text:line-break/>les gens</text:h>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Invitation pour<?line-break?>les gens</title></sect1></book>''')

    def test_note_paragraph(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
<office:body>
<office:text>
<text:h text:style-name="Heading_20_3">Chapter</text:h>
<text:p text:style-name="Note">Plop plop plop</text:p>
</office:text>
</office:body>
</office:document-content>
'''
        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><note><para>Plop plop plop</para></note></sect1></book>''')

    def test_page_break(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
  <office:automatic-styles>
    <style:style style:name="P1" style:family="paragraph" style:parent-style-name="Text_20_body">
      <style:paragraph-properties fo:break-before="page"/>
    </style:style>
  </office:automatic-styles>
  <office:body>
    <office:text>
    <text:h text:style-name="Heading_20_3">Chapter</text:h>
    <text:p text:style-name="Text_20_body">Hello</text:p>
    <text:p text:style-name="P1">World</text:p>
    </office:text>
  </office:body>
</office:document-content>
'''

        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><para>Hello</para><?page-break?><para>World</para></sect1></book>''')

    def test_centered_paragraph(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
  <office:automatic-styles>
    <style:style style:name="P1" style:family="paragraph" style:parent-style-name="Text_20_body">
      <style:paragraph-properties fo:text-align="center"/>
    </style:style>
  </office:automatic-styles>
  <office:body>
    <office:text>
    <text:h text:style-name="Heading_20_3">Chapter</text:h>
    <text:p text:style-name="Text_20_body">Hello</text:p>
    <text:p text:style-name="P1">World</text:p>
    </office:text>
  </office:body>
</office:document-content>
'''

        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><para>Hello</para><para align="center" role="center">World</para></sect1></book>''')

    def test_italic_paragraph(self):
        doc = '''\
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">
  <office:automatic-styles>
    <style:style style:name="P1" style:family="paragraph" style:parent-style-name="Text_20_body">
      <style:text-properties fo:font-style="italic"/>
    </style:style>
  </office:automatic-styles>
  <office:body>
    <office:text>
    <text:h text:style-name="Heading_20_3">Chapter</text:h>
    <text:p text:style-name="Text_20_body">Hello</text:p>
    <text:p text:style-name="P1">World</text:p>
    </office:text>
  </office:body>
</office:document-content>
'''

        self.assertEqual(
                odf2legi.convert_to_legi_xml(doc),
                '''<book><sect1><title>Chapter</title><para>Hello</para><para><emphasis role="italic">World</emphasis></para></sect1></book>''')

if __name__ == '__main__':
    unittest.main()
