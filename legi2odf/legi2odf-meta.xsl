<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
    >

  <xsl:import href="legi2odf-common.xsl"/>

  <xsl:template match="property[@name = 'title']">
    <dc:title><xsl:value-of select="."/></dc:title>
  </xsl:template>

  <xsl:template match="property[@name = 'author']">
    <dc:creator><xsl:value-of select="."/></dc:creator>
  </xsl:template>

  <xsl:template match="property[@type = 'custom']">
    <meta:user-defined meta:name="{@name}" meta:value-type="string"><xsl:value-of
    		select="."/></meta:user-defined>
  </xsl:template>

  <xsl:template match="metadata">
    <office:document-meta
        xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
        xmlns:ooo="http://openoffice.org/2004/office" office:version="1.0">
      <office:meta>
         <meta:generator>legi2odf</meta:generator>
         <meta:keyword>Tabellio/PCF</meta:keyword>
         <xsl:apply-templates select="property" />
         <meta:user-defined meta:name="tabellioDisplayId" meta:value-type="string">
           <xsl:value-of select="$pdfgen.document.id"/>
         </meta:user-defined>
      </office:meta>
    </office:document-meta>
  </xsl:template>

  <xsl:template match="book">
    <xsl:apply-templates select="metadata"/>
  </xsl:template>

</xsl:stylesheet>
