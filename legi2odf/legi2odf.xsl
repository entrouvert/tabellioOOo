<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
    xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
    xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
    xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
    xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
    xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:tabellio="urn:tabellio"
    >

  <xsl:import href="legi2odf-common.xsl"/>

  <xsl:template match="node()">
    <xsl:message>W: unhandled <xsl:value-of select="local-name(.)"/></xsl:message>
  </xsl:template>

  <xsl:template match="anchor" mode="inline">
  </xsl:template>

  <xsl:template match="colspec">
    <xsl:element name="tabellio:colspec">
      <xsl:attribute name="tabellio:colname"><xsl:value-of select="@colname"/></xsl:attribute>
      <xsl:attribute name="tabellio:colnum"><xsl:value-of select="@colnum"/></xsl:attribute>
      <xsl:attribute name="tabellio:colwidth"><xsl:value-of select="@colwidth"/></xsl:attribute>
    </xsl:element>
  </xsl:template>

  <xsl:template match="part|chapter|sect1|sect2|sect3|sect4|preface|synthese|legistic_book|legistic_part|legistic_title|legistic_chapter|legistic_section|legistic_subsection|nosection">
    <xsl:apply-templates select="*"/>
  </xsl:template>

  <xsl:template match="speech/ref">
  </xsl:template>

  <xsl:template match="speech">
    <!-- speech element has the following structure:
           <speech id="986895382">
            <ref type="TABELLIO">
            <param name="id">000374757</param>
            <param name="classname">PARL</param>
            <param name="aspres">1</param>
           </ref>
           <para><emphasis role="bold">M. le président. -</emphasis> Mesdames, messieurs, ...</para>
           <para>...</para>
          </speech>

	  The <ref> is converted into an annotation into the first paragraph.
      -->
 
    <xsl:apply-templates select="para[position() = 1]" mode="speech">
      <xsl:with-param name="ref" select="ref"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="*[position() > 2]"/>
  </xsl:template>

  <!-- identity -->
  <xsl:template match="node()|@*" mode="inline">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*" mode="inline"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="subtitle">
    <text:h text:style-name="Sous-Titre">
      <xsl:apply-templates select="node() | text()" mode="inline"/>
    </text:h>
  </xsl:template>

  <xsl:template match="legistic_part/title">
    <text:list text:style-name="Legistic_20_Part_20_Numbering">
    <text:list-item>
    <text:p text:style-name="Lpart">
       <xsl:apply-templates select="node() | text()" mode="inline"/>
    </text:p>
    </text:list-item>
    </text:list>
  </xsl:template>

  <xsl:template match="legistic_book/title">
    <text:list text:style-name="Legistic_20_Book_20_Numbering">
    <text:list-item>
    <text:p text:style-name="LLivre">
       <xsl:apply-templates select="node() | text()" mode="inline"/>
    </text:p>
    </text:list-item>
    </text:list>
  </xsl:template>

  <xsl:template match="legistic_title/title">
    <text:list text:style-name="Legistic_20_Title_20_Numbering">
    <text:list-item>
    <text:p text:style-name="Ltitre">
       <xsl:apply-templates select="node() | text()" mode="inline"/>
    </text:p>
    </text:list-item>
    </text:list>
  </xsl:template>

  <xsl:template match="legistic_chapter/title">
    <text:list text:style-name="Legistic_20_Chapter_20_Numbering">
    <text:list-item>
    <text:p text:style-name="Lchapitre">
       <xsl:apply-templates select="node() | text()" mode="inline"/>
    </text:p>
    </text:list-item>
    </text:list>
  </xsl:template>

  <xsl:template match="legistic_section/title">
    <text:list text:style-name="Legistic_20_Section_20_Numbering">
    <text:list-item>
    <text:p text:style-name="Lsection">
       <xsl:apply-templates select="node() | text()" mode="inline"/>
    </text:p>
    </text:list-item>
    </text:list>
  </xsl:template>

  <xsl:template match="legistic_subsection/title">
    <text:p text:style-name="LSous-Section">
       <xsl:apply-templates select="node() | text()" mode="inline"/>
    </text:p>
  </xsl:template>

  <xsl:template match="title">
    <xsl:variable name="par"><xsl:value-of select="local-name(parent::node())"/></xsl:variable>
    <xsl:choose>
      <xsl:when test="$par = 'preface'">
        <text:p text:style-name="TitrePreface"><xsl:value-of select="."/></text:p>
      </xsl:when>
      <xsl:when test="$par = 'synthese'">
        <text:p text:style-name="TitreSynthese"><xsl:value-of select="."/></text:p>
      </xsl:when>
      <xsl:when test="$par = 'table'"></xsl:when> <!-- ignoring legend, it's handled in match="table" -->
      <xsl:otherwise>
        <text:h><xsl:attribute name="text:style">
          <xsl:choose>
            <xsl:when test="$par = 'part'">Heading_20_1</xsl:when>
            <xsl:when test="$par = 'chapter'">Heading_20_2</xsl:when>
            <xsl:when test="$par = 'sect1'">Heading_20_3</xsl:when>
            <xsl:when test="$par = 'sect2'">Heading_20_4</xsl:when>
            <xsl:when test="$par = 'sect3'">Heading_20_5</xsl:when>
            <xsl:when test="$par = 'sect4'">Heading_20_6</xsl:when>
            <xsl:otherwise>
              <xsl:message>Unknown location for title: <xsl:value-of select="$par"/></xsl:message>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute><xsl:attribute name="text:outline-level">
          <xsl:choose>
            <xsl:when test="$par = 'part'">1</xsl:when>
            <xsl:when test="$par = 'chapter'">2</xsl:when>
            <xsl:when test="$par = 'sect1'">3</xsl:when>
            <xsl:when test="$par = 'sect2'">4</xsl:when>
            <xsl:when test="$par = 'sect3'">5</xsl:when>
            <xsl:when test="$par = 'sect4'">6</xsl:when>
          </xsl:choose>
        </xsl:attribute><xsl:value-of select="."/></text:h>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="note">
    <text:p text:style-name="Note">
      <xsl:apply-templates select="para/node() | para/text()" mode="inline"/>
    </text:p>
  </xsl:template>

  <xsl:template match="para" mode="speech">
    <xsl:param name="ref"/>
    <text:p>
      <office:annotation>
        <text:p>type: <xsl:value-of select="$ref/@type"/></text:p>
        <text:p>ref id: <xsl:value-of select="$ref/param[@name = 'id']"/></text:p>
        <text:p>classname: <xsl:value-of select="$ref/param[@name = 'classname']"/></text:p>
        <text:p>aspres: <xsl:value-of select="$ref/param[@name = 'aspres']"/></text:p>
      </office:annotation>
      <xsl:apply-templates select="node() | text()" mode="inline"/>
    </text:p>
  </xsl:template>

  <xsl:template match="para">
    <xsl:choose>
      <xsl:when test="orderedlist">
        <xsl:apply-templates select="*"/>
      </xsl:when>
      <xsl:when test="itemizedlist">
        <xsl:apply-templates select="*"/>
      </xsl:when>
      <xsl:when test="@role = 'legistic_manualarticle'">
        <text:p text:style-name="Larttitre">
          <xsl:apply-templates select="node() | text()" mode="inline"/>
        </text:p>
      </xsl:when>
      <xsl:when test="@role = 'note_table_des_matieres'">
        <text:p text:style-name="NoteTableDesMatieres">
          <xsl:apply-templates select="node() | text()" mode="inline"/>
        </text:p>
      </xsl:when>
      <xsl:when test="@role = 'right'">
        <text:p text:style-name="ParaRight">
          <xsl:apply-templates select="node() | text()" mode="inline"/>
        </text:p>
      </xsl:when>
      <xsl:when test="@role = 'left'">
        <text:p text:style-name="ParaLeft">
          <xsl:apply-templates select="node() | text()" mode="inline"/>
        </text:p>
      </xsl:when>
      <xsl:when test="@role = 'center'">
        <text:p text:style-name="ParaCenter">
          <xsl:apply-templates select="node() | text()" mode="inline"/>
        </text:p>
      </xsl:when>
      <xsl:when test="@role = 'objet'">
        <text:p text:style-name="ParaObjet">
          <xsl:apply-templates select="node() | text()" mode="inline"/>
        </text:p>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="@role">
          <xsl:message>W: unknown para role: <xsl:value-of select="@role"/></xsl:message>
        </xsl:if>
        <text:p>
          <xsl:apply-templates select="node() | text()" mode="inline"/>
        </text:p>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="orderedlist[@numeration = 'arabic']">
    <text:list text:style-name="NumList">
      <xsl:apply-templates select="*"/>
    </text:list>
  </xsl:template>

  <xsl:template match="orderedlist[@numeration = 'loweralpha']">
    <text:list text:style-name="AlphaList">
      <xsl:apply-templates select="*"/>
    </text:list>
  </xsl:template>

  <xsl:template match="itemizedlist">
    <text:list text:style-name="BulletList">
      <xsl:apply-templates select="*"/>
    </text:list>
  </xsl:template>

  <xsl:template match="listitem">
    <text:list-item>
      <xsl:apply-templates select="*"/>
    </text:list-item>
  </xsl:template>

  <xsl:template match="footnote" mode="inline">
    <text:note text:note-class="footnote">
      <text:note-body>
        <xsl:apply-templates select="*"/>
      </text:note-body>
    </text:note>
  </xsl:template>

  <xsl:template match="emphasis"> <!-- catch them out of <para> -->
    <text:p>
      <xsl:call-template name="inline-emphasis" mode="inline"/>
    </text:p>
  </xsl:template>

  <xsl:template name="inline-emphasis" match="emphasis" mode="inline">
    <xsl:choose>
      <xsl:when test="@role = 'bold'"/>
      <xsl:when test="@role = 'underline'"/>
      <xsl:when test="@role = 'italic'"/>
      <xsl:otherwise>
        <xsl:message>Unknown emphasis role: <xsl:value-of select="@role"/></xsl:message>
      </xsl:otherwise>
    </xsl:choose>
    <text:span text:style-name="TS{@role}"><xsl:apply-templates select="node() | text()" mode="inline"/></text:span>
  </xsl:template>

  <xsl:template match="processing-instruction('line-break')" mode="inline">
     <text:line-break/>
  </xsl:template>

  <xsl:template match="processing-instruction('page-break')" mode="inline">
     <text:soft-page-break/>
  </xsl:template>

  <xsl:template match="xref" mode="inline">
    <text:sequence-ref text:reference-format="value"><xsl:attribute name="text:ref-name"
      ><xsl:value-of select="@linkend"/></xsl:attribute>1</text:sequence-ref>
  </xsl:template>

  <xsl:template match="informaltable">
    <xsl:element name="table:table">
      <xsl:attribute name="table:name"></xsl:attribute>
      <xsl:attribute name="table:style-name">Table1</xsl:attribute>
      <xsl:attribute name="table:name"><xsl:value-of select="@id"/></xsl:attribute>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>
  
  
  <xsl:template match="table">
    <xsl:variable name="tabletitle"><xsl:value-of select="title"/></xsl:variable>
    <xsl:element name="table:table">
      <xsl:attribute name="table:name"></xsl:attribute>
      <xsl:attribute name="table:style-name">Table1</xsl:attribute>
      <xsl:attribute name="table:name"><xsl:value-of select="@id"/></xsl:attribute>
      <xsl:apply-templates/>
    </xsl:element>
    <xsl:if test="not($tabletitle='')">
      <xsl:element name="text:p">
        <xsl:attribute name="text:style-name">Table</xsl:attribute>
        Tableau
        <text:sequence text:name="Table" text:formula="ooow:Table+1"
                  style:num-format="1"><xsl:attribute 
                  name="text:ref-name"><xsl:value-of select="title/@id"/></xsl:attribute>1</text:sequence>:
          <xsl:value-of select="$tabletitle"/>
        </xsl:element>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="tgroup">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="indexterm">
  </xsl:template>
  
  <xsl:template match="thead">
    <xsl:element name="table:table-header-rows">
      <xsl:apply-templates />
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="tbody">
      <xsl:apply-templates />
  </xsl:template>
  
  <xsl:template match="row">
    <xsl:element name="table:table-row">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="mediaobject" mode="inline">
    <xsl:choose>
      <xsl:when test="caption">
        <draw:frame>
          <xsl:attribute name="svg:width">
            <xsl:value-of select="imageobject/imagedata/@width"/>
          </xsl:attribute>
          <draw:text-box>
            <xsl:attribute name="fo:min-height">
              <xsl:value-of select="imageobject/imagedata/@depth"/>
            </xsl:attribute>
            <text:p>
              <xsl:apply-templates/>
              Illustration
              <text:sequence text:name="Illustration" text:formula="ooow:Illustration+1" style:num-format="1">
                <xsl:attribute name="text:ref-name">
                  <xsl:value-of select="caption/@id"/>
                </xsl:attribute> X </text:sequence>: <xsl:value-of select="caption"/>
            </text:p>
          </draw:text-box>
        </draw:frame>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <xsl:template match="imageobject">
    <draw:frame text:anchor-type="paragraph">
      <xsl:attribute name="svg:width">
         <xsl:value-of select="imagedata/@width"/>
      </xsl:attribute>
      <xsl:attribute name="svg:height">
         <xsl:value-of select="imagedata/@depth"/>
      </xsl:attribute>
      <draw:image xlink:type="simple" xlink:show="embed" xlink:actuate="onLoad">
        <xsl:attribute name="xlink:href">Pictures/<xsl:value-of select="imagedata/@fileref"/></xsl:attribute>
      </draw:image>
    </draw:frame>
  </xsl:template>

  <xsl:template match="entry">
    <xsl:element name="table:table-cell">
      <xsl:if test="ancestor-or-self::thead">
        <xsl:attribute name="table:style-name">Table1.A1</xsl:attribute>
      </xsl:if>
      <xsl:if test="not(ancestor-or-self::thead)">
        <xsl:attribute name="table:style-name">Table1.A2</xsl:attribute>
      </xsl:if>
  
      <xsl:choose>
        <xsl:when test="@spanname">
          <xsl:variable name="sname" >
            <xsl:value-of select="@spanname"/>
          </xsl:variable>
          <xsl:attribute name="table:number-columns-spanned">
            <xsl:variable name="colnamestart">
              <xsl:value-of select="ancestor::tgroup/spanspec[@spanname=$sname]/@namest"/>
            </xsl:variable>
            <xsl:variable name="colnameend">
              <xsl:value-of select="ancestor::tgroup/spanspec[@spanname=$sname]/@nameend"/>
            </xsl:variable>
            <xsl:variable name="colnumstart">
              <xsl:value-of select="ancestor::tgroup/colspec[@colname=$colnamestart]/@colnum"/>
            </xsl:variable>
            <xsl:variable name="colnumend">
              <xsl:value-of select="ancestor::tgroup/colspec[@colname=$colnameend]/@colnum"/>
            </xsl:variable>
            <xsl:value-of select="$colnumend - $colnumstart + 1"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:when  test="@namest and @nameend">
          <xsl:variable name="colnamestart">
            <xsl:value-of select="@namest"/>
          </xsl:variable>
          <xsl:variable name="colnameend">
            <xsl:value-of select="@nameend"/>
          </xsl:variable>
  
          <xsl:attribute name="table:number-columns-spanned">
            <xsl:variable name="colnumstart">
              <xsl:value-of select="ancestor::tgroup/colspec[@colname=$colnamestart]/@colnum"/>
            </xsl:variable>
            <xsl:variable name="colnumend">
              <xsl:value-of select="ancestor::tgroup/colspec[@colname=$colnameend]/@colnum"/>
            </xsl:variable>
            <xsl:value-of select="$colnumend - $colnumstart + 1"/>
          </xsl:attribute>
        </xsl:when>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="not(child::para)">
          <xsl:element name="text:p">
              <xsl:if test="ancestor-or-self::thead">
                <xsl:attribute name="text:style-name">Table Heading</xsl:attribute>
              </xsl:if>
              <xsl:if test="ancestor-or-self::tbody">
                <xsl:attribute name="text:style-name">Table Contents</xsl:attribute>
              </xsl:if>
              <xsl:apply-templates/>
          </xsl:element>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:element>
  </xsl:template>

  <xsl:template match="property">
    <text:user-field-decl office:value-type="string" office:string-value="{.}" text:name="{@name}"/>
  </xsl:template>

  <xsl:template match="metadata" mode="footnotedocrefs">
    <xsl:if test="property[@name = 'docrefs']">
      <text:note text:note-class="footnote">
        <text:note-body>
          <text:p><xsl:value-of select="property[@name = 'docrefs']"/></text:p>
        </text:note-body>
      </text:note>
    </xsl:if>
  </xsl:template>

  <xsl:template match="metadata">
    <text:user-field-decls>
      <xsl:apply-templates select="property"/>
      <text:user-field-decl office:value-type="string" office:string-value="{$pdfgen.document.id}" text:name="tabellioDisplayId"/>
    </text:user-field-decls>
  </xsl:template>

  <xsl:template match="book">
    <office:document-content office:version="1.0">
      <office:automatic-styles>
        <style:style style:name="TSbold" style:family="text">
          <style:text-properties fo:font-weight="bold"/>
        </style:style>
        <style:style style:name="TSunderline" style:family="text">
          <style:text-properties style:text-underline-style="solid"/>
        </style:style>
        <style:style style:name="TSitalic" style:family="text">
          <style:text-properties fo:font-style="italic"/>
        </style:style>
        <style:style style:name="Table1" style:family="table">
          <style:properties style:width="14.649cm" table:align="margins"/>
        </style:style>
        <style:style style:name="Table1.A" style:family="table-column">
          <style:properties style:column-width="2.93cm" style:rel-column-width="13107*"/>
        </style:style>
        <style:style style:name="Table1.A1" style:family="table-cell">
          <style:properties fo:padding="0.097cm" fo:border-left="0.002cm solid #000000" fo:border-right="none" fo:border-top="0.002cm solid #000000" fo:border-bottom="0.002cm solid #000000"/>
        </style:style>
        <style:style style:name="Table1.E1" style:family="table-cell">
          <style:properties fo:padding="0.097cm" fo:border="0.002cm solid #000000"/>
        </style:style>
        <style:style style:name="Table1.A2" style:family="table-cell">
          <style:properties fo:padding="0.097cm" fo:border-left="0.002cm solid #000000" fo:border-right="none" fo:border-top="none" fo:border-bottom="0.002cm solid #000000"/>
        </style:style>
        <style:style style:name="Table1.E2" style:family="table-cell">
          <style:properties fo:padding="0.097cm" fo:border-left="0.002cm solid #000000" fo:border-right="0.002cm solid #000000" fo:border-top="none" fo:border-bottom="0.002cm solid #000000"/>
        </style:style>

	<text:list-style style:name="Litemized">
	  <text:list-level-style-bullet text:level="1" text:bullet-char="—">
	    <style:list-level-properties text:space-before="0.635cm" text:min-label-width="0.635cm"/>
	  </text:list-level-style-bullet>
	</text:list-style>
        <text:list-style style:name="Lordered">
          <text:list-level-style-number text:level="1" style:num-suffix="." style:num-format="1">
            <style:list-level-properties text:space-before="0.635cm" text:min-label-width="0.635cm"/>
          </text:list-level-style-number>
        </text:list-style>
      </office:automatic-styles>
      <office:body>
	<office:text>
          <xsl:apply-templates select="*"/>
	</office:text>
      </office:body>
    </office:document-content>
  </xsl:template>

</xsl:stylesheet>
