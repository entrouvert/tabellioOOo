<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
    xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
    xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
    xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
    >

<xsl:variable name="pdfgen.metadata.no.prefix">
    <xsl:choose>
        <xsl:when test="$pdfgen.metadata.typedocid = 'CRI'">
            <xsl:text>CRI Nº</xsl:text>
        </xsl:when>
        <xsl:when test="$pdfgen.metadata.typedocid = 'CRICOM'">
            <xsl:text>CRIc Nº</xsl:text>
        </xsl:when>
        <xsl:when test="$pdfgen.metadata.typedocid = 'BQR'">
            <xsl:text>Nº</xsl:text>
        </xsl:when>
        <xsl:otherwise>
        </xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="pdfgen.metadata.no" select="/book/metadata/property[@name='no']"/>
<xsl:variable name="pdfgen.metadata.sess" select="/book/metadata/property[@name='sess']"/>
<xsl:variable name="pdfgen.metadata.nodoc" select="/book/metadata/property[@name='nodoc']"/>
<xsl:variable name="pdfgen.metadata.typedocid" select="/book/metadata/property[@name='typedocid']"/>
<xsl:variable name="pdfgen.metadata.anx" select="/book/metadata/property[@name='anx']"/>

<xsl:variable name="pdfgen.document.id">
    <xsl:value-of select="$pdfgen.metadata.no.prefix"/>
    <xsl:value-of select="$pdfgen.metadata.no"/>
    <xsl:if test="$pdfgen.metadata.sess">
      <xsl:text> (</xsl:text>
      <xsl:value-of select="$pdfgen.metadata.sess"/>
      <xsl:text>)</xsl:text>
    </xsl:if>
    <xsl:if test="/book/metadata/property[@name='nodoc']">
        <xsl:text> — Nº </xsl:text>
        <xsl:value-of select="$pdfgen.metadata.nodoc"/>
    </xsl:if>
    <xsl:if test="/book/metadata/property[@name='anx']">
        <xsl:text> (Annexe </xsl:text>
        <xsl:value-of select="$pdfgen.metadata.anx"/>
        <xsl:text>)  </xsl:text>
    </xsl:if>
</xsl:variable>

</xsl:stylesheet>
